/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IoUtils {

    private static final Logger logger = LoggerFactory.getLogger(IoUtils.class);

    public static String readFileAsString(File file) throws IOException {
        InputStream in = new FileInputStream(file);
        return readFileFromInputStreamAsString(in);
    }
    
    /**
     *   Reads a file with the given filename from the resources directory and returns it in String format
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public static String readFileAsString(String filename) throws IOException {
        logger.info("reading file {} as a string", filename);

        // problem - this file name must be unique!
        InputStream in = IoUtils.class.getClassLoader().getResourceAsStream(filename);
        return readFileFromInputStreamAsString(in);
    }
    
    public static String readFileFromInputStreamAsString(InputStream in) throws IOException {

        String fileString = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder builder = new StringBuilder();
            char[] buffer = new char[8192];
            int read;
            while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
                builder.append(buffer, 0, read);
            }
            fileString = builder.toString();
        } finally {
            if (in!=null) {
                in.close();
            }
        }

        return fileString;
    }
    
    /**
     * 
     *   Reads a file with the given filename from the resources directory and returns it as a list of strings (each new line is a new string)
     * 
     * @param filename
     * @return
     * @throws IOException
     */
    public static List<String> readFileAsStringList(String filename) throws IOException {
        InputStream in = IoUtils.class.getClassLoader().getResourceAsStream(filename);
        return readFileFromInputStreamAsStringList(in);
    }

    public static List<String> readFileAsStringList(File file) throws IOException {
        InputStream in = new FileInputStream(file);
        return readFileFromInputStreamAsStringList(in);
    }

    private static List<String> readFileFromInputStreamAsStringList(InputStream in) throws IOException {
        logger.debug("reading from from stream {}", in);

        List<String> stringList = new ArrayList<String>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                if (!"".equals(sCurrentLine.trim())) {
                    stringList.add(sCurrentLine);
                }
            }
        } finally {
            if (in!=null) {
                in.close();
            }
        }

        return stringList;
    }

    public static void writeFileFromString(File file, String saveGameString) throws IOException {

        PrintWriter writer = null;
        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            writer = new PrintWriter(file);
            writer.println(saveGameString);

        } finally {
            if (writer!=null) {
                writer.close();
            }
        }

    }

}
