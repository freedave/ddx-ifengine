/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.util;

public class StringUtils {

    /**
     *  Removes quotes around text and strips leading and trailing spaces
     *
     * @param responseText
     * @return
     */
    public static String trimAndStripQuotes(String responseText) {
        responseText.trim();

        while (responseText.startsWith("\"")) {
            responseText = responseText.substring(1);
        }
        while (responseText.endsWith("\"")) {
            responseText = responseText.substring(0, responseText.length()-1);
        }

        return responseText;
    }

    /**
     *   The first character of a string is make upper case (if not already)
     *
     * @param word
     * @return
     */
    public static String capitalize(String word) {
        StringBuilder capital = new StringBuilder(word.substring(0,1).toUpperCase());
        capital.append(word.substring(1));
        return capital.toString();
    }

    /**
     *  If the first character of the string is upper case - make it lowercase
     *
     * @param word
     * @return
     */
    public static String decapitalize(String word) {
        StringBuilder decapitalized = new StringBuilder(word.substring(0,1).toLowerCase());
        decapitalized.append(word.substring(1));
        return decapitalized.toString();
    }

    /**
     *  Return true if null or blank
     *
     * @param executionMessage
     * @return
     */
    public static boolean isEmpty(String text) {
        return (text==null || text.trim().equals(""));
    }

    /**
     *  Verifies if two strings are both not null and equal (ignoring case)
     *
     * @param id
     * @param narratorId
     * @return
     */
    public static boolean equalsIgnoreCase(String text, String compareText) {
        boolean equal = false;
        if (text!=null && compareText!=null && text.equalsIgnoreCase(compareText)) {
            equal = true;
        }

        return equal;
    }

    /**
     *  Removes all non-alphabetic (a-z, upper or lower case) characters from a text string
     *
     * @param title
     * @return
     */
    public static String stripNonAlphas(String title) {
        return title.replaceAll("[^a-zA-Z]", "");
    }

    /**
     *  Truncates text to the specified limit (if length is greater than the limit)
     *
     * @param text
     * @param limit
     * @return
     */
    public static String truncate(String text, int limit, boolean appendDots) {
        if (text!=null && text.length()>limit) {
            text = text.substring(0, limit);
            if (appendDots) {
                text += "...";
            }
        }
        return text;
    }

    /**
     *
     * Ignores case when checking the startsWith condition, and also checks for null (returns false if either text is null).
     *
     * @param startingText - the fragment of the text to check for
     * @param fullText
     * @return
     */
    public static boolean startsWithIgnoreCase(String startingText, String fullText) {
        return (fullText!=null
            && startingText!=null
            && fullText.toLowerCase().startsWith(startingText.toLowerCase()));
    }
}
