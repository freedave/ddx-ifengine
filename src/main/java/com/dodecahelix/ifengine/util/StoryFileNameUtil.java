/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.util;

import com.dodecahelix.ifengine.data.story.LibraryCard;

public class StoryFileNameUtil {

    public static String getStoryFileName(LibraryCard metadata) {
        return getStoryFileName(metadata.getTitle(), metadata.getAuthor(), metadata.getVersion());
    }

    public static String getStoryFileName(String title, String author, String version) {

        title = StringUtils.stripNonAlphas(title).toLowerCase();
        if (title.length()>15) {
            title = title.substring(0, 15);
        }
        author = StringUtils.stripNonAlphas(author).toLowerCase();
        if (author.length()>10) {
            author = author.substring(0, 10);
        }

        StringBuilder fileNameBuilder = new StringBuilder();

        fileNameBuilder.append(title);
        fileNameBuilder.append("-");
        fileNameBuilder.append(author);
        fileNameBuilder.append("-");
        fileNameBuilder.append(version.toLowerCase());

        return fileNameBuilder.toString();
    }

}
