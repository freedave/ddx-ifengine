/*
 *  Copyright (c) 2015 David Peters
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and limitations under the License.
 *
 *  Contributors:
 *      David Peters - initial API and implementation
 */

package com.dodecahelix.ifengine;

import com.dodecahelix.ifengine.action.ActionHandler;
import com.dodecahelix.ifengine.action.ActionHandlerImpl;
import com.dodecahelix.ifengine.choice.ChoiceProcessor;
import com.dodecahelix.ifengine.choice.ChoiceProcessorImpl;
import com.dodecahelix.ifengine.choice.ChoiceRecorder;
import com.dodecahelix.ifengine.choice.ChoiceRecorderImpl;
import com.dodecahelix.ifengine.command.CommandHandler;
import com.dodecahelix.ifengine.command.CommandHandlerImpl;
import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.condition.ConditionDeterminationImpl;
import com.dodecahelix.ifengine.condition.ConditionResolutionLocator;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.dialog.DialogHandler;
import com.dodecahelix.ifengine.dialog.DialogHandlerImpl;
import com.dodecahelix.ifengine.options.OptionTreeBuilder;
import com.dodecahelix.ifengine.options.OptionTreeBuilderFactory;
import com.dodecahelix.ifengine.options.DialogTreeBuilder;
import com.dodecahelix.ifengine.response.DynamicTextFilter;
import com.dodecahelix.ifengine.response.DynamicTextFilterImpl;
import com.dodecahelix.ifengine.response.StyledResponseFilter;
import com.dodecahelix.ifengine.response.StyledResponseFilterImpl;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;
import com.dodecahelix.ifengine.result.ExecutionResultHandlerImpl;
import com.dodecahelix.ifengine.result.ExecutionServiceLocator;
import com.dodecahelix.ifengine.environment.EnvironmentProcessor;
import com.dodecahelix.ifengine.environment.JsonEnvironmentProcessor;
import com.dodecahelix.ifengine.time.TimeHandler;

/**
 *   This is the primary interface for the IF-Engine library.
 *
 */
public class DefaultIfEngine implements IfEngine {

    private ChoiceProcessor choiceProcessor;
    private ChoiceRecorder choiceRecorder;
    private EnvironmentProcessor environmentProcessor;

    public DefaultIfEngine() {
        this(new SimpleEnvironmentDAO());
    }

    public DefaultIfEngine(EnvironmentDAO dao) {

        // wire up the default dependencies
        ExecutionServiceLocator executionLocator = new ExecutionServiceLocator();
        ConditionResolutionLocator conditionResolutionLocator = new ConditionResolutionLocator();
        ConditionDetermination conditionDetermination = new ConditionDeterminationImpl(dao, conditionResolutionLocator);
        OptionTreeBuilderFactory optionTreeBuilderFactory = new OptionTreeBuilderFactory(dao, conditionDetermination);
        DialogTreeBuilder dialogTreeBuilder = new DialogTreeBuilder(dao, conditionDetermination);
        OptionTreeBuilder optionTreeBuilder = new OptionTreeBuilder(optionTreeBuilderFactory);
        DynamicTextFilter textFilter = new DynamicTextFilterImpl(dao);
        ExecutionResultHandler resultHandler = new ExecutionResultHandlerImpl(dao, executionLocator);
        StyledResponseFilter responseFilter = new StyledResponseFilterImpl(dao);
        DialogHandler dialogHandler = new DialogHandlerImpl(dao, resultHandler, textFilter);
        ActionHandler actionHandler = new ActionHandlerImpl(dao, conditionDetermination, resultHandler, responseFilter);
        TimeHandler timeHandler = new TimeHandler(dao);
        CommandHandler commandHandler = new CommandHandlerImpl(actionHandler, resultHandler, responseFilter);

        choiceRecorder = new ChoiceRecorderImpl();
        choiceProcessor = new ChoiceProcessorImpl(dao, optionTreeBuilder, dialogTreeBuilder, commandHandler, dialogHandler, actionHandler, timeHandler, choiceRecorder);
        environmentProcessor = new JsonEnvironmentProcessor(dao, choiceProcessor, choiceRecorder);
    }

    @Override
    public ChoiceProcessor getChoiceProcessor() {
        return choiceProcessor;
    }

    public void setChoiceProcessor(ChoiceProcessor choiceProcessor) {
        this.choiceProcessor = choiceProcessor;
    }

    @Override
    public EnvironmentProcessor getEnvironmentProcessor() {
        return environmentProcessor;
    }

    public void setEnvironmentProcessor(EnvironmentProcessor environmentProcessor) { this.environmentProcessor = environmentProcessor; }

    @Override
    public ChoiceRecorder getChoiceRecorder() {
        return choiceRecorder;
    }

    public void setChoiceRecorder(ChoiceRecorder choiceRecorder) {
        this.choiceRecorder = choiceRecorder;
    }
}
