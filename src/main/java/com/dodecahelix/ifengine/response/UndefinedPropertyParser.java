/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.response.handlers.DescriptionPropertyHandler;
import com.dodecahelix.ifengine.response.handlers.PropertyHandler;
import com.dodecahelix.ifengine.response.handlers.PutTypeHandler;
import com.dodecahelix.ifengine.response.handlers.ShowWornItemsHandler;
import com.dodecahelix.ifengine.response.handlers.StoryPassageHandler;
import com.dodecahelix.ifengine.response.handlers.TimePropertyHandler;
import com.dodecahelix.ifengine.response.handlers.TitlePropertyHandler;

/**
 *   Response strings can refer to special "undefined" properties of an object, that aren't contained in the property map
 *   
 *   (i.e; static properties, such as Description and Title).  These must be resolved dynamically.
 *   
 * @author dpeters
 *
 */
public class UndefinedPropertyParser {

    public List<PropertyHandler> propertyHandlers;

    public static final String TIME_PROP_ID = "time-full";
    public static final String STATS_PROP_ID = "stats";

    public UndefinedPropertyParser(EnvironmentDAO dao) {

        propertyHandlers = new ArrayList<PropertyHandler>();

        propertyHandlers.add(new StoryPassageHandler(dao));
        propertyHandlers.add(new DescriptionPropertyHandler(dao));
        propertyHandlers.add(new TitlePropertyHandler());
        propertyHandlers.add(new TimePropertyHandler(dao));
        propertyHandlers.add(new ShowWornItemsHandler(dao));
        propertyHandlers.add(new PutTypeHandler());
    }

    public String parseSpecialProperty(Entity subject, String subjectProperty) throws InvalidResponseException {
        String dynamicValue = null;

        for (PropertyHandler handler : propertyHandlers) {
            String propertyHandlerKey = handler.getPropertyName().toUpperCase();

            // use starts with, so we can handle additional attributes
            if (subjectProperty.toUpperCase().startsWith(propertyHandlerKey.toUpperCase())) {
                dynamicValue = handler.parseProperty(subject, subjectProperty);
                break;
            }
        }

        return dynamicValue;
    }






}
