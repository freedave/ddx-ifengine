/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response;

import static com.dodecahelix.ifengine.response.ResponseTokenBuilder.FILTER_CLOSE_TOKEN;
import static com.dodecahelix.ifengine.response.ResponseTokenBuilder.FILTER_START_TOKEN;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.BooleanDisplay;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.PropertyType;
import com.dodecahelix.ifengine.data.RangeDisplay;
import com.dodecahelix.ifengine.entity.EntityParser;
import com.dodecahelix.ifengine.entity.EntityParser.EntityOperand;
import com.dodecahelix.ifengine.entity.ParseException;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   Takes a string with filter annotations and makes the appropriate substitution
 *
 */
public class DynamicTextFilterImpl implements DynamicTextFilter {

    private UndefinedPropertyParser undefinedPropertyParser;
    private EnvironmentDAO dao;

    public DynamicTextFilterImpl(EnvironmentDAO dao) {
        this.dao = dao;
        this.undefinedPropertyParser = new UndefinedPropertyParser(dao);
    }

    public String filter(String unfilteredResponse, String parentEntityId, String localTargetId) throws InvalidResponseException, ParseException {
        StringBuffer response = new StringBuffer("");

        int i = unfilteredResponse.indexOf(FILTER_START_TOKEN);
        while (i >= 0) {
            // static response is untouched
            response.append(unfilteredResponse.substring(0,i));

            unfilteredResponse = unfilteredResponse.substring(i + FILTER_START_TOKEN.length());
            int j = unfilteredResponse.indexOf(FILTER_CLOSE_TOKEN);
            if (j<0) {
                throw new InvalidResponseException("Response has an unclosed filter on string : " + unfilteredResponse);
            } else {
                String dynamic = unfilteredResponse.substring(0,j).trim();
                String parsedValue = parseDynamic(dynamic, parentEntityId, localTargetId);

                // what if parsedValue has tokens?  RECURSE!!
                parsedValue = filter(parsedValue, parentEntityId, localTargetId);

                response.append(parsedValue);
                unfilteredResponse = unfilteredResponse.substring(j + FILTER_CLOSE_TOKEN.length());
            }
            i = unfilteredResponse.indexOf(ResponseTokenBuilder.FILTER_START_TOKEN);
        }

        // add the remainder of the unfiltered response..
        response.append(unfilteredResponse);

        // format - strip quotes and blank spaces from beginning and end of string
        return StringUtils.trimAndStripQuotes(response.toString());
    }

    /**
     *   Parse the dynamic token (text enclosed in the token tags)
     *
     * @param dynamic - dynamic text enclosed in token tags (i.e; @person.boy.name@)
     * @param parentEntityId - resolves the subject for conditions and executions for an entity id of "this"
     * @param localTargetId - resolves the target of conditions and executions for an entity id of "local"
     *
     * @return
     * @throws ParseException
     * @throws InvalidResponseException
     */
    private String parseDynamic(String dynamic, String parentEntityId, String localTargetId) throws ParseException, InvalidResponseException {

        // parse out an entity
        EntityHolder entityElementHolder = EntityParser.parseEntity(dynamic, EntityOperand.SUBJECT);

        // resolve "this" and "local" entities
        String subjectEntityId = entityElementHolder.getSubjectEntityId();
        if (EntityHolder.LOCAL_TARGET_ID.equalsIgnoreCase(subjectEntityId)) {
            subjectEntityId = localTargetId;
        }
        if (EntityHolder.THIS_SUBJECT_ID.equalsIgnoreCase(subjectEntityId)) {
            subjectEntityId = parentEntityId;
        }

        Entity subject = dao.getEntityById(entityElementHolder.getSubjectType(), subjectEntityId);
        if (subject==null) {
            throw new InvalidResponseException("no subject found for dynamic token " + FILTER_START_TOKEN + dynamic + FILTER_CLOSE_TOKEN + " entityType="
                    + entityElementHolder.getSubjectType() + "  entityId=" + subjectEntityId);
        }

        // first, see if this is a defined entity property.  If so, just print the value.
        String dynamicValue = null;
        String propertyKey = entityElementHolder.getSubjectProperty();
        if (propertyKey!=null) {
            Property property = subject.getPropertyById(propertyKey);
            if (property!=null) {
                dynamicValue = parsePropertyValue(property);
            }
        }

        // it isn't a property, so handle it through the undefined property handler
        if (dynamicValue==null) {
            dynamicValue = undefinedPropertyParser.parseSpecialProperty(subject, propertyKey);
        }

        // is it still null?  Throw an illegal arg exception?
        if (dynamicValue==null) {
            throw new IllegalArgumentException("dynamic token " + FILTER_START_TOKEN + dynamic + FILTER_CLOSE_TOKEN + " could not be parsed");

        }

        return dynamicValue;
    }

    private String parsePropertyValue(Property property) {

        // default to the string value of the property
        String propertyDisplay = property.getStringValue();

        for (RangeDisplay rangeDisplay : property.getRangeDisplays()) {
            // if this is an int property, check for a range display
            if ( PropertyType.INTEGER.equals(property.getType())) {
                int intValue = Integer.parseInt(propertyDisplay);
                if (intValue>=rangeDisplay.getLowRange() && intValue<= rangeDisplay.getHighRange()) {
                    propertyDisplay = rangeDisplay.getDisplay();
                    break;  // don't check other range display
                }
            }
        }

        for (BooleanDisplay booleanDisplay : property.getBooleanDisplays()) {
            if ( PropertyType.BOOLEAN.equals(property.getType())) {
                boolean boolValue = Boolean.parseBoolean(propertyDisplay);
                propertyDisplay = booleanDisplay.getDisplay(boolValue);
            }
        }

        return propertyDisplay;
    }

}
