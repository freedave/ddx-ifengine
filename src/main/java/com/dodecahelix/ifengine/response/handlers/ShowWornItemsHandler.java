/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response.handlers;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Item;

public class ShowWornItemsHandler implements PropertyHandler {

    public static final String WORN_ITEMS_TOKEN = "worn-items";

    private EnvironmentDAO dao;

    public ShowWornItemsHandler(EnvironmentDAO dao) {
        this.dao = dao;
    }

    @Override
    public String getPropertyName() {
        return "worn-items";
    }

    @Override
    public String parseProperty(Entity subject, String subjectProperty) {
        StringBuffer statDisplay = new StringBuffer("You are wearing:");

        boolean naked = true;
        for (Item item : dao.getInventory()) {
            if (item.isWorn()) {
                naked = false;
                statDisplay.append("<br>");
                statDisplay.append(item.getTitle());
            }
        }
        if (naked) {
            statDisplay.append("<br>Nothing");
        }

        return statDisplay.toString();
    }

}
