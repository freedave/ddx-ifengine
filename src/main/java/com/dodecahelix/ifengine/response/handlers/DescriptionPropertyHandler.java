/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response.handlers;

import com.dodecahelix.ifengine.Constants;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Schedule;

public class DescriptionPropertyHandler implements PropertyHandler {

    public static final String DESCRIPTION = "description";

    private EnvironmentDAO dao;

    public DescriptionPropertyHandler(EnvironmentDAO dao) {
        this.dao = dao;
    }

    @Override
    public String getPropertyName() {
        return DESCRIPTION;
    }

    @Override
    public String parseProperty(Entity subject, String subjectProperty) {
        String dynamicValue = null;

        if (subject instanceof Location) {
            dynamicValue = getLocationDescription((Location)subject);
        } else {
            dynamicValue = subject.getDescription();
        }

        return dynamicValue;
    }

    private String getLocationDescription(Location location) {
        String locationDescription = location.getDescription();

        for (Person person : dao.getPeopleForLocation(location.getId())) {
            locationDescription += Constants.NEWLINE_TOKEN;

            if (person.isFollowing()) {
                locationDescription += person.getFollowingDescription();
            } else {
                Schedule schedule = dao.getActiveScheduleForPerson(person);
                locationDescription += schedule.getActionMessage();
            }
        }

        return locationDescription;
    }

}
