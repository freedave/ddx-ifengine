/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response.handlers;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Time;
import com.dodecahelix.ifengine.data.configuration.TimeConfiguration;

public class TimePropertyHandler implements PropertyHandler {

    public enum TimeFormat {
        TIME_FULL ("time-full"),
        TIME_HOURS ("time-hours"),
        TIME_DAY ("time-day");

        private String key;

        TimeFormat(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }

        public static TimeFormat findByKey(String key) {
            for (TimeFormat format : values()) {
                if (format.key.equalsIgnoreCase(key)) {
                    return format;
                }
            }
            throw new IllegalArgumentException("No TimeFormat key for " + key);
        }

    }

    private EnvironmentDAO dao;

    public TimePropertyHandler(EnvironmentDAO dao) {
        this.dao = dao;
    }

    @Override
    public String getPropertyName() {
        return "time-";
    }

    @Override
    public String parseProperty(Entity subject, String subjectProperty) {
        String dynamicValue = null;
        TimeFormat timeType = TimeFormat.findByKey(subjectProperty.toLowerCase());
        dynamicValue = parseTime(timeType);

        return dynamicValue;
    }

    private String parseTime(TimeFormat timeType) {

        Time currentTime = dao.getEnvironment().getCurrentTime();
        TimeConfiguration timeConfig = dao.getEnvironment().getConfiguration().getTimeConfiguration();

        StringBuffer timeString = new StringBuffer();

        // TODO - make this configurable?
        int hoursPerDay = 24;
        int minutesPerHour = 60;

        int periodsPerDay = timeConfig.getPeriodsOfDay().size();
        int hoursPerPeriod = hoursPerDay / periodsPerDay;
        int baseHour = (currentTime.getPeriod()-1) * hoursPerPeriod;

        float incrementsPerHour = timeConfig.getIncrementsPerPeriod() / hoursPerPeriod;
        int minutesPerIncrement = Math.round(minutesPerHour / incrementsPerHour);
        int minutesPastBaseHour = (currentTime.getIncrement()-1) * minutesPerIncrement;

        int hoursPastBase = minutesPastBaseHour / minutesPerHour;
        int minutesPastHour = minutesPastBaseHour % minutesPerHour;

        if ((TimeFormat.TIME_FULL == timeType) || (TimeFormat.TIME_HOURS == timeType)) {

            // compute minutes per increment
            boolean AM = true;
            int hour = baseHour + hoursPastBase;
            if (hour>12) {
                hour = hour - 12;
                AM = false;
            }
            if (hour == 0) {
                // 12 AM
                hour = 12;
            }

            timeString.append(String.valueOf(hour));
            timeString.append(":");

            // compute minutes string
            if (minutesPastHour==minutesPerHour) {
                minutesPastHour = 0;
            }
            String minutesString = String.valueOf(minutesPastHour);
            if (minutesString.length()==1) {
                minutesString = "0" + minutesString;
            }
            timeString.append(minutesString);

            if (AM) {
                timeString.append(" AM");
            } else {
                timeString.append(" PM");
            }
            timeString.append(" in the ");
            timeString.append(timeConfig.getPeriodsOfDay().get(currentTime.getPeriod()-1));
        }

        if (TimeFormat.TIME_FULL == timeType) {
            timeString.append(" on ");
        }

        if ((TimeFormat.TIME_FULL == timeType) || (TimeFormat.TIME_DAY == timeType)) {
            timeString.append(timeConfig.getDaysOfWeek().get(currentTime.getWeekday()-1));

            timeString.append(", ");
            timeString.append(timeConfig.getMonthsOfYear().get(currentTime.getMonth()-1));
            timeString.append(" ");

            // calculate day of month
            int dayOfMonth = currentTime.getWeek() * timeConfig.getDaysOfWeek().size() + currentTime.getWeekday();
            timeString.append(String.valueOf(dayOfMonth));

            timeString.append(", ");
            timeString.append(String.valueOf(currentTime.getYear()));
        }

        return timeString.toString();
    }


}
