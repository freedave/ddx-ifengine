/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response.handlers;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.story.Passage;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.response.InvalidResponseException;
import com.dodecahelix.ifengine.util.StringUtils;

public class StoryPassageHandler implements PropertyHandler {

    public static final String PASSAGE = "story-";

    private EnvironmentDAO dao;

    public StoryPassageHandler(EnvironmentDAO dao) {
        this.dao = dao;
    }

    @Override
    public String getPropertyName() {
        return PASSAGE;
    }

    @Override
    public String parseProperty(Entity subject, String subjectProperty) throws InvalidResponseException {
        String passageId = subjectProperty.substring(PASSAGE.length());
        StoryContent content = dao.getEnvironment().getStoryContent();

        if (StoryContent.INTRO_PASSAGE_ID.equalsIgnoreCase(passageId)) {
            return content.getIntroduction();
        }

        for (Passage passage : content.getPassages()) {
            if (StringUtils.equalsIgnoreCase(passage.getPassageId(), passageId)) {
                return passage.getPassageText();
            }
        }

        throw new InvalidResponseException("invalid token. passage id " + passageId + " does not exist");
    }

}
