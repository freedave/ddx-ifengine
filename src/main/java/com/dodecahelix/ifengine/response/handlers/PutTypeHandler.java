/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response.handlers;

import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.response.InvalidResponseException;

public class PutTypeHandler implements PropertyHandler {

    public static final String PUT_TYPE_PROPERTY = "puttype";

    @Override
    public String getPropertyName() {
        return PUT_TYPE_PROPERTY;
    }

    @Override
    public String parseProperty(Entity subject, String subjectProperty) throws InvalidResponseException {
        String dynamicValue = null;

        if (subject instanceof Mass) {
            if (((Mass)subject).isOnNotIn()) {
                dynamicValue = "on top of";
            } else {
                dynamicValue = "inside of";
            }
        } else {
            throw new InvalidResponseException("subject of put-type response token is not a mass.  is a " + subject.getEntityType());
        }

        return dynamicValue;
    }

}
