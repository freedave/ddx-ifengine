/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response;

import java.util.List;

/**
 *   Breaks apart a single, unfiltered text line into multiple styled responses
 *
 */
public interface StyledResponseFilter {


    /**
     *
     * Given a text string with dynamic tokens and style tags break down into StyledResponseLine(s), an java object that statically defines the formatting and content of the text
     *
     * @param responseText
     * @param parentEntityId - resolves the subject for conditions and executions for an entity id of "this"
     * @param localTargetId - resolves the target of conditions and executions for an entity id of "local"

     * @return
     */
    public List<StyledResponseLine> filterResponse(String responseText, String parentEntityId, String localTargetId);

}
