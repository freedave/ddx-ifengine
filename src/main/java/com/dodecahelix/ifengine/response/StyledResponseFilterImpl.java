/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.configuration.MessageConfiguration;

public class StyledResponseFilterImpl implements StyledResponseFilter {

    private DynamicTextFilter dynamicTextFilter;
    private EnvironmentDAO dao;

    public StyledResponseFilterImpl(EnvironmentDAO dao) {
        dynamicTextFilter = new DynamicTextFilterImpl(dao);
        this.dao = dao;
    }

    public StyledResponseFilterImpl(DynamicTextFilter filter) {
        this.dynamicTextFilter = filter;
    }

    /**
     *
     * @param responseText
     * @param parentEntityId - resolves the subject for conditions and executions for an entity id of "this"
     * @param localTargetId - resolves the target of conditions and executions for an entity id of "local"
     *
     * @return
     */
    public List<StyledResponseLine> filterResponse(String responseText, String parentEntityId, String localTargetId) {

        List<StyledResponseLine> responses = new ArrayList<StyledResponseLine>();

        try {
            // filter dynamic property tokens
            String filteredResponse = dynamicTextFilter.filter(responseText, parentEntityId, localTargetId);

            // start with the default response
            ResponseType responseType = ResponseType.READING;
            int tagStart = filteredResponse.indexOf("<");
            while (tagStart>=0) {
                String previousText = filteredResponse.substring(0, tagStart);
                StyledResponseLine responseLine = new StyledResponseLine(previousText);
                responseLine.setType(responseType);
                responses.add(responseLine);

                // restore the default response type
                responseType = ResponseType.READING;

                filteredResponse = filteredResponse.substring(tagStart);
                int tagEnd = filteredResponse.indexOf(">");
                if (tagEnd<=0) {
                    throw new IllegalArgumentException("the '<' symbol can only be used in the context of a tag");
                }
                String tag = filteredResponse.substring(0, tagEnd+1);
                filteredResponse = filteredResponse.substring(tagEnd+1);

                // update the response type style based on the tag name (if other than <br>)
                for (ResponseType type : ResponseType.values()) {
                    if (type.getTag().equalsIgnoreCase(tag)) {
                        responseType = type;
                    }
                }
                if (ResponseType.PAUSE == responseType) {
                    StyledResponseLine pauseLine = new StyledResponseLine(dao
                        .getEnvironment().getConfiguration().getMessageConfiguration().getPauseMessage());
                    pauseLine.setType(ResponseType.PAUSE);
                    responses.add(pauseLine);

                    responseType = ResponseType.READING;
                }

                tagStart = filteredResponse.indexOf("<");
            }
            StyledResponseLine responseLine = new StyledResponseLine(filteredResponse);
            responseLine.setType(responseType);
            responses.add(responseLine);

        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Error filtering response text : " + responseText, e);
        }

        return responses;
    }

}
