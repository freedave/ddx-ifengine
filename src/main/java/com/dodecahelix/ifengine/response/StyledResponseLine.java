/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response;

import com.dodecahelix.ifengine.util.StringUtils;

/**
 *  Indicates a line of text to be presented to the reader, and a "type" for that line (i.e; dialog),
 *   so that the line can be presented with a different style based on the type.
 *
 *  Line and sentence are not the same.  A "line" can span multiple sentences or paragraphs, or could even be devoid of text entirely (i.e; a divider or line-space).
 */
public class StyledResponseLine {

    private String text;
    private ResponseType type;

    public StyledResponseLine(String text) {
        this(ResponseType.READING, text);
    }

    public StyledResponseLine(ResponseType type, String text) {
        this.text = StringUtils.trimAndStripQuotes(text);
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = StringUtils.trimAndStripQuotes(text);
    }

    public ResponseType getType() {
        return type;
    }

    public void setType(ResponseType type) {
        this.type = type;
    }

}
