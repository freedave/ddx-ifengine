/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command.defaults;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.CommandType;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.response.ResponseTokenBuilder;
import com.dodecahelix.ifengine.response.handlers.DescriptionPropertyHandler;
import com.dodecahelix.ifengine.response.handlers.TitlePropertyHandler;

public class LookCommandBuilder implements CommandBuilder {

    public static final String LOOK_REF_ID = "look-type:%s";

    @Override
    public void buildCommands(Environment environment) {
        // TODO Auto-generated method stub
        // Note: look has no executions, only a default Message -- the entity description
        CommandConfiguration commandConfig = environment.getConfiguration().getCommandConfiguration();

        String locationCmdDisplay = "Look";
        Command lookLocationCmd = buildLookCommand(EntityType.LOCATION,
                String.format(locationCmdDisplay, EntityType.LOCATION.name(), TitlePropertyHandler.TITLE));
        commandConfig.getLocationCommands().add(lookLocationCmd);

        Command lookNarratorCmd = buildLookCommand(EntityType.READER, "Look at myself");
        commandConfig.getNarratorCommands().add(lookNarratorCmd);

        String itemCmdDisplay = "Look at the " + ResponseTokenBuilder.buildToken("%s.this.%s");
        Command lookItemCmd = buildLookCommand(EntityType.ITEM,
                String.format(itemCmdDisplay, EntityType.ITEM.name(), TitlePropertyHandler.TITLE));
        commandConfig.getItemCommands().add(lookItemCmd);

        String massCommandDisplay = "Look at the " + ResponseTokenBuilder.buildToken("%s.this.%s");
        Command lookMassCmd = buildLookCommand(EntityType.MASS,
                String.format(massCommandDisplay, EntityType.MASS.name(), TitlePropertyHandler.TITLE));
        commandConfig.getMassCommands().add(lookMassCmd);

        String personCommandDisplay = "Look at " + ResponseTokenBuilder.buildToken("%s.this.%s");
        Command lookPersonCmd = buildLookCommand(EntityType.PERSON,
                String.format(personCommandDisplay, EntityType.PERSON.name(), TitlePropertyHandler.TITLE));
        commandConfig.getPeopleCommands().add(lookPersonCmd);
    }

    protected Command buildLookCommand(EntityType entityType, String display) {

        Command command = new Command(display, entityType);
        command.setReferenceId(String.format(LOOK_REF_ID, entityType.name().toLowerCase()));
        command.setCommandType(CommandType.LOOK);

        String lookMessage = ResponseTokenBuilder.buildToken("%s.this.%s");
        if (EntityType.READER.equals(entityType)) {
            lookMessage = ResponseTokenBuilder.buildToken("%s.%s");
        }
        command.setDefaultExecutionMessage(String.format(lookMessage, entityType, DescriptionPropertyHandler.DESCRIPTION));

        // no executions necessary, this is only printing a message
        command.setDefaultResults(new ExecutionSet());
        return command;
    }

}
