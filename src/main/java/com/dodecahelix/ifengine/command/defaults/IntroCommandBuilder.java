/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command.defaults;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.response.ResponseTokenBuilder;
import com.dodecahelix.ifengine.response.handlers.DescriptionPropertyHandler;
import com.dodecahelix.ifengine.util.StringUtils;

public class IntroCommandBuilder implements CommandBuilder {

    public static final String START_REF_ID = "command-start";

    public IntroCommandBuilder() {
    }

    @Override
    public void buildCommands(Environment environment) {

        Command command = new Command(BuiltinCommandBuilder.START_COMMAND_DISPLAY, EntityType.ENVIRONMENT);
        command.setReferenceId(START_REF_ID);

        // show the intro message and results of the look command
        StringBuffer intro = new StringBuffer(environment.getStoryContent().getIntroduction());

        // get the current location and look
        String currentLocationId = null;
        String narratorId = environment.getCurrentNarrator();
        for (Reader narrator : environment.getNarrators() ) {
            if (StringUtils.equalsIgnoreCase(narratorId, narrator.getId())) {
                currentLocationId = narrator.getCurrentLocation();
            }
        }

        for (Location location : environment.getLocations()) {
            if (StringUtils.equalsIgnoreCase(location.getId(), currentLocationId)) {
                intro.append("<br><action>> Look");
                intro.append("<br>");

                String lookMessage = ResponseTokenBuilder.buildToken("%s.this.%s");
                intro.append(String.format(lookMessage, EntityType.LOCATION.name(), DescriptionPropertyHandler.DESCRIPTION));
            }
        }

        command.setDefaultExecutionMessage(intro.toString());
        command.setDefaultResults(new ExecutionSet());

        environment.getConfiguration().getCommandConfiguration().setIntroCommand(command);
    }

}
