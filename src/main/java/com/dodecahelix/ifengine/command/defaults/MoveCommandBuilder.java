/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command.defaults;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.CommandType;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.entity.EntityStringBuilder;
import com.dodecahelix.ifengine.response.ResponseTokenBuilder;
import com.dodecahelix.ifengine.response.handlers.DescriptionPropertyHandler;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;

public class MoveCommandBuilder implements CommandBuilder {

    public static String EXIT_REF_ID = "exit-from:%s:to:%s";

    @Override
    public void buildCommands(Environment environment) {

        // build move commands for each room
        for (Location location : environment.getLocations()) {
            for (Exit exit : location.getExits()) {
                Command exitCommand = buildCommandForExit(location, exit, environment);
                location.addCommand(exitCommand);
            }
        }
    }

    public Command buildCommandForExit(Location fromLocation, Exit exit, Environment environment) {
        Command command = new Command(exit.getExitName(), EntityType.LOCATION);

        command.setReferenceId(String.format(EXIT_REF_ID, fromLocation.getId(), exit.getToLocationId()));
        command.setValidations(exit.getValidations());
        command.setCommandType(CommandType.MOVE);

        // exit location description in tokenized form (i.e; @location.kitchen.description@)
        String destinationDescription = EntityStringBuilder.buildEntityString(EntityType.LOCATION, exit.getToLocationId(), DescriptionPropertyHandler.DESCRIPTION);
        command.setDefaultExecutionMessage(ResponseTokenBuilder.buildToken(destinationDescription));

        // add an outcome to stop the command if the location is dark and the reader does not carry a light source
        Action tooDarkOutcome = new Action();
        tooDarkOutcome.setExecutionMessage(environment.getConfiguration().getMessageConfiguration().getTooDarkToMoveMessage());

        Condition targetLocationIsDark = new Condition(ConditionType.IS_DARK);
        targetLocationIsDark.getEntityHolder().setSubjectType(EntityType.LOCATION);
        targetLocationIsDark.getEntityHolder().setSubjectEntityId(exit.getToLocationId());
        tooDarkOutcome.addValidation(targetLocationIsDark);

        Condition notHoldingLight = new Condition(ConditionType.HOLDING_LIGHT_SOURCE);
        notHoldingLight.getEntityHolder().setSubjectType(EntityType.READER);
        notHoldingLight.setOperator(ConditionOperator.FALSE);
        tooDarkOutcome.addValidation(notHoldingLight);

        command.addOutcome(tooDarkOutcome);

        ExecutionSet moveExecutions = new ExecutionSet();

        Execution moveExecution = new Execution(ExecutionType.MOVE_ENTITY);
        moveExecution.getEntityHolder().setSubjectType(EntityType.READER);
        moveExecution.getEntityHolder().setTargetType(EntityType.LOCATION);
        moveExecution.getEntityHolder().setTargetEntityId(exit.getToLocationId());
        moveExecution.setTargetType(TargetType.ENTITY);
        moveExecution.setOperator(ExecutionOperator.SET);

        moveExecutions.getExecutions().add(moveExecution);
        command.setDefaultResults(moveExecutions);

        return command;
    }

}
