/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command.defaults;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.response.ResponseTokenBuilder;
import com.dodecahelix.ifengine.response.handlers.ShowWornItemsHandler;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;

public class WearAndRemoveCommandBuilder implements CommandBuilder {

    public static final String WEAR_ITEM_ENTITY_COMMAND_REF_ID = "wear-item:%s";
    public static final String REMOVE_ITEM_ENTITY_COMMAND_REF_ID = "remove-item:%s";

    public void buildCommands(Environment environment) {

        CommandConfiguration commandConfig = environment.getConfiguration().getCommandConfiguration();
        commandConfig.getItemCommands().add(buildWearCommand(environment));
        commandConfig.getItemCommands().add(buildRemoveCommand(environment));

        // add the command that the narrator uses to check what is being worn
        commandConfig.getNarratorCommands().add(buildWearingCommand());
    }

    private Command buildWearingCommand() {
        Command whatIsWornCommand = new Command(BuiltinCommandBuilder.WHAT_IS_WORN_COMMAND, EntityType.READER);

        String defaultResponseMessage = ResponseTokenBuilder.buildToken("reader." + ShowWornItemsHandler.WORN_ITEMS_TOKEN);
        whatIsWornCommand.setDefaultExecutionMessage(defaultResponseMessage);

        return whatIsWornCommand;
    }

    private Command buildWearCommand(Environment environment) {

        String thisItemToken = ResponseTokenBuilder.buildToken("item.this.title");
        Command wearCommand = new Command(BuiltinCommandBuilder.WEAR_COMMAND_DISPLAY + " " + thisItemToken, EntityType.ITEM);

        // add a validation to check if the item is wearable and nothing else is worn on that body part
        Condition isWearable = new Condition(ConditionType.IS_WEARABLE);
        isWearable.getEntityHolder().setSubjectType(EntityType.ITEM);
        isWearable.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        isWearable.setOperator(ConditionOperator.EQUALS);
        isWearable.getEntityHolder().setTargetType(EntityType.READER);
        isWearable.setTargetType(TargetType.ENTITY);
        wearCommand.addValidation(isWearable);

        // add a validation that the reader is carrying the item
        Condition isCarried = new Condition(ConditionType.OWNED);
        isCarried.getEntityHolder().setSubjectType(EntityType.ITEM);
        isCarried.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        isCarried.setOperator(ConditionOperator.EQUALS);
        wearCommand.addValidation(isCarried);

        Execution wearExecution = new Execution(ExecutionType.WEAR_ITEM);
        wearExecution.getEntityHolder().setSubjectType(EntityType.ITEM);
        wearExecution.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        wearExecution.setTargetType(TargetType.ENTITY);
        wearExecution.getEntityHolder().setTargetType(EntityType.READER);
        wearExecution.setOperator(ExecutionOperator.SET);

        wearCommand.addDefaultResult(wearExecution);
        wearCommand.setDefaultExecutionMessage("You put on the " + thisItemToken + ".");

        wearCommand.setReferenceId(String.format(WEAR_ITEM_ENTITY_COMMAND_REF_ID, "this"));

        return wearCommand;
    }

    private Command buildRemoveCommand(Environment environment) {
        String thisItemToken = ResponseTokenBuilder.buildToken("item.this.title");

        // add the remove or unwear command
        Command removeCommand = new Command(BuiltinCommandBuilder.UNWEAR_COMMAND_DISPLAY + " " + thisItemToken, EntityType.ITEM);

        // add a validation to check if you are wearing that item
        Condition isWorn = new Condition(ConditionType.IS_WEARING);
        isWorn.getEntityHolder().setSubjectType(EntityType.ITEM);
        isWorn.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        isWorn.setTargetType(TargetType.ENTITY);
        isWorn.getEntityHolder().setTargetType(EntityType.READER);
        isWorn.setOperator(ConditionOperator.TRUE);
        removeCommand.addValidation(isWorn);

        // TODO - add an outcome where removing the item will cause the reader to fall under carrying capacity

        // TODO - if this is a body option, add an outcome to check if someone else is in the room

        // add an execution to make the Worn property false
        Execution removeExecution = new Execution(ExecutionType.WEAR_ITEM);
        removeExecution.getEntityHolder().setSubjectType(EntityType.ITEM);
        removeExecution.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        removeExecution.setTargetType(TargetType.ENTITY);
        removeExecution.getEntityHolder().setTargetType(EntityType.READER);
        removeExecution.setOperator(ExecutionOperator.RESET);

        removeCommand.addDefaultResult(removeExecution);
        removeCommand.setDefaultExecutionMessage("You take off the " + thisItemToken + ".");

        removeCommand.setReferenceId(String.format(REMOVE_ITEM_ENTITY_COMMAND_REF_ID, "this"));

        return removeCommand;
    }

}
