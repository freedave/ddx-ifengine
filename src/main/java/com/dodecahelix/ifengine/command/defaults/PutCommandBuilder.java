/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command.defaults;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.response.ResponseTokenBuilder;
import com.dodecahelix.ifengine.response.handlers.PutTypeHandler;
import com.dodecahelix.ifengine.result.ExecutionType;

public class PutCommandBuilder implements CommandBuilder {

    public static final String PUT_ITEM_ENTITY_COMMAND_REF_ID = "put-subject:%s:target:%s";

    @Override
    public void buildCommands(Environment environment) {

        Command putCommand = new Command();

        // can put items into multiple local targets
        putCommand.setMultiTarget(true);

        putCommand.setCommandDisplay(BuiltinCommandBuilder.PUT_COMMAND_DISPLAY);

        // you must own the item
        Condition mustOwnItemCondition = new Condition(ConditionType.OWNED);
        mustOwnItemCondition.getEntityHolder().setSubjectType(EntityType.ITEM);
        mustOwnItemCondition.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        mustOwnItemCondition.setOperator(ConditionOperator.TRUE);
        mustOwnItemCondition.setTargetType(TargetType.ENTITY);
        mustOwnItemCondition.getEntityHolder().setTargetType(EntityType.READER);

        // test to see if target entity is a mass??

        // check to see if the mass has the capacity for the item
        Condition massHasRoomCondition = new Condition(ConditionType.TOO_BIG_TO_CARRY);
        massHasRoomCondition.getEntityHolder().setSubjectType(EntityType.ITEM);
        massHasRoomCondition.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        // FALSE = not too big to carry
        massHasRoomCondition.setOperator(ConditionOperator.FALSE);
        massHasRoomCondition.setTargetType(TargetType.ENTITY);
        massHasRoomCondition.getEntityHolder().setTargetType(EntityType.MASS);
        massHasRoomCondition.getEntityHolder().setTargetEntityId(EntityHolder.LOCAL_TARGET_ID);

        ConditionSet validations = new ConditionSet();
        validations.getConditions().add(mustOwnItemCondition);
        validations.getConditions().add(massHasRoomCondition);
        putCommand.setValidations(validations);

        // add an outcome that the item is NOT worn (TODO - shouldnt this look similar to carry?)
        Action isWornOutcome = new Action();
        isWornOutcome.setExecutionMessage(environment.getConfiguration().getMessageConfiguration().getCantDropItemThatIsWornMessage());
        Condition isWorn = new Condition(ConditionType.IS_WEARING);
        isWorn.getEntityHolder().setSubjectType(EntityType.ITEM);
        isWorn.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        isWorn.setTargetType(TargetType.ENTITY);
        isWorn.getEntityHolder().setTargetType(EntityType.READER);
        isWorn.setOperator(ConditionOperator.TRUE);
        isWornOutcome.addValidation(isWorn);
        putCommand.addOutcome(isWornOutcome);

        Execution putInsideExecution = new Execution(ExecutionType.CHOWN_ITEM);
        putInsideExecution.getEntityHolder().setSubjectType(EntityType.ITEM);
        putInsideExecution.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        putInsideExecution.getEntityHolder().setTargetType(EntityType.MASS);
        putInsideExecution.getEntityHolder().setTargetEntityId(EntityHolder.LOCAL_TARGET_ID);
        putInsideExecution.setTargetType(TargetType.ENTITY);

        ExecutionSet executions = new ExecutionSet();
        executions.getExecutions().add(putInsideExecution);
        putCommand.setDefaultResults(executions);

        String defaultMessage = "You put the "
                + ResponseTokenBuilder.buildToken("item.this.title")
                + " "
                + ResponseTokenBuilder.buildToken("mass.local." + PutTypeHandler.PUT_TYPE_PROPERTY)
                + " the "
                + ResponseTokenBuilder.buildToken("mass.local.title")
                + ".";

        putCommand.setDefaultExecutionMessage(defaultMessage);
        putCommand.setReferenceId(String.format(PUT_ITEM_ENTITY_COMMAND_REF_ID, "this", "local"));

        CommandConfiguration commandConfig = environment.getConfiguration().getCommandConfiguration();
        commandConfig.getItemCommands().add(putCommand);
    }

}
