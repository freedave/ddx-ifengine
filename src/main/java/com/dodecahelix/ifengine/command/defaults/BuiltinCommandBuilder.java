/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command.defaults;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.response.ResponseTokenBuilder;
import com.dodecahelix.ifengine.response.handlers.PutTypeHandler;

public class BuiltinCommandBuilder implements CommandBuilder {

    public static final String PUT_COMMAND_DISPLAY = "Put "
            + ResponseTokenBuilder.buildToken("item.this.title")
            + " "
            + ResponseTokenBuilder.buildToken("mass.local." + PutTypeHandler.PUT_TYPE_PROPERTY)
            + " "
            + ResponseTokenBuilder.buildToken("mass.local.title");

    public static final String LOOK_COMMAND_DISPLAY = "Look";
    public static final String TAKE_COMMAND_DISPLAY = "Take";
    public static final String WEAR_COMMAND_DISPLAY = "Wear";
    public static final String UNWEAR_COMMAND_DISPLAY = "Take off";
    public static final String START_COMMAND_DISPLAY = "Start the Game";

    // TODO - replace by making the worn items a selection in the option tree
    public static final String WHAT_IS_WORN_COMMAND = "What am I wearing?";
    public static final String DROP_COMMAND_DISPLAY = "Drop";

    private CommandBuilder lookCommandBuilder;
    private CommandBuilder moveCommandBuilder;
    private CommandBuilder takeCommandBuilder;
    private CommandBuilder wearCommandBuilder;
    private CommandBuilder introCommandBuilder;
    private CommandBuilder putCommandBuilder;

    public BuiltinCommandBuilder() {
        lookCommandBuilder = new LookCommandBuilder();
        moveCommandBuilder = new MoveCommandBuilder();
        takeCommandBuilder = new TakeAndDropCommandBuilder();
        wearCommandBuilder = new WearAndRemoveCommandBuilder();
        putCommandBuilder = new PutCommandBuilder();

        introCommandBuilder = new IntroCommandBuilder();
    }

    /**
     *   Add the default commands to the entities (i.e; look, move, get, drop)
     *     (if the command already exists, does NOT override)
     *
     * @param environment
     */
    public void buildCommands(Environment environment) {
        lookCommandBuilder.buildCommands(environment);
        moveCommandBuilder.buildCommands(environment);
        takeCommandBuilder.buildCommands(environment);
        wearCommandBuilder.buildCommands(environment);
        putCommandBuilder.buildCommands(environment);

        if (environment.getConfiguration().getCommandConfiguration().getIntroCommand()==null) {
            introCommandBuilder.buildCommands(environment);
        }
    }

}
