/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command.defaults;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.response.ResponseTokenBuilder;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;

public class TakeAndDropCommandBuilder implements CommandBuilder {

    public static final String TAKE_ITEM_ENTITY_COMMAND_REF_ID = "take-item:%s";
    public static final String DROP_ITEM_ENTITY_COMMAND_REF_ID = "drop-item:%s";

    @Override
    public void buildCommands(Environment environment) {
        CommandConfiguration commandConfig = environment.getConfiguration().getCommandConfiguration();
        commandConfig.getItemCommands().add(buildTakeCommand(environment));
        commandConfig.getItemCommands().add(buildDropCommand(environment));
    }

    private Command buildDropCommand(Environment environment) {

        String thisItemToken = ResponseTokenBuilder.buildToken("item.this.title");
        Command command = new Command(BuiltinCommandBuilder.DROP_COMMAND_DISPLAY + " " + thisItemToken, EntityType.ITEM);

        // add condition that person is carrying the item
        Condition carriedCondition = new Condition(ConditionType.OWNED);
        carriedCondition.getEntityHolder().setSubjectType(EntityType.ITEM);
        carriedCondition.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        carriedCondition.setOperator(ConditionOperator.EQUALS);
        command.addValidation(carriedCondition);

        // add confition that checks if item is NOT worn
        Condition isWorn = new Condition(ConditionType.IS_WEARING);
        isWorn.getEntityHolder().setSubjectType(EntityType.ITEM);
        isWorn.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        isWorn.setTargetType(TargetType.ENTITY);
        isWorn.getEntityHolder().setTargetType(EntityType.READER);
        isWorn.setOperator(ConditionOperator.FALSE);
        command.addValidation(isWorn);

        command.setDefaultExecutionMessage("You drop the " + thisItemToken + ".");

        // exeution result : drop the item on the floor
        Execution dropExecution = new Execution(ExecutionType.CHOWN_ITEM);
        dropExecution.setOperator(ExecutionOperator.SET);
        dropExecution.getEntityHolder().setSubjectType(EntityType.ITEM);
        dropExecution.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        dropExecution.getEntityHolder().setTargetType(EntityType.CURRENT_LOCATION);
        dropExecution.setTargetType(TargetType.ENTITY);
        command.addDefaultResult(dropExecution);

        command.setReferenceId(String.format(DROP_ITEM_ENTITY_COMMAND_REF_ID, "this"));

        return command;
    }

    private Command buildTakeCommand(Environment environment) {

        String thisItemToken = ResponseTokenBuilder.buildToken("item.this.title");
        Command command = new Command(BuiltinCommandBuilder.TAKE_COMMAND_DISPLAY + " " + thisItemToken, EntityType.ITEM);

        // add condition that person can see the item
        Condition visibilityCondition = new Condition(ConditionType.VISIBILE);
        visibilityCondition.setOperator(ConditionOperator.EQUALS);
        visibilityCondition.getEntityHolder().setSubjectType(EntityType.ITEM);
        visibilityCondition.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        command.addValidation(visibilityCondition);

        command.setDefaultExecutionMessage("You pick up the " + thisItemToken + ".");

        // result : take the item
        ExecutionSet takeExecutions = new ExecutionSet();
        Execution takeExecution = new Execution(ExecutionType.CHOWN_ITEM);
        takeExecution.getEntityHolder().setSubjectType(EntityType.ITEM);
        takeExecution.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);

        takeExecutions.getExecutions().add(takeExecution);
        command.setDefaultResults(takeExecutions);

        // outcomes: too big

        // if you hit the limit you can carry, then send a message that your hands are full
        Action carrySizeTooBigOutcome = new Action();
        String tooBigToCarryMessage = environment.getConfiguration().getMessageConfiguration().getTooBigToCarryMessage();
        carrySizeTooBigOutcome.setExecutionMessage(tooBigToCarryMessage);

        Condition checkSizeLimit = new Condition(ConditionType.TOO_BIG_TO_CARRY);
        checkSizeLimit.setOperator(ConditionOperator.TRUE);
        checkSizeLimit.getEntityHolder().setSubjectType(EntityType.ITEM);
        checkSizeLimit.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        carrySizeTooBigOutcome.addValidation(checkSizeLimit);

        command.addOutcome(carrySizeTooBigOutcome);

        command.setReferenceId(String.format(TAKE_ITEM_ENTITY_COMMAND_REF_ID, "this"));

        return command;
    }

}
