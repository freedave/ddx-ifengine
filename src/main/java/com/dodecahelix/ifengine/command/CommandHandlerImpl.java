/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.response.StyledResponseFilter;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.action.ActionHandler;
import com.dodecahelix.ifengine.action.ActionResult;
import com.dodecahelix.ifengine.choice.ChoiceResult;

public class CommandHandlerImpl implements CommandHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(CommandHandlerImpl.class);

    private ActionHandler actionHandler;
    private ExecutionResultHandler defaultResultHandler;
    private StyledResponseFilter responseFilter;

    public CommandHandlerImpl(ActionHandler actionHandler,
            ExecutionResultHandler resultHandler,
            StyledResponseFilter responseFilter) {

        this.actionHandler = actionHandler;
        this.defaultResultHandler = resultHandler;
        this.responseFilter = responseFilter;
    }

    @Override
    public ChoiceResult handle(Command command, String subjectEntityId, String localTargetId) {

        ChoiceResult result = new ChoiceResult();
        result.setFailure(true);
        // whether or not an alternative outcome has executed
        boolean hasAlternativeOutcome = false;
        LOGGER.debug("Handling command : {}", command);

        if (command.getOutcomes()!=null && !command.getOutcomes().isEmpty()) {
            LOGGER.debug("Handling command with outcomes");

            // go through each alternative outcome and test to see if it passes validation check
            // if validation is true, this becomes the result (default is not executed)
            for (Action alternative : command.getOutcomes()) {
                ActionResult actionResult = actionHandler.handleAction(alternative, subjectEntityId, localTargetId);
                boolean success = actionResult.isSuccess();
                if (success) {
                    result.setFailure(false);
                    hasAlternativeOutcome = true;

                    result.getResponseLines().addAll(actionResult.getResponseLines());

                    // for a solo action, ignore all other actions
                    if (alternative.isSolo()) {
                        break;
                    }
                }
            }
        } else {
            // no alternative outcomes - automatic success
            result.setFailure(false);
        }

        // if there are no valid alternative outcomes, then process the default result
        if (!hasAlternativeOutcome) {
            // not alternatives have fired - execute the default
            result.getResponseLines().addAll(responseFilter.filterResponse(command.getDefaultExecutionMessage(), subjectEntityId, localTargetId));

            // execute, and filter any responses in the default executions
            ExecutionResults defaultResult = defaultResultHandler.execute(command.getDefaultResults(), subjectEntityId, localTargetId);
            List<String> defaultResponses = defaultResult.getMessages();
            for (String defaultResponseMessage : defaultResponses) {
                result.getResponseLines().addAll(responseFilter.filterResponse(defaultResponseMessage, subjectEntityId, localTargetId));
            }

            // now, perform any followup actions, and filter their response as well
            for (Action followupAction : defaultResult.getFollowupActions()) {
                ActionResult followupResult = actionHandler.handleAction(followupAction, subjectEntityId, localTargetId);
                result.getResponseLines().addAll(followupResult.getResponseLines());
            }
        }

        return result;
    }

}
