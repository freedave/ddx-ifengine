/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.choice.ChoiceResult;

public interface CommandHandler {

    /**
     *
     * evaluate the appropriate response to a command (and execute any data model changes)
     *
     * @param command
     * @param subjectEntityId - This uniquely identifies the subject for conditions and executions that specify a subject entity id of "this"
     * @param localTargetId - This uniquely identifies the target of conditions and executions that specify a target entity id of "local"
     *
     * @return Message to be sent to the console
     */
    public ChoiceResult handle(Command command, String subjectEntityId, String localTargetId);

}
