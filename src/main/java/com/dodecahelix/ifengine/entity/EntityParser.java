/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.entity;

import java.util.StringTokenizer;

import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;

public class EntityParser {

    public static final String ENTITY_TOKEN = ".";

    public enum EntityOperand {
        SUBJECT,
        TARGET
    }

    public static EntityHolder parseEntity(String entityElement, EntityOperand operand) throws ParseException {
        EntityHolder parsed = new EntityHolder();

        return appendEntity(entityElement, parsed, operand);
    }

    public static EntityHolder appendEntity(String entityElement, EntityHolder parsed, EntityOperand operand) throws ParseException {

        StringTokenizer subjectTokenizer = new StringTokenizer(entityElement, ENTITY_TOKEN);

        // first element is the entity type
        String entityTypeAbbr = subjectTokenizer.nextToken();
        EntityType type = EntityType.getTypeByAbbreviation(entityTypeAbbr);
        if (type==null) {
            throw new ParseException("Invalid subject string : " + entityElement + "  No entity type found for " + entityTypeAbbr);
        }

        String entityId = null;
        if (EntityType.ITEM.equals(type)
                || EntityType.NARRATOR.equals(type)
                || EntityType.LOCATION.equals(type)
                || EntityType.MASS.equals(type)
                || EntityType.PERSON.equals(type)) {

            // TODO - throw an invalid exception if no more tokens
            entityId = subjectTokenizer.nextToken();
        }

        // if there is another token, it is for the property of this entity
        String entityProperty = null;
        if (subjectTokenizer.hasMoreTokens()) {
            entityProperty = subjectTokenizer.nextToken();
        }

        // the operand specifies which type of entity it being referenced
        if (operand.equals(EntityOperand.SUBJECT)) {
            parsed.setSubjectType(type);
            parsed.setSubjectEntityId(entityId);
            parsed.setSubjectProperty(entityProperty);
        }
        if (operand.equals(EntityOperand.TARGET)) {
            parsed.setTargetType(type);
            parsed.setTargetEntityId(entityId);
            parsed.setTargetProperty(entityProperty);
        }

        return parsed;
    }

}


