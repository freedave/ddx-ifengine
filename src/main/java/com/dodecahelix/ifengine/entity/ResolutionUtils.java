/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.entity;

import com.dodecahelix.ifengine.data.EntityHolder;


public class ResolutionUtils {

    public static String getSubjectEntityId(EntityHolder entityHolder, String parentEntityId) {
        String subjectId = entityHolder.getSubjectEntityId();
        if (EntityHolder.THIS_SUBJECT_ID.equalsIgnoreCase(subjectId)) {
            subjectId = parentEntityId;
        }
        return subjectId;
    }

    public static String getTargetEntityId(EntityHolder entityHolder, String localTargetId) {
        String targetId = entityHolder.getTargetEntityId();
        if (EntityHolder.LOCAL_TARGET_ID.equalsIgnoreCase(targetId)) {
            targetId = localTargetId;
        }
        return targetId;
    }

}
