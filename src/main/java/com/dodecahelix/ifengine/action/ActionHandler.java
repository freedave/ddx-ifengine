/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.action;

import com.dodecahelix.ifengine.choice.ChoiceResult;
import com.dodecahelix.ifengine.data.Action;

/**
 *  Handles both "global actions' and regular command actions (outcomes)
 *
 */
public interface ActionHandler {

    /**
     * Process entity actions that occur independent of the command, and append to the result.
     *
     * @param result
     * @return
     */
    public ChoiceResult processGlobalActions(ChoiceResult result);

    /**
     * Handle an 'action', which can be a command outcome or an independent action.
     *
     * @param action
     * @param subjectEntityId - This uniquely identifies the subject for conditions and executions that specify a subject entity id of "this"
     * @param localTargetId - This uniquely identifies the target of conditions and executions that specify a target entity id of "local"
     * @return
     */
    public ActionResult handleAction(Action action, String subjectEntityId, String localTargetId);

}
