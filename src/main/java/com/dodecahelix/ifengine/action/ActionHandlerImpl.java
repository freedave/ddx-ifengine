/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.action;

import java.util.List;

import com.dodecahelix.ifengine.choice.ChoiceResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.response.StyledResponseFilter;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.util.StringUtils;

public class ActionHandlerImpl implements ActionHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ActionHandlerImpl.class);

    private EnvironmentDAO environmentDao;
    private ConditionDetermination conditionDetermination;
    private ExecutionResultHandler resultHandler;
    private StyledResponseFilter styledResponseFilter;

    public ActionHandlerImpl(EnvironmentDAO dao,
            ConditionDetermination conditionDetermination,
            ExecutionResultHandler resultHandler,
            StyledResponseFilter styledResponseFilter) {

        this.conditionDetermination = conditionDetermination;
        this.environmentDao = dao;
        this.resultHandler = resultHandler;
        this.styledResponseFilter = styledResponseFilter;
    }

    @Override
    public ChoiceResult processGlobalActions(ChoiceResult result) {

        Environment environment = environmentDao.getEnvironment();

        // TODO - get all local targets, and handle actions against them?
        // Set<Entity> locals = environmentDao.getLocalTargets();

        // process environment actions
        result = handleActions(result, environment.getActions(), null, null);

        // process actions for current reader only (no other narrators)
        Reader reader = environmentDao.getReader();
        result = handleActions(result, reader.getActions(), reader.getId(), null);

        // process actions for people in the current location
        for (Person person : environmentDao.getPeopleForCurrentLocation()) {
            result = handleActions(result, person.getActions(), person.getId(), null);
        }

        // process actions for current location
        Location currentLocation = environmentDao.getCurrentLocation();
        result = handleActions(result, currentLocation.getActions(), currentLocation.getId(), null);

        // process actions for masses in the current location
        for (Mass mass : environmentDao.getMassesForCurrentLocation()) {
            result = handleActions(result, mass.getActions(), mass.getId(), null);
        }

        // process actions for items (in inventory)
        for (Item item : environmentDao.getInventory()) {
            result = handleActions(result, item.getActions(), item.getId(), null);
        }

        // process actions for items (in location)
        for (Item item : environmentDao.getContentsForEntity(EntityType.LOCATION, currentLocation.getId())) {
            result = handleActions(result, item.getActions(), item.getId(), null);
        }

        return result;
    }

    private ChoiceResult handleActions(ChoiceResult result, List<Action> actions, String parentEntityId, String localTargetId) {

        for (Action action : actions) {

            LOGGER.info("processing turn action : " + action);

            ActionResult actionResult = handleAction(action, parentEntityId, localTargetId);
            if (actionResult.isSuccess()) {
                LOGGER.info("action is successful!");
                for (StyledResponseLine responseLine : actionResult.getResponseLines()) {
                    result.addResponseLine(responseLine);
                }

                // if this is a solo
                if (action.isSolo()) {
                    break;
                }
            }
        }

        return result;
    }

    @Override
    public ActionResult handleAction(Action consequence, String subjectEntityId, String localTargetId) {

        ActionResult result = new ActionResult();

        boolean success = conditionDetermination.determineSet(consequence.getValidations(), subjectEntityId, localTargetId);
        result.setSuccess(success);

        if (success) {
            LOGGER.debug("action is successful : {}", consequence.getExecutionMessage());

            if (!StringUtils.isEmpty(consequence.getExecutionMessage())) {
                result.getResponseLines().addAll(styledResponseFilter.filterResponse(consequence.getExecutionMessage(), subjectEntityId, localTargetId));
            }

            // executions don't normally return responses, but append if they do
            ExecutionResults executionResult = resultHandler.execute(consequence.getResults(), subjectEntityId, localTargetId);
            for (String executionMessage : executionResult.getMessages()) {
                result.getResponseLines().addAll(styledResponseFilter.filterResponse(executionMessage, subjectEntityId, localTargetId));
            }

            // perform followup actions (i.e; property change events)
            for (Action followupAction : executionResult.getFollowupActions()) {
                ActionResult followupActionResult = handleAction(followupAction, subjectEntityId, localTargetId);
                for (StyledResponseLine followupResponseLine : followupActionResult.getResponseLines()) {
                    result.addResponseLine(followupResponseLine);
                }
            }
        }

        result.setSuccess(success);
        return result;
    }

}
