/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.options.item.DialogListItem;
import com.dodecahelix.ifengine.options.item.GroupListItem;
import com.dodecahelix.ifengine.options.item.ListItem;

public class DialogTreeBuilder {

    private final static Logger LOGGER = LoggerFactory.getLogger(DialogTreeBuilder.class);

    private EnvironmentDAO dao;
    private ConditionDetermination conditionDetermination;

    public DialogTreeBuilder(EnvironmentDAO dao, ConditionDetermination conditionDetermination) {
        this.dao = dao;
        this.conditionDetermination = conditionDetermination;
    }

    public GroupListItem buildDialogTree(List<Dialog> dialogs, String personId) {
        LOGGER.debug("building (sub)dialog tree for person {}", personId);
        GroupListItem dialogTree = new GroupListItem(ListGroup.PERSON, "Dialogs");

        List<ListItem> dialogItems = getItemsForGroup(dialogs, personId);
        dialogTree.setChildListItems(dialogItems);

        return dialogTree;
    }

    public List<ListItem> getItemsForGroup(List<Dialog> dialogs, String personId) {

        List<ListItem> commandOptions = new ArrayList<ListItem>();

        for (Dialog dialog : dialogs) {
            if (dialog.getTopic()==null) {
                if (!dialog.isDisabled() && conditionDetermination.determineSet(dialog.getValidations(), personId, null)) {
                    commandOptions.add(new DialogListItem(dialog, personId));
                }
            } else {
                String topic = dialog.getTopic();
                if (!dialog.isDisabled() && dao.getReader().getKnownTopics().contains(topic)) {
                    if (conditionDetermination.determineSet(dialog.getValidations(), personId, null)) {
                        commandOptions.add(new DialogListItem(dialog, personId));
                    }
                }
            }
        }

        return commandOptions;
    }
}
