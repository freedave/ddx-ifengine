/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options.build;

import java.util.List;
import java.util.Set;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.options.item.CommandListItem;
import com.dodecahelix.ifengine.options.item.ListItem;
import com.dodecahelix.ifengine.response.DynamicTextFilter;
import com.dodecahelix.ifengine.response.DynamicTextFilterImpl;

public abstract class EntityCommandListBuilder {

    protected EnvironmentDAO dao;
    protected ConditionDetermination conditionDetermination;
    protected DynamicTextFilter commandDisplayFilter;

    public EntityCommandListBuilder(EnvironmentDAO dao, ConditionDetermination conditionDetermination) {
        this.dao = dao;
        this.conditionDetermination = conditionDetermination;

        commandDisplayFilter = new DynamicTextFilterImpl(dao);
    }

    /**
     *   Adds the command to the options list (as a CommandListItem)
     *
     * @param commandOptions - current options list
     * @param command - new command to add to the options list
     * @param entity - entity which owns this command
     */
    protected void appendCommandOptions(List<ListItem> commandOptions, Command command, Entity entity) {

        if (command.isMultiTarget()) {

            Set<Entity> targets = filterTargets();
            for (Entity target : targets) {
                if (conditionDetermination.determineSet(command.getValidations(), entity.getId(), target.getId())) {

                    // filter the command display (this can include dynamic tokenized values)
                    String display;
                    try {
                        display = commandDisplayFilter.filter(command.getCommandDisplay(), entity.getId(), target.getId());
                    } catch (Exception e) {
                        e.printStackTrace();
                        display = command.getCommandDisplay();
                    }
                    commandOptions.add(new CommandListItem(command, display, entity.getId(), target.getId()));
                }
            }

        } else { // single target (simple case)

            if (conditionDetermination.determineSet(command.getValidations(), entity.getId(), null)) {

                // filter the command display (this can include dynamic tokenized values)
                String display;
                try {
                    display = commandDisplayFilter.filter(command.getCommandDisplay(), entity.getId(), entity.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                    display = command.getCommandDisplay();
                }

                commandOptions.add(new CommandListItem(command, display, entity.getId(), null));
            }
        }
    }

    private Set<Entity> filterTargets() {
        Set<Entity> targets = dao.getLocalTargets();
        // TODO - use command.getTargetFilters() to filter out targets

        return targets;
    }
}
