/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options.build;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.CommandType;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.options.OptionListBuilder;
import com.dodecahelix.ifengine.options.ListGroup;
import com.dodecahelix.ifengine.options.item.GroupListItem;
import com.dodecahelix.ifengine.options.item.ListItem;
import com.dodecahelix.ifengine.util.StringUtils;

public class RoomOptionListBuilder extends EntityCommandListBuilder implements OptionListBuilder {

    public RoomOptionListBuilder(EnvironmentDAO dao, ConditionDetermination conditionDetermination) {
        super(dao, conditionDetermination);
    }

    @Override
    public List<ListItem> getItemsForGroup(String groupId) {

        List<ListItem> commandOptions = new ArrayList<ListItem>();

        boolean frozenInTime = dao.getEnvironment().isFrozenInTime();

        // maintain the list of references so that entity-specific commands can override entity-type commands
        List<String> overrides = new ArrayList<String>();

        Location currentLocation = dao.getCurrentLocation();
        for (Command command : currentLocation.getCommands()) {
            if (!StringUtils.isEmpty(command.getOverrideId())) {
                overrides.add(command.getOverrideId());
            }

            boolean enoughTimeToAct = (!frozenInTime || command.getTimeToExecute()==0);
            if ( enoughTimeToAct && (CommandType.MOVE != command.getCommandType())) {
                this.appendCommandOptions(commandOptions, command, currentLocation);
            }
        }

        CommandConfiguration commandConfiguration = dao.getEnvironment().getConfiguration().getCommandConfiguration();
        for (Command command : commandConfiguration.getLocationCommands()) {
            boolean enoughTimeToAct = (!frozenInTime || command.getTimeToExecute()==0);
            boolean overridden = overrides.contains(command.getReferenceId());
            if ( !overridden && enoughTimeToAct && (CommandType.MOVE != command.getCommandType())) {
                this.appendCommandOptions(commandOptions, command, currentLocation);
            }
        }

        for (Mass mass : dao.getMassesForCurrentLocation()) {
            // TODO - if mass is disabled, don't show it in the list?
            commandOptions.add(new GroupListItem(ListGroup.MASS, mass.getTitle() + "...", mass.getId()));
        }

        // loose items in location - should these be in a pile of stuff ListGroup?
        Set<Item> looseItems = dao.getContentsForEntity(EntityType.LOCATION, currentLocation.getId());
        if (!looseItems.isEmpty()) {
            commandOptions.add(new GroupListItem(ListGroup.LOOSE_ITEMS, "Items on the ground...", currentLocation.getId()));
        }

        return commandOptions;
    }

}
