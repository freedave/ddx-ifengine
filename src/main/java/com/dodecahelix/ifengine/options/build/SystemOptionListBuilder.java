/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options.build;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.options.OptionListBuilder;
import com.dodecahelix.ifengine.options.item.CommandListItem;
import com.dodecahelix.ifengine.options.item.ListItem;

public class SystemOptionListBuilder implements OptionListBuilder {

    private EnvironmentDAO dao;
    private ConditionDetermination conditionDetermination;

    public SystemOptionListBuilder(EnvironmentDAO dao, ConditionDetermination conditionDetermination) {
        this.dao = dao;
        this.conditionDetermination = conditionDetermination;
    }

    @Override
    public List<ListItem> getItemsForGroup(String groupId) {

        List<ListItem> items = new ArrayList<ListItem>();

        // save, load, quit, exit, help
        for (Command command : dao.getEnvironment().getCommands()) {
            // only list the command if it passes validation checks
            if (conditionDetermination.determineSet(command.getValidations(), null, null)) {
                items.add(new CommandListItem(command, command.getCommandDisplay(), null, null));
            }
        }

        return items;
    }


}
