/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options.build;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.options.OptionListBuilder;
import com.dodecahelix.ifengine.options.ListGroup;
import com.dodecahelix.ifengine.options.item.GroupListItem;
import com.dodecahelix.ifengine.options.item.ListItem;

public class LooseItemsOptionListBuilder extends EntityCommandListBuilder implements OptionListBuilder {

    public LooseItemsOptionListBuilder(EnvironmentDAO dao, ConditionDetermination conditionDetermination) {
        super(dao, conditionDetermination);
    }

    @Override
    public List<ListItem> getItemsForGroup(String groupId) {

        List<ListItem> commandOptions = new ArrayList<ListItem>();

        Location currentLocation = dao.getCurrentLocation();
        for (Item item : dao.getContentsForEntity(EntityType.LOCATION, currentLocation.getId())) {
            commandOptions.add(new GroupListItem(ListGroup.ITEM, item.getTitle() + "...", item.getId()));
        }

        return commandOptions;
    }

}
