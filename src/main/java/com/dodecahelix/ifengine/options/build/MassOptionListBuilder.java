/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options.build;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.options.OptionListBuilder;
import com.dodecahelix.ifengine.options.ListGroup;
import com.dodecahelix.ifengine.options.item.GroupListItem;
import com.dodecahelix.ifengine.options.item.ListItem;
import com.dodecahelix.ifengine.util.StringUtils;

public class MassOptionListBuilder extends EntityCommandListBuilder implements OptionListBuilder {

    public MassOptionListBuilder(EnvironmentDAO dao, ConditionDetermination conditionDetermination) {
        super(dao, conditionDetermination);
    }

    @Override
    public List<ListItem> getItemsForGroup(String groupId) {

        List<ListItem> commandOptions = new ArrayList<ListItem>();

        boolean frozenInTime = dao.getEnvironment().isFrozenInTime();

        // maintain the list of references so that entity-specific commands can override entity-type commands
        List<String> overrides = new ArrayList<String>();

        Mass mass = (Mass) dao.getEntityById(EntityType.MASS, groupId);
        for (Command command : mass.getCommands()) {
            if (!StringUtils.isEmpty(command.getOverrideId())) {
                overrides.add(command.getOverrideId());
            }
            boolean enoughTimeToAct = (!frozenInTime || command.getTimeToExecute()==0);
            if (enoughTimeToAct) {
                this.appendCommandOptions(commandOptions, command, mass);
            }
        }

        CommandConfiguration commandConfiguration = dao.getEnvironment().getConfiguration().getCommandConfiguration();
        for (Command command : commandConfiguration.getMassCommands()) {
            boolean enoughTimeToAct = (!frozenInTime || command.getTimeToExecute()==0);
            boolean overridden = overrides.contains(command.getReferenceId());
            if (!overridden && enoughTimeToAct) {
                this.appendCommandOptions(commandOptions, command, mass);
            }
        }

        for (String contentItemId : mass.getContents()) {
            Item contentItem = (Item) dao.getEntityById(EntityType.ITEM, contentItemId);
            commandOptions.add(new GroupListItem(ListGroup.ITEM, contentItem.getTitle(), contentItem.getId()));
        }

        return commandOptions;
    }


}
