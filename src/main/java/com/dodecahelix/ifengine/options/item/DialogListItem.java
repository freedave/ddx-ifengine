/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options.item;

import com.dodecahelix.ifengine.data.Dialog;


public class DialogListItem implements ListItem {

    private Dialog dialog;
    private String personId;

    public static String DIALOG_COMMAND_ID = "dialog";

    public DialogListItem(Dialog dialog, String personId) {
        this.dialog = dialog;
    }

    @Override
    public String getItemId() {
        return DIALOG_COMMAND_ID + "." + dialog.getOptionDisplay();
    }

    @Override
    public String getLabel() {
        return dialog.getOptionDisplay();
    }

    @Override
    public ListItemIcon getIcon() {
        return ListItemIcon.TALK;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public String getPersonId() {
        return personId;
    }

}
