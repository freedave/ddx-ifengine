/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options.item;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.options.ListGroup;
import com.dodecahelix.ifengine.util.StringUtils;

public class GroupListItem implements ListItem {

    private String label;
    private ListGroup groupType;
    private String groupEntityId;

    private List<ListItem> childListItems = new ArrayList<ListItem>();

    public GroupListItem(ListGroup group, String label) {
        this.groupType = group;
        this.label = label;
    }

    public GroupListItem(ListGroup group, String label, String groupEntityId) {
        this.groupType = group;
        this.label = StringUtils.capitalize(label);
        this.groupEntityId = groupEntityId;
    }

    @Override
    public String getLabel() {
        return label;
    }

    public String toString() {
        return label;
    }

    @Override
    public ListItemIcon getIcon() {
        return ListItemIcon.STAR;
    }

    public ListGroup getGroupType() {
        return groupType;
    }

    public String getGroupEntityId() {
        return groupEntityId;
    }

    public void setGroupEntityId(String groupEntityId) {
        this.groupEntityId = groupEntityId;
    }

    public List<ListItem> getChildListItems() {
        return childListItems;
    }

    public void setChildListItems(List<ListItem> childListItems) {
        this.childListItems = childListItems;
    }

    @Override
    public String getItemId() {
        return groupEntityId + "." + label;
    }

}
