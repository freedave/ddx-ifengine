/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.options.item.GroupListItem;
import com.dodecahelix.ifengine.options.item.ListItem;

public class OptionTreeBuilder {

    private final static Logger LOGGER = LoggerFactory.getLogger(OptionTreeBuilder.class);

    private OptionTreeBuilderFactory builderFactory;

    public OptionTreeBuilder(OptionTreeBuilderFactory builderFactory) {
        this.builderFactory = builderFactory;
    }

    public GroupListItem buildOptionTree() {
        GroupListItem commandTree = new GroupListItem(ListGroup.TOP, "Actions");

        List<ListItem> groupItems = buildGroupLevel(ListGroup.TOP, null);
        commandTree.setChildListItems(groupItems);

        return commandTree;
    }

    private List<ListItem> buildGroupLevel(ListGroup group, String groupId) {

        OptionListBuilder builder = builderFactory.getBuilderForGroupType(group);
        List<ListItem> groupItems = builder.getItemsForGroup(groupId);
        for (ListItem listItem : groupItems) {
            LOGGER.trace("Adding list item \"{}\" to group {} : {}", listItem.getLabel(), group, groupId);

            if (listItem instanceof GroupListItem) {
                GroupListItem groupListItem = (GroupListItem)listItem;

                List<ListItem> childListItems = buildGroupLevel(groupListItem.getGroupType(), groupListItem.getGroupEntityId());
                groupListItem.setChildListItems(childListItems);
            }
        }

        return groupItems;
    }
}
