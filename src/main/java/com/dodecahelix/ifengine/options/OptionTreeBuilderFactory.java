/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.options.build.InventoryOptionListBuilder;
import com.dodecahelix.ifengine.options.build.ItemOptionListBuilder;
import com.dodecahelix.ifengine.options.build.LooseItemsOptionListBuilder;
import com.dodecahelix.ifengine.options.build.MassOptionListBuilder;
import com.dodecahelix.ifengine.options.build.MoveOptionListBuilder;
import com.dodecahelix.ifengine.options.build.PersonOptionListBuilder;
import com.dodecahelix.ifengine.options.build.ReaderOptionListBuilder;
import com.dodecahelix.ifengine.options.build.RoomOptionListBuilder;
import com.dodecahelix.ifengine.options.build.SystemOptionListBuilder;
import com.dodecahelix.ifengine.options.build.TopLevelOptionListBuilder;

public class OptionTreeBuilderFactory {

    private OptionListBuilder topLevelOptionListBuilder;
    private OptionListBuilder roomOptionListBuilder;
    private OptionListBuilder inventoryOptionListBuilder;
    private OptionListBuilder itemOptionListBuilder;
    private OptionListBuilder massOptionListBuilder;
    private OptionListBuilder personOptionListBuilder;
    private OptionListBuilder readerOptionListBuilder;
    private OptionListBuilder systemOptionListBuilder;
    private OptionListBuilder moveOptionListBuilder;
    private OptionListBuilder looseItemsOptionListBuilder;

    public OptionTreeBuilderFactory(EnvironmentDAO dao, ConditionDetermination conditionDetermination) {

        topLevelOptionListBuilder = new TopLevelOptionListBuilder(dao, conditionDetermination);
        roomOptionListBuilder = new RoomOptionListBuilder(dao, conditionDetermination);
        inventoryOptionListBuilder = new InventoryOptionListBuilder(dao);
        itemOptionListBuilder = new ItemOptionListBuilder(dao, conditionDetermination);
        massOptionListBuilder = new MassOptionListBuilder(dao, conditionDetermination);
        personOptionListBuilder = new PersonOptionListBuilder(dao, conditionDetermination);
        readerOptionListBuilder = new ReaderOptionListBuilder(dao, conditionDetermination);
        systemOptionListBuilder = new SystemOptionListBuilder(dao, conditionDetermination);
        moveOptionListBuilder = new MoveOptionListBuilder(dao, conditionDetermination);
        looseItemsOptionListBuilder = new LooseItemsOptionListBuilder(dao, conditionDetermination);

    }

    public OptionListBuilder getBuilderForGroupType(ListGroup groupType) {
        OptionListBuilder builder = null;

        switch (groupType) {
            case TOP : builder = topLevelOptionListBuilder; break;
            case ROOM : builder = roomOptionListBuilder; break;
            case INVENTORY : builder = inventoryOptionListBuilder; break;
            case ITEM : builder = itemOptionListBuilder; break;
            case MASS : builder = massOptionListBuilder; break;
            case PERSON : builder = personOptionListBuilder; break;
            case READER : builder = readerOptionListBuilder; break;
            case SYSTEM :builder = systemOptionListBuilder; break;
            case MOVE : builder = moveOptionListBuilder; break;
            case LOOSE_ITEMS : builder = looseItemsOptionListBuilder; break;
        }

        return builder;
    }

}
