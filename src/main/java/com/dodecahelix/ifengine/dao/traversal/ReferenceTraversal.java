/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.dao.traversal;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   Parses the environment tree, looking for data objects with the given reference ID
 *
 */
public class ReferenceTraversal extends DefaultEnvironmentTraversal {

    private String referenceId;
    private Command foundCommand;
    private Dialog foundDialog;

    public void traverseEnvironment(Environment environment, String referenceId) {
        this.referenceId = referenceId;
        super.traverseEnvironment(environment);
    }

    @Override
    public void processCommand(Command command) {
        if (StringUtils.equalsIgnoreCase(referenceId, command.getReferenceId())) {
            foundCommand = command;
        }
    }

    @Override
    public void processDialog(Dialog dialog) {
        if (StringUtils.equalsIgnoreCase(referenceId, dialog.getReferenceId())) {
            foundDialog = dialog;
        }
    }

    public Command getFoundCommand() {
        return foundCommand;
    }

    public Dialog getFoundDialog() {
        return foundDialog;
    }

}
