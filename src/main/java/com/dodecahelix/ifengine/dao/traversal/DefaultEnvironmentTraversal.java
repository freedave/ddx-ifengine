/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.dao.traversal;

import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Schedule;

/**
 *   Simple implementation of Environment Traversal that does nothing
 *   (so that any subclass will not need to implement all the abstract methods of EnvironmentTraversal)
 *
 */
public class DefaultEnvironmentTraversal extends EnvironmentTraversal {

    @Override
    public void processEnvironment(Environment environment) {
    }

    @Override
    public void processLocation(Location environment) {
    }

    @Override
    public void processItem(Item item) {
    }

    @Override
    public void processPerson(Person person) {
    }

    @Override
    public void processReader(Reader reader) {
    }

    @Override
    public void processNarrator(Reader narrator) {
    }

    @Override
    public void processCondition(Condition condition) {
    }

    @Override
    public void processExecution(Execution execution) {
    }

    @Override
    public void processCommand(Command command) {
    }

    @Override
    public void processExecutionSet(ExecutionSet execution) {
    }

    @Override
    public void processConditionSet(ConditionSet conditionSet) {
    }

    @Override
    public void processAction(Action action) {
    }

    @Override
    public void processProperty(Property property) {
    }

    @Override
    public void processExit(Exit exit) {
    }

    @Override
    public void processDialog(Dialog dialog) {
    }

    @Override
    public void processSchedule(Schedule schedule) {
    }

    @Override
    public void processMass(Mass mass) {
    }

}
