/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.dao.traversal;

import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   For operations that need to be performed on all items of a specific type in the environment tree
 *   <p>
 *   This expose a process method to be operated on that specific type
 *
 */
public abstract class EnvironmentTraversal {

    public void traverseEnvironment(Environment environment) {
        processEnvironment(environment);
        traverseEntity(environment);

        for (Location location : environment.getLocations()) {
            processLocation(location);
            traverseLocation(location);
        }

        for (Mass mass : environment.getMasses()) {
            processMass(mass);
            traverseMass(mass);
        }

        for (Item item : environment.getItems()) {
            processItem(item);
            traverseItem(item);
        }

        for (Person person : environment.getPeople()) {
            processPerson(person);
            traversePerson(person);
        }

        for (Reader narrator : environment.getNarrators()) {
            processNarrator(narrator);
            if ( StringUtils.equalsIgnoreCase(environment.getCurrentNarrator(), narrator.getId())) {
                processReader(narrator);
            }
            traverseNarrator(narrator);
        }

        for (ExecutionSet sharedExecution : environment.getSharedExecutions()) {
            processExecutionSet(sharedExecution);
            traverseExecutionSet(sharedExecution);
        }
        for (ConditionSet sharedCondition : environment.getSharedConditions()) {
            processConditionSet(sharedCondition);
            traverseConditionSet(sharedCondition);
        }

        CommandConfiguration commandConfig = environment.getConfiguration().getCommandConfiguration();
        traverseCommandConfiguration(commandConfig);
    }

    private void traverseCommandConfiguration(CommandConfiguration commandConfig) {
        for (Command command : commandConfig.getItemCommands()) {
            processCommand(command);
            traverseCommand(command);
        }
        for (Command command : commandConfig.getLocationCommands()) {
            processCommand(command);
            traverseCommand(command);
        }
        for (Command command : commandConfig.getMassCommands()) {
            processCommand(command);
            traverseCommand(command);
        }
        for (Command command : commandConfig.getNarratorCommands()) {
            processCommand(command);
            traverseCommand(command);
        }
        for (Command command : commandConfig.getPeopleCommands()) {
            processCommand(command);
            traverseCommand(command);
        }
    }

    private void traverseEntity(Entity entity) {
        for (Action action : entity.getActions()) {
            processAction(action);
            traverseAction(action);
        }

        for (Command command : entity.getCommands()) {
            processCommand(command);
            traverseCommand(command);
        }

        for (Property property : entity.getProperties()) {
            processProperty(property);
        }
    }

    private void traverseCommand(Command command) {
        for (Action consequence : command.getOutcomes()) {
            processAction(consequence);
            traverseAction(consequence);
        }

        ExecutionSet executionSet = command.getDefaultResults();
        processExecutionSet(executionSet);
        traverseExecutionSet(executionSet);

        ConditionSet conditionSet = command.getValidations();
        processConditionSet(conditionSet);
        traverseConditionSet(conditionSet);
    }

    private void traverseConditionSet(ConditionSet conditionSet) {
        for (Condition condition : conditionSet.getConditions()) {
            processCondition(condition);
        }
        for (ConditionSet subset : conditionSet.getSubsets()) {
            processConditionSet(subset);
            traverseConditionSet(subset);
        }
    }

    private void traverseExecutionSet(ExecutionSet executionSet) {
        for (Execution execution : executionSet.getExecutions()) {
            processExecution(execution);
        }
    }

    private void traverseAction(Action action) {
        ExecutionSet executionSet = action.getResults();
        processExecutionSet(executionSet);
        traverseExecutionSet(executionSet);

        ConditionSet conditionSet = action.getValidations();
        processConditionSet(conditionSet);
        traverseConditionSet(conditionSet);
    }

    private void traverseNarrator(Reader narrator) {
        traverseEntity(narrator);
    }

    private void traversePerson(Person person) {
        traverseEntity(person);

        for (Dialog dialog : person.getDialogs()) {
            processDialog(dialog);
            traverseDialog(dialog);
        }

        for (Schedule schedule : person.getSchedules()) {
            processSchedule(schedule);
        }
    }


    private void traverseMass(Mass mass) {
        traverseEntity(mass);
    }

    private void traverseDialog(Dialog dialog) {
        traverseExecutionSet(dialog.getResults());
        traverseConditionSet(dialog.getValidations());
    }

    private void traverseItem(Item item) {
        traverseEntity(item);
    }

    private void traverseLocation(Location location) {
        traverseEntity(location);
        for (Exit exit : location.getExits()) {
            processExit(exit);
            traverseExit(exit);
        }
    }

    private void traverseExit(Exit exit) {
        ConditionSet validations = exit.getValidations();
        processConditionSet(validations);
        traverseConditionSet(validations);
    }

    public abstract void processMass(Mass mass);

    public abstract void processEnvironment(Environment environment);

    public abstract void processLocation(Location environment);

    public abstract void processItem(Item item);

    public abstract void processPerson(Person person);

    public abstract void processReader(Reader reader);

    public abstract void processNarrator(Reader narrator);

    public abstract void processCondition(Condition condition);

    public abstract void processExecution(Execution execution);

    public abstract void processCommand(Command command);

    public abstract void processExecutionSet(ExecutionSet execution);

    public abstract void processConditionSet(ConditionSet conditionSet);

    public abstract void processAction(Action action);

    public abstract void processProperty(Property property);

    public abstract void processExit(Exit exit);

    public abstract void processDialog(Dialog dialog);

    public abstract void processSchedule(Schedule schedule);
}
