/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.dao.simple;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.dodecahelix.ifengine.command.defaults.BuiltinCommandBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.traversal.ReferenceTraversal;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.data.Time;
import com.dodecahelix.ifengine.data.validation.EnvironmentValidator;
import com.dodecahelix.ifengine.util.InvalidEnvironmentDataException;
import com.dodecahelix.ifengine.util.StringUtils;

public class SimpleEnvironmentDAO implements EnvironmentDAO {

    protected Environment environment;

    public SimpleEnvironmentDAO() {
        // start with null environment and wait to be initialized from load command
    }

    public SimpleEnvironmentDAO(Environment environment) {
        loadEnvironment(environment, true);
    }

    public SimpleEnvironmentDAO(Environment environment, boolean addBuiltInCommands) {
        loadEnvironment(environment, addBuiltInCommands);
    }

    @Override
    public void loadEnvironment(Environment environment, boolean addBuiltinCommands) {
        this.environment = environment;

        try {
            // validate environment (i.e; make sure all objects have descriptions, etc..)
            EnvironmentValidator.validate(environment);
        } catch (InvalidEnvironmentDataException e) {
            throw new IllegalStateException("environment data is invalid", e);
        }

        if (addBuiltinCommands) {
            addBuiltInCommands();
        }
    }

    private void addBuiltInCommands() {
        BuiltinCommandBuilder builtinCommandBuilder = new BuiltinCommandBuilder();
        builtinCommandBuilder.buildCommands(environment);
    }

    @Override
    public Environment getEnvironment() {
        return environment;
    }

    public Reader getReader() {
        Reader reader = null;
        String narratorId = environment.getCurrentNarrator();
        for (Reader narrator : environment.getNarrators()) {
            if (StringUtils.equalsIgnoreCase(narrator.getId(), narratorId)) {
                reader = narrator;
            }
        }
        return reader;
    }

    public Location getCurrentLocation() {
        Location location = null;
        String locationId = getReader().getCurrentLocation();

        for (Location searchLocation : environment.getLocations()) {
            if (searchLocation.getId().equalsIgnoreCase(locationId)) {
                location = searchLocation;
            }
        }
        return location;
    }

    @Override
    public Set<Item> getInventory() {
        Set<Item> inventory = new HashSet<Item>();

        Set<String> invStrings = getReader().getContents();
        for (Item item : environment.getItems()) {
            if (invStrings.contains(item.getId())) {
                inventory.add(item);
            }
        }
        return inventory;
    }

    @Override
    public Set<Mass> getMassesForCurrentLocation() {
        Set<Mass> masses = new HashSet<Mass>();

        Set<String> massStrings = getCurrentLocation().getMasses();
        for (Mass mass : environment.getMasses()) {
            if (massStrings.contains(mass.getId())) {
                masses.add(mass);
            }
        }

        return masses;
    }


    @Override
    public Set<Item> getContentsForEntity(EntityType entityType, String entityId) {
        Set<Item> contents = new HashSet<Item>();

        Entity entity = getEntityById(entityType, entityId);
        Set<String> itemIds = entity.getContents();

        for (Item item : environment.getItems()) {
            if (itemIds.contains(item.getId())) {
                contents.add(item);
            }
        }

        return contents;
    }

    @Override
    public Entity getEntityById(EntityType type, String id) {
        Entity entity = null;
        Collection<? extends Entity> entityList = null;
        switch (type) {
            case PERSON : entityList = environment.getPeople(); break;
            case NARRATOR : entityList = environment.getNarrators(); break;
            case ITEM : entityList = environment.getItems(); break;
            case LOCATION : entityList = environment.getLocations(); break;
            case MASS : entityList = environment.getMasses(); break;
            case ENVIRONMENT : entity = environment; break;
            case READER : entity = getReader(); break;
            case CURRENT_LOCATION : entity = getCurrentLocation(); break;
            case LOCAL_PERSON : entityList = getPeopleForCurrentLocation(); break;
        }

        if (entity==null && entityList!=null) {
            for (Entity e : entityList) {
                if (e.getId().equalsIgnoreCase(id)) {
                    entity = e;
                }
            }
        }

        return entity;
    }

    @Override
    public Set<Person> getPeopleForCurrentLocation() {

        String currentLocationId = getCurrentLocation().getId();
        return getPeopleForLocation(currentLocationId);
    }


    @Override
    public Set<Person> getPeopleForLocation(String locationId) {
        Set<Person> people = new HashSet<Person>();

        for (Person pp : environment.getPeople()) {
            // if person is following, and looking at current location
            if (pp.isFollowing()) {
                String currentLocationId = getCurrentLocation().getId();
                if (StringUtils.equalsIgnoreCase(currentLocationId, locationId)) {
                    people.add(pp);
                }
            } else {
                // check schedule
                Schedule activeSchedule = getActiveScheduleForPerson(pp);
                if (activeSchedule!=null
                        && StringUtils.equalsIgnoreCase(activeSchedule.getLocationId(), locationId)) {
                    people.add(pp);
                }
            }
        }

        return people;
    }

    @Override
    public Schedule getActiveScheduleForPerson(Person person) {

        Time currentTime = environment.getCurrentTime();

        // active schedule can end up being null, meaning that the person is not around at this time
        Schedule activeSchedule = null;
        String overrideSchedule = person.getOverrideSchedule();

        for (Schedule schedule : person.getSchedules()) {
            if (StringUtils.equalsIgnoreCase(overrideSchedule, schedule.getReferenceId())) {
                activeSchedule = schedule;
                break;
            }

            // check validations and timing of schedule
            if (schedule.isEnabled() && schedule.getTimeRange().isWithin(currentTime)) {

                activeSchedule = schedule;
                break;
            }
        }

        return activeSchedule;
    }

    @Override
    public ExecutionSet findReferenceExecutionById(String referenceSet) {
        ExecutionSet set = null;
        for (ExecutionSet sharedSet : environment.getSharedExecutions()) {
            if (StringUtils.equalsIgnoreCase(sharedSet.getReferenceId(), referenceSet)) {
                set = sharedSet;
            }
        }

        if (set==null) {
            throw new IllegalStateException("execution references to an invalid id : " + referenceSet);
        }

        return set;
    }

    @Override
    /**
     *  return all visible local entities
     */
    public Set<Entity> getLocalTargets() {
        Location currentLocation = getCurrentLocation();
        Set<Entity> localTargets = new HashSet<Entity>();

        // add all local people to targets list
        localTargets.addAll(getPeopleForCurrentLocation());

        // add all local items
        Set<String> itemIds = currentLocation.getContents();
        // should inventory items be included?
        //itemIds.addAll(getReader().getContents());
        for (Item item : environment.getItems()) {
            if (itemIds.contains(item.getId())) {
                localTargets.add(item);
            }
        }

        // add all local masses to targets list
        Set<String> massIds = currentLocation.getMasses();
        for (Mass mass : environment.getMasses()) {
            if (massIds.contains(mass.getId())) {
                localTargets.add(mass);
            }
        }

        return localTargets;
    }

    @Override
    public Command findCommandByReferenceId(Entity entity, String referenceId) {
        for (Command command : entity.getCommands()) {
            if (StringUtils.equalsIgnoreCase(command.getReferenceId(), referenceId)) {
                return command;
            }
        }
        return null;
    }

    @Override
    public Dialog findDialogByReferenceId(final String refId) {

        ReferenceTraversal traversal = new ReferenceTraversal();
        traversal.traverseEnvironment(environment, refId);
        return traversal.getFoundDialog();
    }

    @Override
    public Command findCommandByReferenceId(String refId) {

        ReferenceTraversal traversal = new ReferenceTraversal();
        traversal.traverseEnvironment(environment, refId);
        return traversal.getFoundCommand();
    }


}
