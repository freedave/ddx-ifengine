/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.dao;

import java.util.Set;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Schedule;

/**
 *   Wrapper around the environment model for logical access
 *
 */
public interface EnvironmentDAO {

    /**
     *   Loads a fresh environment into the DAO.
     *
     * @param environment
     * @param addDefaultCommands - appends default commands (get, look, move, etc..)
     */
    public void loadEnvironment(Environment environment, boolean addDefaultCommands);

    public Environment getEnvironment();

    public Location getCurrentLocation();

    /**
     *   Gets the current Reader object, from the list of narrators
     */
    public Reader getReader();

    /**
     *
     * @return the set of Item's carried by the Reader
     */
    public Set<Item> getInventory();

    /**
     *
     * @return a list of Masses for the current location
     */
    public Set<Mass> getMassesForCurrentLocation();

    public Entity getEntityById(EntityType type, String id);

    /**
     *
     * @return  Items belonging to the given entity
     */
    public Set<Item> getContentsForEntity(EntityType entityType, String entityId);

    public Set<Person> getPeopleForCurrentLocation();

    public Set<Person> getPeopleForLocation(String id);

    public Schedule getActiveScheduleForPerson(Person person);

    public ExecutionSet findReferenceExecutionById(String referenceSet);

    public Set<Entity> getLocalTargets();

    public Dialog findDialogByReferenceId(String refId);

    public Command findCommandByReferenceId(String refId);

    /**
     *  find the Command for the given entity, by reference id
     */
    public Command findCommandByReferenceId(Entity entity, String referenceId);


}
