/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.time;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Time;
import com.dodecahelix.ifengine.data.configuration.TimeConfiguration;

public class TimeHandler {

    private static Logger logger = LoggerFactory.getLogger(TimeHandler.class);

    private EnvironmentDAO dao;

    private int incrementsPerPeriod;
    private int periodsPerDay;
    private int daysPerWeek;
    private int weeksPerMonth;
    private int monthsPerYear;

    public TimeHandler(EnvironmentDAO dao) {
        this.dao = dao;
    }

    private void setup() {
        TimeConfiguration timeConfig = dao.getEnvironment().getConfiguration().getTimeConfiguration();

        this.incrementsPerPeriod = timeConfig.getIncrementsPerPeriod();
        this.periodsPerDay = timeConfig.getPeriodsOfDay().size();
        this.daysPerWeek = timeConfig.getDaysOfWeek().size();
        this.weeksPerMonth = timeConfig.getWeeksPerMonth();
        this.monthsPerYear = timeConfig.getMonthsOfYear().size();

        logger.debug("configuration - incrementsPerPeriod: {}, periodsPerDay: {}, "
                + "daysPerWeek: {}, weeksPerMonth: {}, monthsPerYear: {}",
                new Object[] { incrementsPerPeriod, periodsPerDay, daysPerWeek, weeksPerMonth, monthsPerYear });
    }

    public boolean increment(Time time, int numIncrements) {
        boolean newPeriod = false;

        if (logger.isDebugEnabled()) {
            logger.debug("incrementing time.");
            logger.debug("current time: {}", time);
            logger.debug("number of increments: {}", numIncrements);
        }

        setup();
        int increment = time.getIncrement();
        increment += numIncrements;
        if (increment > incrementsPerPeriod) {
            increment -= incrementsPerPeriod;
            newPeriod = true;
            incrementPeriod(time, 1);
        }
        time.setIncrement(increment);
        logger.debug("increment complete.  new time: {}", time);

        return newPeriod;
    }

    /**
     *   Increment the time period (i.e; afternoon into evening)
     *
     * @return whether or not it is a new day
     */
    public boolean incrementPeriod(Time time, int numPeriods) {
        setup();

        boolean newDay = false;
        int period = time.getPeriod();
        period += numPeriods;
        if (period > periodsPerDay) {
            period -= periodsPerDay;
            newDay = true;
            incrementDay(time);
        }
        time.setPeriod(period);
        return newDay;
    }

    public boolean incrementDay(Time time) {
        setup();

        boolean newWeek = false;
        int weekday = time.getWeekday();
        weekday++;
        if (weekday > daysPerWeek) {
            time.setWeekday(1);
            newWeek = true;
            incrementWeek(time);
        } else {
            time.setWeekday(weekday);
        }
        return newWeek;
    }

    public boolean incrementWeek(Time time) {
        setup();

        boolean newMonth = false;
        int week = time.getWeek();
        week++;
        if (week > weeksPerMonth) {
            time.setWeek(1);
            newMonth = true;
            incrementMonth(time);
        } else {
            time.setWeek(week);
        }
        return newMonth;
    }

    public boolean incrementMonth(Time time) {
        setup();

        boolean newYear = false;
        int month = time.getMonth();
        month++;
        if (month > monthsPerYear) {
            time.setMonth(1);
            newYear = true;
            incrementYear(time);
        } else {
            time.setMonth(month);
        }
        return newYear;
    }

    public void incrementYear(Time time) {
        setup();
        time.setYear(time.getYear() + 1);
    }

}
