/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.environment;

import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.choice.ChoiceResult;

/**
 *   Interface with the client application for loading, saving and restarting environments/stories.
 *
 *   Think of the "Environment" as the current state of the story, and the "Story" as the original state of the environment.
 *
 */
public interface EnvironmentProcessor {

    /**
     * Loads in a new story from a bundle in some serialized format (i.e; byte[], json, csv, json-zipped, json-encrypted-zipped).
     *
     * @param story
     */
    public void loadStory(StoryBundle story);

    /**
     *   Verifies that the environment/story is loaded, and then process the initial command.
     */
    public ChoiceResult startStory();

    /**
     *   Rebuilds and loads the environment from the last bundle, and then starts the story, processing the initial command.
     */
    public ChoiceResult restartStory();

    /**
     *   Load the saved environment state and process the choice result of a look command
     *
     * @param environmentSave
     * @return
     */
    public ChoiceResult loadSavedEnvironment(EnvironmentSave environmentSave);

    /**
     *   Saves the current environment state for persistence.
     *
     * @return
     */
    public EnvironmentSave saveEnvironment();

    /**
     *  Retrieves the metadata (i.e; title, author, synopsis, etc..) for the currently loaded story.
     *
     * @return
     */
    public LibraryCard getCurrentStoryInfo();

    /**
     *   Returns a unique name/identifier for the current story (based on its metadata).
     *
     *   This identifier is useful for creating a default filename for the story when you save its progress.
     *
     * @return
     */
    public String getStoryIdentifier();

}
