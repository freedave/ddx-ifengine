/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.environment;

import com.dodecahelix.ifengine.command.defaults.LookCommandBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Time;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.save.EnvironmentSaveBuilder;
import com.dodecahelix.ifengine.data.save.EnvironmentSaveLoader;
import com.dodecahelix.ifengine.data.save.json.JsonEnvironmentSaveBuilder;
import com.dodecahelix.ifengine.data.save.json.JsonEnvironmentSaveLoader;
import com.dodecahelix.ifengine.data.serializer.MediaFormat;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.data.serializer.json.JsonStoryBundleLoader;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.choice.ChoiceProcessor;
import com.dodecahelix.ifengine.choice.ChoiceRecorder;
import com.dodecahelix.ifengine.choice.ChoiceResult;
import com.dodecahelix.ifengine.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *   Handles initializing, saving and restarting IF stories (using a JSON format for serialization)
 *
 */
public class JsonEnvironmentProcessor implements EnvironmentProcessor {

    private static final Logger logger = LoggerFactory.getLogger(JsonEnvironmentProcessor.class);

    private EnvironmentDAO environmentDao;

    private ChoiceProcessor choiceProcessor;
    private ChoiceRecorder choiceRecorder;

    /**
     *  Loads in the initial environment from some artifact called a StoryBundle
     */
    private JsonStoryBundleLoader storyBundleLoader;

    /**
     *  Saves/Exports the Environment as an artifact for persistence.
     */
    private EnvironmentSaveBuilder environmentSaveBuilder;

    /**
     *  Loads/Import the environment from a previously saved state.
     */
    private EnvironmentSaveLoader environmentSaveLoader;


    public JsonEnvironmentProcessor(EnvironmentDAO environmentDao,
                                    ChoiceProcessor choiceProcessor,
                                    ChoiceRecorder choiceRecorder) {

        storyBundleLoader = new JsonStoryBundleLoader();

        this.environmentDao = environmentDao;
        this.choiceProcessor = choiceProcessor;
        this.choiceRecorder = choiceRecorder;

        this.environmentSaveBuilder = new JsonEnvironmentSaveBuilder();
        this.environmentSaveLoader = new JsonEnvironmentSaveLoader();
    }

    /**
     * Loads (and builds) a new story from a bundle.
     *
     * @param story
     */
    public void loadStory(StoryBundle story) {
        logger.debug("loading story from bundle");

        if (story.getDataFormat() != MediaFormat.JSON) {
            throw new UnsupportedOperationException("unsupported serialization format : " + story.getDataFormat());
        }
        storyBundleLoader.setBundle(story);

        Environment environment = storyBundleLoader.build();
        environmentDao.loadEnvironment(environment, true);

        logger.debug("loaded story into environment: {}", environment.getLibraryCard().getTitle());
    }

    /**
     *  Starts a story after it has been loaded.
     *
     * @return
     */
    @Override
    public ChoiceResult startStory() {
        logger.debug("starting story {}", environmentDao.getEnvironment().getLibraryCard().getTitle());

        Command introCommand = environmentDao.getEnvironment().getConfiguration().getCommandConfiguration().getIntroCommand();
        Location currentLocation = environmentDao.getCurrentLocation();

        choiceRecorder.resetRecord();

        return choiceProcessor.processCommand(introCommand, currentLocation.getId(), null);
    }

    @Override
    public ChoiceResult restartStory() {

        // recreate the environment from its serialized state (can't use the existing Environment)
        Environment defaultEnvironment = storyBundleLoader.build();
        defaultEnvironment.setCurrentTime(new Time(defaultEnvironment.getConfiguration().getTimeConfiguration()));

        environmentDao.loadEnvironment(defaultEnvironment, true);

        return startStory();
    }

    public EnvironmentSave saveEnvironment() {
        EnvironmentSave environmentSave = environmentSaveBuilder
            .buildFromEnvironment(environmentDao.getEnvironment(), getStoryIdentifier());

        return environmentSave;
    }

    @Override
    public ChoiceResult loadSavedEnvironment(EnvironmentSave environmentSave) {

        Environment newEnvironment = environmentSaveLoader.buildEnvironmentFromSave(environmentSave);
        environmentDao.loadEnvironment(newEnvironment, false);

        // new record will only be valid from current save
        choiceRecorder.resetRecord();

        Command lookCommand = null;
        CommandConfiguration commandConfig = environmentDao.getEnvironment().getConfiguration().getCommandConfiguration();
        String lookLocationRefId = String.format(LookCommandBuilder.LOOK_REF_ID, EntityType.LOCATION.name().toLowerCase());
        for (Command command : commandConfig.getLocationCommands()) {
            if (StringUtils.equalsIgnoreCase(lookLocationRefId, command.getReferenceId())) {
                lookCommand = command;
            }
        }

        if (lookCommand==null) {
            throw new IllegalStateException("could not retrieve look command");
        }

        Location currentLocation = environmentDao.getCurrentLocation();
        return choiceProcessor.processCommand(lookCommand, currentLocation.getId(), null);
    }

    @Override
    public String getStoryIdentifier() {
        String title = environmentDao.getEnvironment().getLibraryCard().getTitle();
        return title.replaceAll("[^a-zA-Z]", "") + ".json";
    }

    /**
     * Retrieves the metadata (i.e; title, author, etc..) for the current story
     * @return
     */
    public LibraryCard getCurrentStoryInfo() {
        return environmentDao.getEnvironment().getLibraryCard();
    }


}
