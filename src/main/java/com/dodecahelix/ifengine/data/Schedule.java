/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;

/**
 *  Specifies where a Person entity should be located during a certain time period.
 *  
 */
public class Schedule implements Serializable {

    private static final long serialVersionUID = 2443201026876301610L;

    /**
     *  Can be enabled or disabled by an execution
     */
    private boolean enabled = true;

    /**
     *   So other objects can reference this schedule (i.e; conditions, or setting a schedule override)
     */
    private String referenceId;

    /**
     *  When printing the room description or the player description, this will print the Person as doing this.
     *
     */
    private String actionMessage;

    /**
     *  What locaiton the person will be in for this schedule
     *
     */
    private String locationId;

    /**
     *   What time this schedule occurs
     */
    private TimeRange timeRange = new TimeRange();

    public String getActionMessage() {
        return actionMessage;
    }

    public void setActionMessage(String actionMessage) {
        this.actionMessage = actionMessage;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public TimeRange getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(TimeRange timeRange) {
        this.timeRange = timeRange;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "Schedule [enabled=" + enabled + ", referenceId=" + referenceId + ", actionMessage=" + actionMessage + ", locationId=" + locationId
                + ", timeRange=" + timeRange + "]";
    }

}
