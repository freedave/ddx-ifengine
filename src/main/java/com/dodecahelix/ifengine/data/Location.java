/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.util.HashSet;
import java.util.Set;

import com.dodecahelix.ifengine.util.IdUtil;

public class Location extends Entity {

    private static final long serialVersionUID = -414934899993137462L;

    private Set<Exit> exits = new HashSet<Exit>();
    private Set<String> masses = new HashSet<String>();

    /**
     *   If the location is dark, it can only be entered with a light source
     */
    private boolean dark = false;

    public Location() {
    }

    public Location(String title, String description) {
        // WARNING -- this will be not be unique if another room has the same title
        this(IdUtil.fromTitle(title), title, description);
    }

    public Location(String id, String title, String description) {
        this.setId(id);

        // have the UI capitalize when necessary
        this.setTitle(title.toLowerCase());
        this.setDescription(description);
    }

    @Override
    public EntityType getEntityType() {
        return EntityType.LOCATION;
    }

    public Set<Exit> getExits() {
        return exits;
    }

    public void setExits(Set<Exit> exits) {
        this.exits = exits;
    }

    public Set<String> getMasses() {
        return masses;
    }

    public void setMasses(Set<String> masses) {
        this.masses = masses;
    }

    public void addExit(Exit exit) {
        exits.add(exit);
    }

    public void addMass(String massId) {
        masses.add(massId);
    }

    public boolean isDark() {
        return dark;
    }

    public void setDark(boolean dark) {
        this.dark = dark;
    }

    @Override
    public String toString() {
        return "Location [exits=" + exits + ", dark=" + dark + ", masses=" + masses + ", entity=" + super.toString() + "]";
    }



}
