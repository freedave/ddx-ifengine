/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.util.HashSet;
import java.util.Set;

import com.dodecahelix.ifengine.data.configuration.Configuration;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *  The environment is the root level object in the IF data model
 *  
 * @author dpeters
 *
 */
public class Environment extends Entity {

    private static final long serialVersionUID = -4613532106219292331L;

    private LibraryCard libraryCard = new LibraryCard();
    private StoryContent storyContent = new StoryContent();

    /**
     *  flag to indicate that the story is over and no more turns will be processed
     */
    private boolean storyComplete = false;

    private Set<Location> locations = new HashSet<Location>();
    private Set<Person> people = new HashSet<Person>();
    private Set<Item> items = new HashSet<Item>();
    private Set<Mass> masses = new HashSet<Mass>();

    /**
     *  a story can have multiple narrators.  The current narrator is the reader.
     */
    private Set<Reader> narrators = new HashSet<Reader>();
    private String currentNarrator;

    private Set<ConditionSet> sharedConditions = new HashSet<ConditionSet>();
    private Set<ExecutionSet> sharedExecutions = new HashSet<ExecutionSet>();

    private Configuration configuration = new Configuration();

    private Time currentTime;

    /**
     *   State where current time cannot change.
     *   <p>
     *   Temporal commands (commands that take time) cannot be executed.
     */
    private boolean frozenInTime;

    public Environment() {
    }

    @Override
    public EntityType getEntityType() {
        return EntityType.ENVIRONMENT;
    }

    public Set<Location> getLocations() {
        return locations;
    }

    public void setLocations(Set<Location> locations) {
        this.locations = locations;
    }

    public Set<Person> getPeople() {
        return people;
    }

    public void setPeople(Set<Person> people) {
        this.people = people;
    }

    public String getCurrentNarrator() {
        return currentNarrator;
    }

    public void setCurrentNarrator(String currentNarrator) {
        this.currentNarrator = currentNarrator;
    }

    public Set<Mass> getMasses() {
        return masses;
    }

    public void setMasses(Set<Mass> masses) {
        this.masses = masses;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public void addLocation(Location location) {
        locations.add(location);
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void addMass(Mass mass) {
        masses.add(mass);
    }

    public void addPerson(Person person) {
        people.add(person);
    }

    public Time getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Time currentTime) {
        this.currentTime = currentTime;
    }

    public boolean isFrozenInTime() {
        return frozenInTime;
    }

    public void setFrozenInTime(boolean frozenInTime) {
        this.frozenInTime = frozenInTime;
    }

    public LibraryCard getLibraryCard() {
        return libraryCard;
    }

    public void setLibraryCard(LibraryCard libraryCard) {
        this.libraryCard = libraryCard;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Set<Reader> getNarrators() {
        return narrators;
    }

    public void setNarrators(Set<Reader> narrators) {
        this.narrators = narrators;
    }

    public StoryContent getStoryContent() {
        return storyContent;
    }

    public void setStoryContent(StoryContent storyContent) {
        this.storyContent = storyContent;
    }

    public Set<ConditionSet> getSharedConditions() {
        return sharedConditions;
    }

    public void setSharedConditions(Set<ConditionSet> sharedConditions) {
        this.sharedConditions = sharedConditions;
    }

    public Set<ExecutionSet> getSharedExecutions() {
        return sharedExecutions;
    }

    public void setSharedExecutions(Set<ExecutionSet> sharedExecutions) {
        this.sharedExecutions = sharedExecutions;
    }

    public boolean isStoryComplete() {
        return storyComplete;
    }

    public void setStoryComplete(boolean storyIsOver) {
        this.storyComplete = storyIsOver;
    }

    @Override
    public String toString() {
        return "Environment [libraryCard=" + libraryCard + ", storyContent=" + storyContent + ", storyComplete=" + storyComplete + ", currentNarrator="
                + currentNarrator + ", sharedConditions=" + sharedConditions + ", sharedExecutions=" + sharedExecutions + ", configuration=" + configuration
                + ", currentTime=" + currentTime + ", frozenInTime=" + frozenInTime + ", getProperties()=" + getProperties() + ", getContents()="
                + getContents() + ", getId()=" + getId() + ", getDescription()=" + StringUtils.truncate(getDescription(), 20, true) + ", getTitle()=" + getTitle() + ", getCommands()="
                + getCommands() + ", getActions()=" + getActions() + "]";
    }



}
