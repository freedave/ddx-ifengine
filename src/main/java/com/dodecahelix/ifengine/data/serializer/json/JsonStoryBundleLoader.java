/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.serializer.json;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.serializer.JsonStoryBundle;
import com.dodecahelix.ifengine.data.serializer.MediaFormat;
import com.dodecahelix.ifengine.data.serializer.StoryBundleLoader;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.google.gson.Gson;

public class JsonStoryBundleLoader implements StoryBundleLoader {

    private Gson gson;

    private String jsonStoryString;

    public JsonStoryBundleLoader() {
        gson = new Gson();
    }

    @Override
    public Environment build() {
        if (jsonStoryString == null) {
            throw new IllegalStateException("cannot build a story file before it is loaded.");
        }

        return gson.fromJson(jsonStoryString, Environment.class);
    }

    @Override
    public void setBundle(StoryBundle story) {
        if (MediaFormat.JSON.equals(story.getDataFormat())) {
            jsonStoryString = ((JsonStoryBundle)story).getStoryJson();
        } else {
            throw new IllegalArgumentException("Expecting a JSON story file, but received this format :  " + story.getDataFormat());
        }
    }

    public Environment buildFromBundle(StoryBundle story) {
        setBundle(story);
        return build();
    }
}
