/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.configuration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AnatomyConfiguration implements Serializable {

    private static final long serialVersionUID = 7047917169985880099L;

    /**
     *  Defines a list of body parts for clothing references
     */
    private List<String> bodyParts = new ArrayList<String>();

    public AnatomyConfiguration() {

        // initialize the defaults
        bodyParts.add("head");
        bodyParts.add("body");
        bodyParts.add("feet");
        bodyParts.add("back");
    }

    public List<String> getBodyParts() {
        return bodyParts;
    }

    public void setBodyParts(List<String> bodyParts) {
        this.bodyParts = bodyParts;
    }

    @Override
    public String toString() {
        return "AnatomyConfiguration [bodyParts=" + bodyParts + "]";
    }

}
