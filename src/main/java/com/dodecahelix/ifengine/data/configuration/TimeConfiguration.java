/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.configuration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TimeConfiguration implements Serializable {

    private static final long serialVersionUID = -4176589626818592189L;

    /**
     *   Number of time increments (i.e; a 15 minute interval) for each period
     */
    private int incrementsPerPeriod;

    /**
     *   This is probably the easiest property to configure to shorten the years
     */
    private int weeksPerMonth;

    /**
     *  Names of the period of times for each day
     */
    private List<String> periodsOfDay = new ArrayList<String>();

    /**
     *  Names of the days in each week
     */
    private List<String> daysOfWeek = new ArrayList<String>();

    /**
     *  Names of the month in each year
     */
    private List<String> monthsOfYear = new ArrayList<String>();

    private int startPeriod;
    private int startDayOfMonth;
    private int startMonth;
    private int startingYear;

    public TimeConfiguration() {

        // load the defaults
        this.incrementsPerPeriod = 6;
        this.weeksPerMonth = 4;
        this.startingYear = 2000;
        this.startMonth = 6;   // june
        this.startPeriod = 2;  // morning
        this.startDayOfMonth = 1;

        String[] periodNames = new String[] { "Night", "Morning", "Afternoon", "Evening" };
        String[] weekdays = new String[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
        String[] monthNames = new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

        this.monthsOfYear.addAll(Arrays.asList(monthNames));
        this.daysOfWeek.addAll(Arrays.asList(weekdays));
        this.periodsOfDay.addAll(Arrays.asList(periodNames));
    }

    public int getIncrementsPerPeriod() {
        return incrementsPerPeriod;
    }

    public void setIncrementsPerPeriod(int incrementsPerPeriod) {
        this.incrementsPerPeriod = incrementsPerPeriod;
    }

    public int getWeeksPerMonth() {
        return weeksPerMonth;
    }

    public void setWeeksPerMonth(int weeksPerMonth) {
        this.weeksPerMonth = weeksPerMonth;
    }

    public List<String> getPeriodsOfDay() {
        return periodsOfDay;
    }

    public void setPeriodsOfDay(List<String> periodsOfDay) {
        this.periodsOfDay = periodsOfDay;
    }

    public List<String> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(List<String> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public List<String> getMonthsOfYear() {
        return monthsOfYear;
    }

    public void setMonthsOfYear(List<String> monthsOfYear) {
        this.monthsOfYear = monthsOfYear;
    }

    public int getStartingYear() {
        return startingYear;
    }

    public void setStartingYear(int startingYear) {
        this.startingYear = startingYear;
    }

    public int getStartPeriod() {
        return startPeriod;
    }

    public void setStartPeriod(int startPeriod) {
        this.startPeriod = startPeriod;
    }

    public int getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(int startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartDayOfMonth() {
        return startDayOfMonth;
    }

    public void setStartDayOfMonth(int startDayOfMonth) {
        this.startDayOfMonth = startDayOfMonth;
    }

    @Override
    public String toString() {
        return "TimeConfiguration [incrementsPerPeriod=" + incrementsPerPeriod + ", weeksPerMonth=" + weeksPerMonth + ", periodsOfDay=" + periodsOfDay
                + ", daysOfWeek=" + daysOfWeek + ", monthsOfYear=" + monthsOfYear + ", startPeriod=" + startPeriod + ", startDayOfMonth=" + startDayOfMonth
                + ", startMonth=" + startMonth + ", startingYear=" + startingYear + "]";
    }



}
