/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.configuration;

import java.io.Serializable;


/**
 *   Contains verious configuration settings for the application
 *
 */
public class Configuration implements Serializable {

    private static final long serialVersionUID = -5837381836899058365L;

    private CommandConfiguration commandConfiguration = new CommandConfiguration();
    private DialogConfiguration dialogConfiguration = new DialogConfiguration();
    private TimeConfiguration timeConfiguration = new TimeConfiguration();
    private AnatomyConfiguration anatomyConfiguration = new AnatomyConfiguration();
    private MessageConfiguration messageConfiguration = new MessageConfiguration();

    public DialogConfiguration getDialogConfiguration() {
        return dialogConfiguration;
    }
    public void setDialogConfiguration(DialogConfiguration dialogConfiguration) {
        this.dialogConfiguration = dialogConfiguration;
    }
    public TimeConfiguration getTimeConfiguration() {
        return timeConfiguration;
    }
    public void setTimeConfiguration(TimeConfiguration timeConfiguration) {
        this.timeConfiguration = timeConfiguration;
    }
    public AnatomyConfiguration getAnatomyConfiguration() {
        return anatomyConfiguration;
    }
    public void setAnatomyConfiguration(AnatomyConfiguration anatomyConfiguration) {
        this.anatomyConfiguration = anatomyConfiguration;
    }
    public MessageConfiguration getMessageConfiguration() {
        return messageConfiguration;
    }
    public void setMessageConfiguration(MessageConfiguration messageConfiguration) {
        this.messageConfiguration = messageConfiguration;
    }
    public CommandConfiguration getCommandConfiguration() {
        return commandConfiguration;
    }
    public void setCommandConfiguration(CommandConfiguration commandConfiguration) {
        this.commandConfiguration = commandConfiguration;
    }

}
