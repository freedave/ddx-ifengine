/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.configuration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.data.Command;

/**
 *   Special commands and commands that are to be applied across all entity types.
 *
 */
public class CommandConfiguration implements Serializable {

    private static final long serialVersionUID = 4637951379814316761L;

    private Command introCommand;

    private List<Command> locationCommands = new ArrayList<Command>();
    private List<Command> narratorCommands = new ArrayList<Command>();
    private List<Command> itemCommands = new ArrayList<Command>();
    private List<Command> massCommands = new ArrayList<Command>();
    private List<Command> peopleCommands = new ArrayList<Command>();

    public Command getIntroCommand() {
        return introCommand;
    }

    public void setIntroCommand(Command startCommand) {
        this.introCommand = startCommand;
    }

    public List<Command> getLocationCommands() {
        return locationCommands;
    }

    public void setLocationCommands(List<Command> locationCommands) {
        this.locationCommands = locationCommands;
    }

    public List<Command> getNarratorCommands() {
        return narratorCommands;
    }

    public void setNarratorCommands(List<Command> narratorCommands) {
        this.narratorCommands = narratorCommands;
    }

    public List<Command> getItemCommands() {
        return itemCommands;
    }

    public void setItemCommands(List<Command> itemCommands) {
        this.itemCommands = itemCommands;
    }

    public List<Command> getMassCommands() {
        return massCommands;
    }

    public void setMassCommands(List<Command> massCommands) {
        this.massCommands = massCommands;
    }

    public List<Command> getPeopleCommands() {
        return peopleCommands;
    }

    public void setPeopleCommands(List<Command> peopleCommands) {
        this.peopleCommands = peopleCommands;
    }

    @Override
    public String toString() {
        return "CommandConfiguration [introCommand=" + introCommand + ", locationCommands=" + locationCommands + ", narratorCommands=" + narratorCommands
                + ", itemCommands=" + itemCommands + ", massCommands=" + massCommands + ", peopleCommands=" + peopleCommands + "]";
    }

}
