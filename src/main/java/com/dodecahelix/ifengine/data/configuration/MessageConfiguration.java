/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.configuration;

import java.io.Serializable;

/**
 *   TODO - let this be modifiable from 
 *
 */
public class MessageConfiguration implements Serializable {

    private static final long serialVersionUID = -621537737360374249L;

    private String notEnoughTimeMessage = "There is no time for that!";
    private String cantDropItemThatIsWornMessage = "That item is worn.  Please remove it first.";
    private String tooBigToCarryMessage = "You're carrying too much stuff.  You can't carry any more.";
    private String tooDarkToMoveMessage = "It's too dark there.  You need a light.";
    private String genericExceptionMessage = "An unexpected problem has occurred when handling your command! "
            + "Please contact the ClickFic technical support forum with the story name, "
            + "a save game file (if possible) and the command that you entered when receiving this exception.";
    private String pauseMessage = "Paused... Please click on panel to continue.";

    public String getNotEnoughTimeMessage() {
        return notEnoughTimeMessage;
    }

    public void setNotEnoughTimeMessage(String notEnoughTimeMessage) {
        this.notEnoughTimeMessage = notEnoughTimeMessage;
    }

    public String getCantDropItemThatIsWornMessage() {
        return cantDropItemThatIsWornMessage;
    }

    public void setCantDropItemThatIsWornMessage(String cantDropItemThatIsWornMessage) {
        this.cantDropItemThatIsWornMessage = cantDropItemThatIsWornMessage;
    }

    public String getTooBigToCarryMessage() {
        return tooBigToCarryMessage;
    }

    public void setTooBigToCarryMessage(String tooBigToCarryMessage) {
        this.tooBigToCarryMessage = tooBigToCarryMessage;
    }

    public void setTooDarkToMoveMessage(String tooDarkToMoveMessage) {
        this.tooDarkToMoveMessage = tooDarkToMoveMessage;
    }

    public String getTooDarkToMoveMessage() {
        return tooDarkToMoveMessage;
    }

    public String getGenericExceptionMessage() {
        // TODO Auto-generated method stub
        return genericExceptionMessage;
    }

    public String getPauseMessage() {
        return pauseMessage;
    }

    public void setPauseMessage(String pauseMessage) {
        this.pauseMessage = pauseMessage;
    }
}
