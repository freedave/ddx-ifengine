/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *   Bundles the executions, allowing them to be shared
 *   
 * @author dpeters
 *
 */
public class ExecutionSet implements Serializable {

    private static final long serialVersionUID = -1283273907064751171L;

    /**
     *   Handle used for sharing this set
     */
    private String referenceId;

    /**
     *  Reference ID's for any executions from the Environment's shared executions
     */
    private List<String> referenceExecutions = new ArrayList<String>();

    private List<Execution> executions = new ArrayList<Execution>();

    public String getReferenceId() {
        return referenceId;
    }
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
    public List<String> getReferenceExecutions() {
        return referenceExecutions;
    }
    public void setReferenceExecutions(List<String> referenceExecutions) {
        this.referenceExecutions = referenceExecutions;
    }
    public List<Execution> getExecutions() {
        return executions;
    }
    public void setExecutions(List<Execution> executions) {
        this.executions = executions;
    }

    @Override
    public String toString() {
        return "ExecutionSet{" +
        "referenceId='" + referenceId + '\'' +
        ", referenceExecutions=" + referenceExecutions +
        ", executions=" + executions +
        '}';
    }
}
