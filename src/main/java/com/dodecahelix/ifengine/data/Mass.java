/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import com.dodecahelix.ifengine.util.IdUtil;

/**
 * A mass is an entity that is a fixed part a location.
 *
 * (i.e; a refrigerator in the kitchen)
 *
 */
public class Mass extends Entity {

    private static final long serialVersionUID = 4764944868177140716L;

    /**
     *   if true, this is an ON container (things are put on top of it)
     *   if false, this is an IN container (things are put inside of it)
     */
    private boolean onNotIn = false;

    public Mass() {
    }

    public Mass(String title, String description) {
        this(IdUtil.fromTitle(title), title, description);
    }

    public Mass(String id, String title, String description) {
        this.setId(id);
        this.setTitle(title.toLowerCase());
        this.setDescription(description);
    }

    @Override
    public EntityType getEntityType() {
        return EntityType.MASS;
    }

    public boolean isOnNotIn() {
        return onNotIn;
    }

    public void setOnNotIn(boolean onNotIn) {
        this.onNotIn = onNotIn;
    }

    @Override
    public String toString() {
        return "Mass{" +
        "onNotIn=" + onNotIn +
        '}';
    }
}
