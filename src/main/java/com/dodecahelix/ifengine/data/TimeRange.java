/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class TimeRange implements Serializable {

    private static final long serialVersionUID = 5208668577178542142L;

    /**
     *   What periods of the day this is active (i.e; 1,2 = morning, afternoon)
     */
    private Set<Integer> periodsOfDay = new HashSet<Integer>();

    /**
     *   Which days of the week this is active (i.e; 1,2,3,4,5 = Monday - Friday)
     */
    private Set<Integer> daysOfWeek = new HashSet<Integer>();

    /**
     *   Which months out of the year is this active (i.e; 6,7,8 = June, July, August)
     */
    private Set<Integer> monthsOfYear = new HashSet<Integer>();


    /**
     *   Determines if the given time falls within the range
     *
     * @param time
     * @return
     */
    public boolean isWithin(Time time) {
        boolean active = true;

        int period = time.getPeriod();
        if (!periodsOfDay.isEmpty() && !periodsOfDay.contains(period)) {
            active = false;
        }

        int weekday = time.getWeekday();
        if (!daysOfWeek.isEmpty() && !daysOfWeek.contains(weekday)) {
            active = false;
        }

        int month = time.getMonth();
        if (!monthsOfYear.isEmpty() && !monthsOfYear.contains(month)) {
            active = false;
        }

        return active;
    }

    public void setAllDay() {
        // if the set is empty, this is an all day range
        periodsOfDay.clear();
    }

    public void setEveryDayOfWeek() {
        daysOfWeek.clear();
    }

    public void setEveryMonthOfYear() {
        monthsOfYear.clear();
    }

    public Set<Integer> getPeriodsOfDay() {
        return periodsOfDay;
    }

    public void setPeriodsOfDay(Set<Integer> periodsOfDay) {
        this.periodsOfDay = periodsOfDay;
    }

    public Set<Integer> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(Set<Integer> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public Set<Integer> getMonthsOfYear() {
        return monthsOfYear;
    }

    public void setMonthsOfYear(Set<Integer> monthsOfYear) {
        this.monthsOfYear = monthsOfYear;
    }

}
