/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *  Template for a special type of property - a statistic
 *  Defined at the system, or environment level, hold global configuration for statistic objects
 *
 *
 */
public class Statistic implements Serializable {

    private static final long serialVersionUID = -3347770183054900172L;

    // groups statistic (i.e; skills, attributes, stats)
    private String category;

    private String propertyId;   // this is essentially a property
    private String displayName;  // how the stat is displayed
    private String description;  // long description of what this stat represents

    private int defaultValue; // if value is not defined in property, the character will be autopopulated with this value;
    private int maxValue;     // when this value is hit, dont allow the number to go higher, run the max handlers
    private int minValue;      // when this value is hit, dont allow the number to go lower, run the min handlers

    private Set<Execution> maxHandlers = new HashSet<Execution>();
    private Set<Execution> minHandlers = new HashSet<Execution>();

    public Statistic() {
    }

    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getDefaultValue() {
        return defaultValue;
    }
    public void setDefaultValue(int defaultValue) {
        this.defaultValue = defaultValue;
    }
    public int getMaxValue() {
        return maxValue;
    }
    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }
    public int getMinValue() {
        return minValue;
    }
    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }
    public String getPropertyId() {
        return propertyId;
    }
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public Set<Execution> getMaxHandlers() {
        return maxHandlers;
    }

    public void setMaxHandlers(Set<Execution> maxHandlers) {
        this.maxHandlers = maxHandlers;
    }

    public Set<Execution> getMinHandlers() {
        return minHandlers;
    }

    public void setMinHandlers(Set<Execution> minHandlers) {
        this.minHandlers = minHandlers;
    }

}
