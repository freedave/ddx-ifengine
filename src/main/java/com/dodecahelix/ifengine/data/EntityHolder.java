/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;


/**
 *   Bean for holding the parsed entity information (for both subject and target entities)
 *
 */
public class EntityHolder implements Serializable {

    private static final long serialVersionUID = 529135031452805831L;

    public static final String THIS_SUBJECT_ID = "this";
    public static final String LOCAL_TARGET_ID = "local";

    private EntityType subjectType;
    private String subjectEntityId;
    private String subjectProperty;

    private EntityType targetType;
    private String targetEntityId;
    private String targetProperty;

    public EntityType getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(EntityType subjectType) {
        this.subjectType = subjectType;
    }

    public String getSubjectEntityId() {
        return subjectEntityId;
    }

    public void setSubjectEntityId(String subjectEntityId) {
        this.subjectEntityId = subjectEntityId;
    }

    public String getSubjectProperty() {
        return subjectProperty;
    }

    public void setSubjectProperty(String subjectProperty) {
        this.subjectProperty = subjectProperty;
    }

    public EntityType getTargetType() {
        return targetType;
    }

    public void setTargetType(EntityType targetType) {
        this.targetType = targetType;
    }

    public String getTargetEntityId() {
        return targetEntityId;
    }

    public void setTargetEntityId(String targetEntityId) {
        this.targetEntityId = targetEntityId;
    }

    public String getTargetProperty() {
        return targetProperty;
    }

    public void setTargetProperty(String targetProperty) {
        this.targetProperty = targetProperty;
    }

    @Override
    public String toString() {
        return "EntityHolder{" +
        "subjectType=" + subjectType +
        ", subjectEntityId='" + subjectEntityId + '\'' +
        ", subjectProperty='" + subjectProperty + '\'' +
        ", targetType=" + targetType +
        ", targetEntityId='" + targetEntityId + '\'' +
        ", targetProperty='" + targetProperty + '\'' +
        '}';
    }
}
