/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

public enum EntityType {

    ENVIRONMENT ("environment"),
    NARRATOR ("narrator"),
    PERSON ("person"),
    LOCATION ("location"),
    MASS ("mass"),
    ITEM ("item"),
    READER ("reader"),

    // the location that the reader is in
    CURRENT_LOCATION ("current_location"),

    // person in the current location
    LOCAL_PERSON ("local_person"),
    ;

    private String abbreviation;

    EntityType(String abbr) {
        abbreviation = abbr;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public static EntityType getTypeByAbbreviation(String abbr) {
        EntityType found = null;
        for (EntityType type : EntityType.values()) {
            if (type.getAbbreviation().equalsIgnoreCase(abbr)) {
                found = type;
            }
        }
        return found;
    }
}
