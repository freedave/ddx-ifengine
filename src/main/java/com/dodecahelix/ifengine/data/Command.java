/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.util.StringUtils;

/**
 *  A command is something that you can "do" with or to an entity.
 *
 */
public class Command implements Serializable {

    private static final long serialVersionUID = 7305913873796228971L;

    private String referenceId;

    /**
     *   For a specific entity, this will override the item-type entity command with the given reference ID
     */
    private String overrideId;

    /**
     *  How command is displayed in the choice list on the reader client.  This value can have dynamic tokens.
     */
    private String commandDisplay;

    private EntityType parentEntityType;

    /**
     *  Reference id of the entity this command belongs to.
     *
     *  "this" is a special ID used with commands that apply to all entities of this type.
     */
    private String parentEntityId;

    /**
     *   Conditions that must resolve to true for this command to be visible as a choice.
     *
     *  (optional -- if no validations, this command will always appear)
     */
    private ConditionSet validations = new ConditionSet();

    /**
     *  A command can have many possible outcomes.
     *
     *  For each called command, the engine will iterate through the outcomes (in order),
     *  and the first one to validate will be executed.
     *  <p>
     *  If none of the outcomes are valid, this will  run the default executions, and print the default message
     *
     */
    private List<Action> outcomes = new ArrayList<Action>();

    /**
     *  If no outcomes are valid, print this default message
     */
    private String defaultExecutionMessage;

    /**
     *   If there are no valid outcomes, perform this set of executions as a default
     */
    private ExecutionSet defaultResults = new ExecutionSet();

    /**
     *   Time, in 'increments' to execute the command.  A number >0 will flag the Command as being temporal.
     */
    private int timeToExecute = 0;

    /**
     *   Indicates that this command can have multiple targets, and each target should have its own option
     *   presented in the interface.
     *
     *   TODO : target should be filtered by a condition
     *
     */
    private boolean multiTarget;

    /**
     *  Different command types may have different presentations in the reader client.
     */
    private CommandType commandType = CommandType.ACTION;

    public Command() {
    }

    public Command(String display, EntityType ownerEntityType) {
        this.commandDisplay = display;
        this.parentEntityType = ownerEntityType;
    }

    public boolean isTemporal() {
        boolean temporal = false;
        if (timeToExecute>0) {
            temporal = true;
        }
        return temporal;
    }

    public String getCommandDisplay() {
        return commandDisplay;
    }

    public void setCommandDisplay(String commandDisplay) {
        this.commandDisplay = commandDisplay;
    }

    public ConditionSet getValidations() {
        return validations;
    }

    public void setValidations(ConditionSet validations) {
        this.validations = validations;
    }

    public List<Action> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(List<Action> consequences) {
        this.outcomes = consequences;
    }

    public String getDefaultExecutionMessage() {
        return defaultExecutionMessage;
    }

    public void setDefaultExecutionMessage(String defaultExecutionMessage) {
        this.defaultExecutionMessage = defaultExecutionMessage;
    }

    public ExecutionSet getDefaultResults() {
        return defaultResults;
    }

    public void setDefaultResults(ExecutionSet defaultResults) {
        this.defaultResults = defaultResults;
    }

    public EntityType getParentEntityType() {
        return parentEntityType;
    }

    public void setParentEntityType(EntityType parentEntityType) {
        this.parentEntityType = parentEntityType;
    }

    public void addOutcome(Action consequence) {
        this.outcomes.add(consequence);
    }

    public void addValidation(Condition validation) {
        this.validations.getConditions().add(validation);
    }

    public void addDefaultResult(Execution result) {
        this.defaultResults.getExecutions().add(result);
    }

    public int getTimeToExecute() {
        return timeToExecute;
    }

    public void setTimeToExecute(int timeToExecute) {
        this.timeToExecute = timeToExecute;
    }

    public String getParentEntityId() {
        return parentEntityId;
    }

    public void setParentEntityId(String parentEntityId) {
        this.parentEntityId = parentEntityId;
    }

    public boolean isMultiTarget() {
        return multiTarget;
    }

    public void setMultiTarget(boolean multiTarget) {
        this.multiTarget = multiTarget;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    public String getOverrideId() {
        return overrideId;
    }

    public void setOverrideId(String overrideId) {
        this.overrideId = overrideId;
    }

    @Override
    public String toString() {
        return "Command{" +
        "overrideId='" + overrideId + '\'' +
        ", referenceId='" + referenceId + '\'' +
        ", commandDisplay='" + commandDisplay + '\'' +
        ", parentEntityType=" + parentEntityType +
        ", parentEntityId='" + parentEntityId + '\'' +
        ", defaultExecutionMessage='" + defaultExecutionMessage + '\'' +
        ", timeToExecute=" + timeToExecute +
        ", multiTarget=" + multiTarget +
        ", commandType=" + commandType +
        '}';
    }
}
