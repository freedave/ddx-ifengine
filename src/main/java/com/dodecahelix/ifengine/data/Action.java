/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;

import com.dodecahelix.ifengine.util.StringUtils;

/**
 *  
 *   An action represents a specific consequence that can occur if its conditions are met.  An action can have any number of changes/executions as a result
 *
 */
public class Action implements Serializable {

    private static final long serialVersionUID = 8001176605811463108L;

    /**
     *  when set to solo, the execution of this action will block checking for all other actions (default) for the command
     */
    private boolean solo = true;

    /**
     *  all validations must be met for action to be executed
     */
    private ConditionSet validations = new ConditionSet();

    /**
     *   What is printed to the console when the action occurs
     */
    private String actionMessage;

    /**
     *   What happens (i.e; data model changes) when the action conditions are met
     */
    private ExecutionSet results = new ExecutionSet();

    public boolean isSolo() {
        return solo;
    }

    public void setSolo(boolean solo) {
        this.solo = solo;
    }

    public ConditionSet getValidations() {
        return validations;
    }

    public void setValidations(ConditionSet validations) {
        this.validations = validations;
    }

    public String getExecutionMessage() {
        return actionMessage;
    }

    public void setExecutionMessage(String executionMessage) {
        this.actionMessage = executionMessage;
    }

    public ExecutionSet getResults() {
        return results;
    }

    public void setResults(ExecutionSet results) {
        this.results = results;
    }

    public void addValidation(Condition validation) {
        this.validations.getConditions().add(validation);
    }

    public void addResult(Execution result) {
        this.results.getExecutions().add(result);
    }

    @Override
    public String toString() {
        return "Action [solo=" + solo + ", validations=" + validations + ", actionMessage=" + StringUtils.truncate(actionMessage, 20, true) + ", results=" + results + "]";
    }



}
