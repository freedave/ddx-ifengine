/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * <p/>
 * Contributors:
 * David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.story;

import java.io.Serializable;


/**
 *   Library Card (metadata) information for the story
 *
 */
public class LibraryCard implements Serializable {

    private static final long serialVersionUID = -3115056283460153559L;

    private String title;
    private String author;
    private Genre genre;
    private String synopsis;
    private ContentRating contentRating;
    private StorySize storySize;
    private String version;
    private String acknowledgements;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public ContentRating getContentRating() {
        return contentRating;
    }

    public void setContentRating(ContentRating contentRating) {
        this.contentRating = contentRating;
    }

    public StorySize getStorySize() {
        return storySize;
    }

    public void setStorySize(StorySize storySize) {
        this.storySize = storySize;
    }

    public String getAcknowledgements() { return acknowledgements; }

    public void setAcknowledgements(String acknowledgements) { this.acknowledgements = acknowledgements; }

    @Override
    public String toString() {
        return "LibraryCard{" +
        "title='" + title + '\'' +
        ", author='" + author + '\'' +
        ", genre=" + genre +
        ", synopsis='" + synopsis + '\'' +
        ", contentRating=" + contentRating +
        ", storySize=" + storySize +
        ", version='" + version + '\'' +
        ", acknowledgements='" + acknowledgements + '\'' +
        '}';
    }
}
