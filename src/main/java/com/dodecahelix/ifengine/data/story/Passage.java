/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.story;

import java.io.Serializable;

/**
 *   This is an element of textual content of a story, like a chapter introduction or conclusion, a verse or an episode
 *
 */
public class Passage implements Serializable {

    private static final long serialVersionUID = -2907085988447710570L;

    private String passageId;
    private String passageText;

    public Passage(String id, String text) {
        this.passageId = id;
        this.passageText = text;
    }

    public String getPassageId() {
        return passageId;
    }
    public void setPassageId(String passageId) {
        this.passageId = passageId;
    }
    public String getPassageText() {
        return passageText;
    }
    public void setPassageText(String passageText) {
        this.passageText = passageText;
    }



}
