/*
 *  Copyright (c) 2015 David Peters
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and limitations under the License.
 *
 *  Contributors:
 *      David Peters - initial API and implementation
 */
package com.dodecahelix.ifengine.data.story;

import java.io.Serializable;

/**
 * Created on 8/1/2016.
 */
public class Conclusion implements Serializable {

    private static final long serialVersionUID = 317792921096112864L;

    private String conclusionId;
    private String conclusionText;

    public Conclusion(String conclusionId, String conclusionText) {
        this.conclusionId = conclusionId;
        this.conclusionText = conclusionText;
    }

    public String getConclusionId() {
        return conclusionId;
    }

    public void setConclusionId(String conclusionId) {
        this.conclusionId = conclusionId;
    }

    public String getConclusionText() {
        return conclusionText;
    }

    public void setConclusionText(String conclusionText) {
        this.conclusionText = conclusionText;
    }
}
