/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.story;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class StoryContent implements Serializable {

    private static final long serialVersionUID = -2423552930808651058L;

    public static final String INTRO_PASSAGE_ID = "introduction";

    private String introduction;

    private Set<Conclusion> conclusions = new HashSet<>();

    private Set<Passage> passages = new HashSet<Passage>();

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Set<Passage> getPassages() {
        return passages;
    }

    public void setPassages(Set<Passage> passages) {
        this.passages = passages;
    }

    public Set<Conclusion> getConclusions() {
        return conclusions;
    }

    public void setConclusions(Set<Conclusion> conclusions) {
        this.conclusions = conclusions;
    }
}
