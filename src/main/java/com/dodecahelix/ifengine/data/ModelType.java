/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.util.HashSet;

import com.dodecahelix.ifengine.data.configuration.AnatomyConfiguration;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.data.configuration.Configuration;
import com.dodecahelix.ifengine.data.configuration.DialogConfiguration;
import com.dodecahelix.ifengine.data.configuration.MessageConfiguration;
import com.dodecahelix.ifengine.data.configuration.TimeConfiguration;
import com.dodecahelix.ifengine.data.story.Conclusion;
import com.dodecahelix.ifengine.data.story.Passage;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.data.story.LibraryCard;


/**
 *   This is helpful when using switch statements for model classes
 *   
 * @author dpeters
 *
 */
public enum ModelType {

    COMMAND (Command.class),
    CONDITION (Condition.class),
    ACTION (Action.class),
    DIALOG (Dialog.class),
    ENVIRONMENT (Environment.class),
    EXECUTION (Execution.class),
    EXIT (Exit.class),
    ITEM (Item.class),
    LOCATION (Location.class),
    MASS (Mass.class),
    PERSON (Person.class),
    PROPERTY (Property.class),
    READER (Reader.class),
    SCHEDULE (Schedule.class),
    STATISTIC (Statistic.class),
    TIME (Time.class),
    CONDITION_SET (ConditionSet.class),
    EXECUTION_SET (ExecutionSet.class),
    SHARED_CONDITIONS (HashSet.class),
    SHARED_EXECUTIONS (HashSet.class),
    CONFIGURATION (Configuration.class),
    ANATOMY_CONFIGURATION (AnatomyConfiguration.class),
    COMMAND_CONFIGURATION (CommandConfiguration.class),
    ENTITY_COMMAND_ITEMS (CommandConfiguration.class),
    ENTITY_COMMAND_MASSES (CommandConfiguration.class),
    ENTITY_COMMAND_LOCATIONS (CommandConfiguration.class),
    ENTITY_COMMAND_PEOPLE (CommandConfiguration.class),
    ENTITY_COMMAND_NARRATORS (CommandConfiguration.class),
    DIALOG_CONFIGURATION (DialogConfiguration.class),
    MESSAGE_CONFIGURATION (MessageConfiguration.class),
    TIME_CONFIGURATION (TimeConfiguration.class),
    STORY_METADATA (LibraryCard.class),
    STORY_CONTENT (StoryContent.class),
    STORY_PASSAGE (Passage.class),
    STORY_CONCLUSION (Conclusion.class),
    RANGE_DISPLAY (RangeDisplay.class),
    BOOLEAN_DISPLAY (BooleanDisplay.class),
    ;


    private Class<?> clazz;

    ModelType(Class<?> clazz) {
        this.clazz = clazz;
    }

    public Class<?> getTypeClass() {
        return clazz;
    }

    public static ModelType forClass(Class<?> clazz) {
        ModelType modelType = null;
        for (ModelType type : ModelType.values()) {
            if (type.getTypeClass().equals(clazz)) {
                return type;
            }
        }
        return modelType;
    }
}
