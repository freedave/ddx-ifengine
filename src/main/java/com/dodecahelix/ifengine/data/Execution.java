/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;

import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;

/**
 *   environment update that is executed as a result of a command
 *  
 * @author dpeters
 *
 */
public class Execution implements Serializable {

    private static final long serialVersionUID = 7463762822529189024L;

    private ExecutionType executionType;

    private ExecutionOperator operator;

    private TargetType targetType;

    private EntityHolder entityHolder = new EntityHolder();

    private String targetStaticValue;

    public Execution() {
    }

    public Execution(ExecutionType executionType) {
        this.executionType = executionType;
    }

    public ExecutionType getExecutionType() {
        return executionType;
    }

    public void setExecutionType(ExecutionType executionType) {
        this.executionType = executionType;
    }

    public ExecutionOperator getOperator() {
        return operator;
    }

    public void setOperator(ExecutionOperator operator) {
        this.operator = operator;
    }

    public TargetType getTargetType() {
        return targetType;
    }

    public void setTargetType(TargetType targetType) {
        this.targetType = targetType;
    }

    public EntityHolder getEntityHolder() {
        return entityHolder;
    }

    public void setEntityHolder(EntityHolder entityHolder) {
        this.entityHolder = entityHolder;
    }

    public String getTargetStaticValue() {
        return targetStaticValue;
    }

    public void setTargetStaticValue(String targetStaticValue) {
        this.targetStaticValue = targetStaticValue;
    }

    @Override
    public String toString() {
        return "Execution{" +
        "executionType=" + executionType +
        ", operator=" + operator +
        ", targetType=" + targetType +
        ", entityHolder=" + entityHolder +
        ", targetStaticValue='" + targetStaticValue + '\'' +
        '}';
    }
}
