/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.util.IdUtil;

/**
 * A 'person' is a third-party character in the story, whose actions are controlled by AI.
 *
 */
public class Person extends Entity {

    private static final long serialVersionUID = -3652491750328905661L;

    /**
     *   List of dialogs that you may ask this person
     */
    private List<Dialog> dialogs = new ArrayList<Dialog>();

    /**
     *   If this person is following the reader.
     */
    private boolean following = false;

    /**
     *   The description that is printed when entering a location while following the reader
     */
    private String followingDescription;

    /**
     *   To dynamically set a schedule (via its reference ID), overridding all other schedules
     *
     */
    private String overrideSchedule;

    /**
     *  Optional.  If in the time range and not in the location, this will move the person to the location.  If in the same room as the player, this will print a message that the person leaves the room
     */
    private List<Schedule> schedules = new ArrayList<Schedule>();

    public Person() {
    }

    public Person(String title, String description) {
        this(IdUtil.fromTitle(title), title, description);
    }

    public Person(String id, String title, String description) {
        this.setId(id);
        this.setTitle(title.toLowerCase());
        this.setDescription(description);
    }

    @Override
    public EntityType getEntityType() {
        return EntityType.PERSON;
    }

    public List<Dialog> getDialogs() {
        return dialogs;
    }

    public void setDialogs(List<Dialog> dialogs) {
        this.dialogs = dialogs;
    }

    public void addDialog(Dialog dialog) {
        dialogs.add(dialog);
    }

    public String getOverrideSchedule() {
        return overrideSchedule;
    }

    public void setOverrideSchedule(String overrideSchedule) {
        this.overrideSchedule = overrideSchedule;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    public void addSchedule(Schedule schedule) {
        schedules.add(schedule);
    }

    public boolean isFollowing() {
        return following;
    }

    public void setFollowing(boolean following) {
        this.following = following;
    }

    public String getFollowingDescription() {
        return followingDescription;
    }

    public void setFollowingDescription(String followingDescription) {
        this.followingDescription = followingDescription;
    }

    @Override
    public String toString() {
        return "Person [dialogs=" + dialogs + ", overrideSchedule=" + overrideSchedule + ", schedules=" + schedules + ", following=" + following + ", followingDescription=" + followingDescription + ", entity=" + super.toString() + "]";
    }



}
