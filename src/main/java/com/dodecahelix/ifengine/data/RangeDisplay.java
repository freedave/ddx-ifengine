/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

/**
 *   Assign a string value to a range of numbers (i.e; health = 10 should be displayed as "good health")
 *
 */
public class RangeDisplay implements Display {

    private static final long serialVersionUID = -3600839962786397401L;

    private String display;
    private int lowRange = Integer.MIN_VALUE;
    private int highRange = Integer.MAX_VALUE;

    public RangeDisplay() {
    }

    public RangeDisplay(String display, int lowRange, int highRange) {
        this.lowRange = lowRange;
        this.highRange = highRange;
        this.display = display;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public int getLowRange() {
        return lowRange;
    }

    public void setLowRange(int lowRange) {
        this.lowRange = lowRange;
    }

    public int getHighRange() {
        return highRange;
    }

    public void setHighRange(int highRange) {
        this.highRange = highRange;
    }

    @Override
    public String toString() {
        return "RangeDisplay [display=" + display + ", lowRange=" + lowRange + ", highRange=" + highRange + "]";
    }

}
