/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;

import com.dodecahelix.ifengine.data.configuration.TimeConfiguration;

/**
 *   Simple time object where fields are mapped as integers
 *
 */
public class Time implements Serializable {

    private static final long serialVersionUID = -5634716446879337252L;

    public static final int MAX_INCREMENT = 999;
    private static final String SEPARATOR_TOKEN = "-";

    private int increment;  // (i.e; an hour; 6 hours per period)
    private int period;     // (i.e; afternoon, evening, starts at 1)
    private int weekday;    // (i.e; Monday, starts at 1)
    private int week;       // (i.e; 2nd week of February - starts at 1)
    private int month;      // (i.e; February, starts at 1)
    private int year;       // (i.e; 1997)

    public Time(TimeConfiguration timeConfig) {
        increment = 1;

        // set up the start time
        this.period = timeConfig.getStartPeriod();
        this.month = timeConfig.getStartMonth();
        this.year = timeConfig.getStartingYear();
        this.weekday = timeConfig.getStartDayOfMonth() % timeConfig.getDaysOfWeek().size();
        this.week = timeConfig.getStartDayOfMonth() / timeConfig.getDaysOfWeek().size();
    }

    public String toTokenizedString() {
        StringBuffer csvString = new StringBuffer();
        csvString.append(increment);
        csvString.append(SEPARATOR_TOKEN);
        csvString.append(period);
        csvString.append(SEPARATOR_TOKEN);
        csvString.append(weekday);
        csvString.append(SEPARATOR_TOKEN);
        csvString.append(week);
        csvString.append(SEPARATOR_TOKEN);
        csvString.append(month);
        csvString.append(SEPARATOR_TOKEN);
        csvString.append(year);

        return csvString.toString();
    }

    public void fromTokenizedString(String csv) {
        String[] timeTokens = csv.split(SEPARATOR_TOKEN);

        this.increment = Integer.parseInt(timeTokens[0]);
        this.period = Integer.parseInt(timeTokens[1]);
        this.weekday = Integer.parseInt(timeTokens[2]);
        this.week = Integer.parseInt(timeTokens[3]);
        this.month = Integer.parseInt(timeTokens[4]);
        this.year = Integer.parseInt(timeTokens[5]);
    }

    public int getIncrement() {
        return increment;
    }

    public void setIncrement(int increment) {
        this.increment = increment;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getWeekday() {
        return weekday;
    }

    public void setWeekday(int weekday) {
        this.weekday = weekday;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Time [increment=" + increment + ", period=" + period + ", weekday=" + weekday + ", week=" + week + ", month=" + month + ", year=" + year + "]";
    }

}
