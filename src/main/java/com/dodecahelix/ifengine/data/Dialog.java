/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.util.StringUtils;

public class Dialog implements Serializable {

    private static final long serialVersionUID = 1318766040212202181L;

    private String referenceId;

    /**
     *   How this dialog option should be displayed
     */
    private String optionDisplay;

    /**
     *   The text response of this dialog.  Allows dynamic text filtering.
     *
     */
    private String response;

    /**
     *   If not-null, this dialog requires knowledge of this topic before being given as an option.
     */
    private String topic;

    /**
     *   Conditions that need to be met for the dialog option to appear
     */
    private ConditionSet validations = new ConditionSet();

    /**
     *   Any environment changes that occur as a result of having this dialog.
     */
    private ExecutionSet results = new ExecutionSet();

    /**
     *   Only allow this dialog to be run once (semi dangerous, if it contains a clue)
     */
    private boolean once = false;

    /**
     *  If set to true, this dialog will not appear as an option in the option tree.
     */
    private boolean disabled = false;

    /**
     *  Dialog options which occur as the direct result of selecting this dialog.
     *
     *  If this dialog has sub-dialogs, then the choice result will contain a list of the sub-dialogs instead of the normal option tree.
     */
    private List<Dialog> subDialogs = new ArrayList<Dialog>();

    public Dialog() {
    }

    public Dialog(String display, String response, String topic) {
        this.optionDisplay = display;
        this.response = response;
        this.topic = topic;
    }

    public void addDialog(Dialog subdialog) {
        if (subDialogs == null) {
            subDialogs = new ArrayList<Dialog>();
        }
        subDialogs.add(subdialog);
    }

    public String getOptionDisplay() {
        return optionDisplay;
    }

    public void setOptionDisplay(String optionDisplay) {
        this.optionDisplay = optionDisplay;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public ExecutionSet getResults() {
        return results;
    }

    public void setResults(ExecutionSet results) {
        this.results = results;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isOnce() {
        return once;
    }

    public void setOnce(boolean once) {
        this.once = once;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void addResult(Execution result) {
        this.results.getExecutions().add(result);
    }

    public void addValidation(Condition validation) {
        this.validations.getConditions().add(validation);
    }

    public ConditionSet getValidations() {
        return validations;
    }

    public void setValidations(ConditionSet validations) {
        this.validations = validations;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public List<Dialog> getSubDialogs() {
        return subDialogs;
    }

    public void setSubDialogs(List<Dialog> subDialogs) {
        this.subDialogs = subDialogs;
    }

    @Override
    public String toString() {
        return "Dialog [optionDisplay=" + optionDisplay + ", referenceId=" + referenceId + ", response=" + StringUtils.truncate(response, 20, true) + ", topic=" + topic + ", validations=" + validations + ", results="
                + results + ", once=" + once + ", disabled=" + disabled + "]";
    }

}
