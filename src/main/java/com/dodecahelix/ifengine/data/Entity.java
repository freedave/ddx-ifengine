/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.dodecahelix.ifengine.util.StringUtils;

public abstract class Entity implements Serializable {

    private static final long serialVersionUID = 4074564070161017082L;

    /**
     *   Unique identifier for this entity.  Will not be visible in the GUI
     */
    private String id;

    /**
     *   Short title, which may be displayed in the GUI
     */
    private String title;

    /**
     *   Long description of this entity.  Displayed when a look command is made
     */
    private String description;

    private Set<Property> properties = new HashSet<Property>();

    /**
     *   What items (by ID) this entity may contain
     */
    private Set<String> contents = new HashSet<String>();

    /**
     *  What commands this entity supports (make an array so they show up in order in the interface)
     */
    private List<Command> commands = new ArrayList<Command>();

    /**
     *   Timed actions - "consequences" checked at the end of each turn.
     */
    private List<Action> actions = new ArrayList<Action>();

    /**
     *  indicate the total capacity in terms of item size this entity can handle
     *  <br>if not a container, this value should be zero
     *
     */
    private int carrySize = 0;

    public abstract EntityType getEntityType();

    public Set<Property> getProperties() {
        return properties;
    }

    public Property getPropertyById(String propId) {
        Property foundProp = null;
        if (propId!=null) {
            for (Property property : properties) {
                if (propId.equalsIgnoreCase(property.getPropertyId())) {
                    foundProp = property;
                    break;
                }
            }
        }
        return foundProp;
    }

    /**
     *   @deprecated : Inefficient because this will cause parseObjectAndTypeFromString twice.  Use setProperty instead
     *
     * @param property
     */
    @Deprecated
    public void addProperty(Property property) {
        this.setProperty(property.getPropertyId(), property.getStringValue());
    }

    /**
     *   Sets the property if it already exists, otherwise creates a new one.  Returns the updated or new property
     * @param propertyId
     * @param propertyValue
     * @return
     */
    public Property setProperty(String propertyId, String propertyValue) {
        // if property already exists, just update the value
        Property prop = this.getPropertyById(propertyId);
        if (prop!=null) {
            // an existing prop, parse it
            prop.parseObjectAndTypeFromString(propertyValue);
        } else {
            prop = new Property(propertyId, propertyValue);
            properties.add(prop);
        }

        return prop;
    }

    public Set<String> getContents() {
        return contents;
    }

    public void setContents(Set<String> contents) {
        this.contents = contents;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addContentItem(String itemId) {
        contents.add(itemId);
    }

    public void addCommand(Command command) {
        commands.add(command);
    }

    public List<Command> getCommands() {
        return commands;
    }

    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public void addAction(Action action) {
        actions.add(action);
    }

    public int getCarrySize() {
        return carrySize;
    }

    public void setCarrySize(int carrySize) {
        this.carrySize = carrySize;
    }

    @Override
    public String toString() {
        return "Entity [id=" + id + ", title=" + title + ", description=" + StringUtils.truncate(description, 20, true) + ", properties=" + properties + ", contents=" + contents
                + ", commands=" + commands + ", actions=" + actions + ", carrySize=" + carrySize + "]";
    }


}
