/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.save.csv;

import java.util.List;

import com.dodecahelix.ifengine.dao.EnvironmentBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.save.EnvironmentComparator;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.save.EnvironmentSaveBuilder;

/**
 *  @Deprecated - CSV no longer supported for save data, use JSON instead
 */ 
@Deprecated
public class CsvEnvironmentSaveBuilder implements EnvironmentSaveBuilder {

    private EnvironmentDAO environmentDao;
    private EnvironmentBuilder environmentBuilder;
    private EnvironmentComparator saveGameFileGenerator;

    public CsvEnvironmentSaveBuilder(EnvironmentDAO environmentDao, EnvironmentBuilder environmentBuilder) {
        this.environmentDao = environmentDao;
        this.environmentBuilder = environmentBuilder;

        this.saveGameFileGenerator = new CsvEnvironmentComparator();
    }

    @Override
    public EnvironmentSave buildFromEnvironment(Environment dirtyEnvironment, String saveName) {
        Environment defaultEnvironment = environmentBuilder.build();

        // environment = EnvironmentCloneTool.clone(dirtyEnvironment);
        List<String> saveGameLines = saveGameFileGenerator.compareEnvironment(defaultEnvironment, dirtyEnvironment);

        // TODO - support JSON format?
        EnvironmentSave environmentSave = new CsvEnvironmentSave(saveName, saveGameLines);
        return environmentSave;
    }

}
