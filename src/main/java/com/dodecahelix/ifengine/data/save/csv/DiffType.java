/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.save.csv;

/**
 *   Diff tag flags in save files
 *
 */
public enum DiffType {

    CONTENT_ITEM_REMOVED ("content.item.removed"),
    CONTENT_ITEM_ADDED ("content.item.added"),
    PROPERTY_CHANGED ("property.changed"),
    SET_LOCATION ("set.location"),
    SET_CURRENT_NARRATOR ("set.current.narrator"),
    SET_CURRENT_TIME ("set.current.time"),
    SET_FROZEN_IN_TIME ("set.frozen.in.time"),
    SET_SCHEDULE_OVERRIDE ("set.schedule.override"),
    SET_ITEM_HIDDEN("set.item.hidden"),
    SET_ITEM_WORN ("set.item.worn"),
    SET_ITEM_USES ("set.item.uses"),
    SKILL_CHANGED ("skill.changed"),
    STAT_CHANGED ("stat.changed"),
    ATTRIBUTE_CHANGED ("attribute.changed"),
    SET_KNOWN_TOPIC ("set.known.topic"),
    ;

    private String typeToken;

    DiffType(String typeToken) {
        this.typeToken = typeToken;
    }

    public String getTypeToken() {
        return typeToken;
    }

    public static DiffType fromTypeToken(String diffType) {
        for (DiffType type : DiffType.values()) {
            if (type.getTypeToken().equalsIgnoreCase(diffType)) {
                return type;
            }
        }
        return null;
    }

}
