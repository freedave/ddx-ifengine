/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.save.csv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.save.EnvironmentComparator;

/**
 *  @Deprecated - CSV no longer supported for save data, use JSON instead
 */ 
@Deprecated
public class CsvEnvironmentComparator implements EnvironmentComparator {

    private List<String> environmentDifferences;
    private Map<String, Entity> dirtyEntityMap;

    @Override
    public List<String> compareEnvironment(Environment defaultEnvironment, Environment dirtyEnvironment) {

        environmentDifferences = new ArrayList<String>();
        buildDirtyEntityMap(dirtyEnvironment);

        // compare environment entities
        compareEnvironmentEntity(defaultEnvironment, dirtyEnvironment);

        for (Reader narrator : defaultEnvironment.getNarrators()) {
            Reader dirtyReader = (Reader)dirtyEntityMap.get(narrator.getId());
            compareReaderEntity(narrator, dirtyReader);
        }

        // compare location entities
        for (Location location : defaultEnvironment.getLocations()) {
            Location dirtyLocation = (Location) dirtyEntityMap.get(location.getId());
            compareLocationEntity(location, dirtyLocation);
        }

        for (Mass mass : defaultEnvironment.getMasses()) {
            Mass dirtyMass = (Mass)dirtyEntityMap.get(mass.getId());
            compareMassEntity(mass, dirtyMass);
        }

        for (Person person : defaultEnvironment.getPeople()) {
            Person dirtyPerson = (Person)dirtyEntityMap.get(person.getId());
            comparePersonEntity(person, dirtyPerson);
        }

        for (Item item : defaultEnvironment.getItems()) {
            Item dirtyItem = (Item)dirtyEntityMap.get(item.getId());
            compareItemEntity(item, dirtyItem);
        }

        return environmentDifferences;
    }

    /**
     *  Maps all entities by their ID
     *
     * @param environment
     * @return
     */
    private void buildDirtyEntityMap(Environment environment) {
        dirtyEntityMap = new HashMap<String, Entity>();

        for (Reader narrator : environment.getNarrators()) {
            dirtyEntityMap.put(narrator.getId(), narrator);
        }
        for (Item item : environment.getItems()) {
            dirtyEntityMap.put(item.getId(), item);
        }
        for (Location location : environment.getLocations()) {
            dirtyEntityMap.put(location.getId(), location);
        }
        for (Mass mass : environment.getMasses()) {
            dirtyEntityMap.put(mass.getId(), mass);
        }
        for (Person person : environment.getPeople()) {
            dirtyEntityMap.put(person.getId(), person);
        }
    }

    private void compareEnvironmentEntity(Environment defaultEnvironment, Environment dirtyEnvironment) {
        compareContents(EntityType.ENVIRONMENT, null, defaultEnvironment.getContents(), dirtyEnvironment.getContents());
        compareDynamicProperties(EntityType.ENVIRONMENT, DiffType.PROPERTY_CHANGED, null, defaultEnvironment.getProperties(), dirtyEnvironment.getProperties());

        // set the current reader
        environmentDifferences.add(DiffStringBuilder.buildDiffString(DiffType.SET_CURRENT_NARRATOR, EntityType.ENVIRONMENT, null, null, dirtyEnvironment.getCurrentNarrator()));

        // set the time
        environmentDifferences.add(DiffStringBuilder.buildDiffString(DiffType.SET_CURRENT_TIME, EntityType.ENVIRONMENT, null, null, dirtyEnvironment.getCurrentTime().toTokenizedString()));

        // set the frozen time property
        environmentDifferences.add(DiffStringBuilder.buildDiffString(DiffType.SET_FROZEN_IN_TIME, EntityType.ENVIRONMENT, null, null, String.valueOf(dirtyEnvironment.isFrozenInTime())));

    }

    private void compareReaderEntity(Reader defaultReader, Reader dirtyReader) {
        compareContents(EntityType.READER, null, defaultReader.getContents(), dirtyReader.getContents());
        compareDynamicProperties(EntityType.READER, DiffType.PROPERTY_CHANGED, null, defaultReader.getProperties(), dirtyReader.getProperties());

        // set the current location
        environmentDifferences.add(DiffStringBuilder.buildDiffString(DiffType.SET_LOCATION, EntityType.READER, null, null, dirtyReader.getCurrentLocation()));

        // TODO - compare topics
        for (String topic : dirtyReader.getKnownTopics()) {
            environmentDifferences.add(DiffStringBuilder.buildDiffString(DiffType.SET_KNOWN_TOPIC, EntityType.READER, null, null, topic));
        }
    }

    private void compareLocationEntity(Location location, Location dirtyLocation) {
        compareContents(EntityType.LOCATION, location.getId(), location.getContents(), dirtyLocation.getContents());
        compareDynamicProperties(EntityType.LOCATION, DiffType.PROPERTY_CHANGED, location.getId(), location.getProperties(), dirtyLocation.getProperties());

        // TODO - what about exits?  static?
    }

    private void comparePersonEntity(Person person, Person dirtyPerson) {
        compareContents(EntityType.PERSON, person.getId(), person.getContents(), dirtyPerson.getContents());
        compareDynamicProperties(EntityType.PERSON, DiffType.PROPERTY_CHANGED, person.getId(), person.getProperties(), dirtyPerson.getProperties());

        if (dirtyPerson.getOverrideSchedule()!=null) {
            environmentDifferences.add(DiffStringBuilder.buildDiffString(DiffType.SET_SCHEDULE_OVERRIDE, EntityType.PERSON, dirtyPerson.getId(), null, String.valueOf(dirtyPerson.getOverrideSchedule())));
        }
    }

    private void compareMassEntity(Mass mass, Mass dirtyMass) {
        compareContents(EntityType.MASS, mass.getId(), mass.getContents(), dirtyMass.getContents());
        compareDynamicProperties(EntityType.MASS, DiffType.PROPERTY_CHANGED, mass.getId(), mass.getProperties(), dirtyMass.getProperties());
    }

    private void compareItemEntity(Item item, Item dirtyItem) {
        compareDynamicProperties(EntityType.ITEM, DiffType.PROPERTY_CHANGED, item.getId(), item.getProperties(), dirtyItem.getProperties());

        // compare WORN property
        environmentDifferences.add(DiffStringBuilder.buildDiffString(DiffType.SET_ITEM_WORN, EntityType.ITEM, item.getId(), null, String.valueOf(dirtyItem.isWorn())));
    }

    /**
     *   Figure out the differences between the dynamic properties of an entity and change them
     *
     * @param entityType
     * @param entityId
     * @param defaultProperties
     * @param dirtyProperties
     */
    private void compareDynamicProperties(EntityType entityType, DiffType diffType, String entityId, Set<Property> defaultProperties, Set<Property> dirtyProperties) {
        for (Property property : defaultProperties) {
            String originalValue = property.getStringValue();
            for (Property dirtyProperty : dirtyProperties) {
                if ((property.getPropertyId().equalsIgnoreCase(dirtyProperty.getPropertyId()))
                    && (!dirtyProperty.getStringValue().equalsIgnoreCase(originalValue))) {
                        environmentDifferences.add(DiffStringBuilder.buildDiffString(diffType, entityType, entityId, property.getPropertyId(), dirtyProperty.getStringValue()));
                }
            }
        }
    }

    private void compareContents(EntityType entityType, String entityId, Set<String> defaultContents, Set<String> dirtyContents) {
        // see which items have been added
        // which items have been removed
        for (String contentId : defaultContents) {
            boolean contentStillOwned = dirtyContents.remove(contentId);
            if (!contentStillOwned) {
                // content removed
                environmentDifferences.add(DiffStringBuilder.buildDiffString(DiffType.CONTENT_ITEM_REMOVED, entityType, entityId, null, contentId));
            }
        }

        // if the content hasn't been removed, it is a new item
        for (String contentId : dirtyContents) {
            environmentDifferences.add(DiffStringBuilder.buildDiffString(DiffType.CONTENT_ITEM_ADDED, entityType, entityId, null, contentId));
        }

    }

}
