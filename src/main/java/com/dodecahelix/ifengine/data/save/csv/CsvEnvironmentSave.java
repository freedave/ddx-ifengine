/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.save.csv;

import java.util.List;

import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.serializer.MediaFormat;

public class CsvEnvironmentSave implements EnvironmentSave {

    private List<String> csvLines;
    private String name;

    public CsvEnvironmentSave(String name, List<String> csvLines) {
        super();
        this.csvLines = csvLines;
        this.name = name;
    }

    public List<String> getCsvLines() {
        return csvLines;
    }

    public void setCsvLines(List<String> csvLines) {
        this.csvLines = csvLines;
    }

    @Override
    public MediaFormat getDataFormat() {
        return MediaFormat.CSV;
    }

    @Override
    public String getName() {
        return name;
    }



}
