/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.save.csv;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.save.EnvironmentSaveLoader;
import com.dodecahelix.ifengine.data.serializer.csv.ParsedDiff;

/**
 *  @Deprecated - CSV no longer supported for save data, use JSON instead
 */ 
@Deprecated
public class CsvEnvironmentSaveEnvironmentLoader implements EnvironmentSaveLoader {

    protected final static Logger LOGGER = Logger.getLogger(CsvEnvironmentSaveEnvironmentLoader.class.getName());

    private EnvironmentDAO dao;

    public CsvEnvironmentSaveEnvironmentLoader(EnvironmentDAO dao) {
        this.dao = dao;
    }

    @Override
    public Environment buildEnvironmentFromSave(EnvironmentSave environmentSave) {

        List<String> saveGameLines = ((CsvEnvironmentSave) environmentSave).getCsvLines();
        for (String diffLine : saveGameLines) {
            LOGGER.log(Level.FINE, "Loading diff line " + diffLine);

            ParsedDiff diff = DiffParser.parse(diffLine);

            // grab the subject entity
            Entity entity = dao.getEntityById(diff.getEntity(), diff.getEntityId());

            switch (diff.getDiffType()) {
                case CONTENT_ITEM_ADDED: entity.getContents().add(diff.getStaticValue()); break;
                case CONTENT_ITEM_REMOVED: entity.getContents().remove(diff.getStaticValue()); break;
                case PROPERTY_CHANGED: entity.setProperty(diff.getEntityProperty(), diff.getStaticValue()); break;
                case SET_LOCATION: ((Reader)entity).setCurrentLocation(diff.getStaticValue()); break;
                case SET_CURRENT_NARRATOR: ((Environment)entity).setCurrentNarrator(diff.getStaticValue()); break;
                case SET_CURRENT_TIME: ((Environment)entity).getCurrentTime().fromTokenizedString(diff.getStaticValue()); break;
                case SET_FROZEN_IN_TIME: ((Environment)entity).setFrozenInTime(Boolean.parseBoolean(diff.getStaticValue())); break;
                case SET_ITEM_WORN: ((Item)entity).setWorn(Boolean.parseBoolean(diff.getStaticValue())); break;
                case SET_KNOWN_TOPIC: ((Reader)entity).addTopic(diff.getStaticValue()); break;
                case SET_SCHEDULE_OVERRIDE: ((Person)entity).setOverrideSchedule(diff.getStaticValue()); break;
                default: LOGGER.warning("DiffType is unrecognized : " + diff.getDiffType());
                    break;
            }
        }

        return dao.getEnvironment();
    }


}
