/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.save;

import com.dodecahelix.ifengine.data.Environment;

/**
 *   Transforms the Environment model into a format that can be saved/stored.
 */
public interface EnvironmentSaveBuilder {

    /**
     * Builds an artifact (i.e; serialized, encrypted, and compressed file) that represents the current state of the environment, identified by the given name.
     *
     * The "reader" client will store this content to its file system using whatever IO api exists for that platform.
     *
     * @param environment
     * @param saveName
     * @return
     */
    public EnvironmentSave buildFromEnvironment(Environment environment, String saveName);

}
