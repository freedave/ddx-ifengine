/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.save.json;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.save.EnvironmentSaveLoader;
import com.dodecahelix.ifengine.data.serializer.MediaFormat;
import com.google.gson.Gson;

public class JsonEnvironmentSaveLoader implements EnvironmentSaveLoader {

    private final static Logger LOGGER = LoggerFactory.getLogger(JsonEnvironmentSaveLoader.class);

    private Gson gson;

    public JsonEnvironmentSaveLoader() {
        this.gson = new Gson();
    }

    public Environment buildEnvironmentFromSave(EnvironmentSave environmentSave) {
        LOGGER.info("loading environment from EnvironmentSave {} using JSON", environmentSave.getName());

        if (MediaFormat.JSON != environmentSave.getDataFormat()) {
            throw new IllegalArgumentException("EnvironmentSave has incorrect format " + environmentSave.getDataFormat() + ".  expecting json");
        }
        JsonEnvironmentSave jsonSaveGame = (JsonEnvironmentSave) environmentSave;
        String jsonEnvironment = jsonSaveGame.getContent();

        return gson.fromJson(jsonEnvironment, Environment.class);
    }

}
