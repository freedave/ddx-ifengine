/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.save.csv;

import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.serializer.csv.ParsedDiff;
import com.dodecahelix.ifengine.entity.EntityParser;
import com.dodecahelix.ifengine.entity.EntityParser.EntityOperand;
import com.dodecahelix.ifengine.entity.ParseException;

public class DiffParser {

    public static ParsedDiff parse(String diffString) {
        ParsedDiff diff = new ParsedDiff();

        String[] diffTokens = diffString.split(DiffStringBuilder.SEPARATOR_TOKEN);
        String diffType = diffTokens[0];
        diff.setDiffType(DiffType.fromTypeToken(diffType));

        String entityString = diffTokens[1];
        try {
            EntityHolder entityHolder = EntityParser.parseEntity(entityString, EntityOperand.SUBJECT);
            diff.setEntity(entityHolder.getSubjectType());
            diff.setEntityId(entityHolder.getSubjectEntityId());
            diff.setEntityProperty(entityHolder.getSubjectProperty());
        } catch (ParseException e) {
            throw new IllegalStateException("Could not parse entity string " + entityString, e);
        }

        if (diffTokens.length==3) {
            diff.setStaticValue(diffTokens[2]);
        }

        return diff;
    }

}
