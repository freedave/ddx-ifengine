/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import com.dodecahelix.ifengine.util.IdUtil;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *  An item is an IF entity that can be "owned" by entities, and passed around to other entities via the change_ownership execution.
 *
 *  (i.e; the flashlight which is carried around by the narrator, the screwdriver inside of the toolbag mass, the flower in the garden location)
 *
 */
public class Item extends Entity {

    private static final long serialVersionUID = 2757284339562534889L;

    /**
     *  size indicates how big the item is, how many "slots" it will fill up in someone's backpack for example
     *  <br>By default, all items have a size of 1.
     */
    private int size = 1;

    /**
     *   What body part you can wear this on (i.e; head, feet, etc..)
     *   <br>If this is null, the item is not wearable;
     */
    private String wearPart;

    /**
     *   Whether or not the item is being worn by its owner
     */
    private boolean worn = false;

    /**
     *   Whether the item is a light source and allows usage in the dark
     */
    private boolean lightSource = false;

    public Item() {
    }

    public Item(String title, String description) {
        this(IdUtil.fromTitle(title), title, description);
    }

    public Item(String id, String title, String description) {
        this.setId(id);
        this.setTitle(title);
        this.setDescription(description);
    }

    @Override
    public EntityType getEntityType() {
        return EntityType.ITEM;
    }

    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * @return the wearPart
     */
    public String getWearPart() {
        return wearPart;
    }

    /**
     * @param wearPart the wearPart to set
     */
    public void setWearPart(String wearPart) {
        this.wearPart = wearPart;
    }

    /**
     * @return the worn
     */
    public boolean isWorn() {
        return worn;
    }

    /**
     * @param worn the worn to set
     */
    public void setWorn(boolean worn) {
        this.worn = worn;
    }

    public boolean isWearable() {
        boolean wearable = false;
        if (!StringUtils.isEmpty(wearPart)) {
            wearable = true;
        }
        return wearable;
    }

    public boolean isLightSource() {
        return lightSource;
    }

    public void setLightSource(boolean lightSource) {
        this.lightSource = lightSource;
    }

    @Override
    public String toString() {
        return "Item{" +
        "size=" + size +
        ", wearPart='" + wearPart + '\'' +
        ", worn=" + worn +
        ", lightSource=" + lightSource +
        '}';
    }
}
