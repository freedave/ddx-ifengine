/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.condition.TargetType;

public class Condition implements Serializable {

    private static final long serialVersionUID = 504550750971424040L;

    private ConditionType type;

    private EntityHolder entityHolder;

    private ConditionOperator operator;

    private TargetType targetType;

    private String targetStaticValue;

    public Condition(ConditionType conditionType) {
        this.type = conditionType;
        entityHolder = new EntityHolder();

        // default to true
        operator = ConditionOperator.TRUE;
    }

    public ConditionType getType() {
        return type;
    }

    public void setType(ConditionType type) {
        this.type = type;
    }

    public EntityHolder getEntityHolder() {
        return entityHolder;
    }

    public void setEntityHolder(EntityHolder entityHolder) {
        this.entityHolder = entityHolder;
    }

    public ConditionOperator getOperator() {
        return operator;
    }

    public void setOperator(ConditionOperator operator) {
        this.operator = operator;
    }

    public TargetType getTargetType() {
        return targetType;
    }

    public void setTargetType(TargetType targetType) {
        this.targetType = targetType;
    }

    public String getTargetStaticValue() {
        return targetStaticValue;
    }

    public void setTargetStaticValue(String targetStaticValue) {
        this.targetStaticValue = targetStaticValue;
    }

    @Override
    public String toString() {
        return "Condition{" +
        "type=" + type +
        ", entityHolder=" + entityHolder +
        ", operator=" + operator +
        ", targetType=" + targetType +
        ", targetStaticValue='" + targetStaticValue + '\'' +
        '}';
    }
}
