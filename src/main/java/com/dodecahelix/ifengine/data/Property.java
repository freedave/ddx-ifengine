/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Property implements Serializable {

    private static final long serialVersionUID = -5352599573845301126L;

    private PropertyType type;
    private String propertyId;

    // don't serialize this
    private transient Object value;

    private String stringValue;

    /**
     *   When a property is changed, cycle through these actions
     */
    private List<Action> changeActions = new ArrayList<Action>();

    private List<BooleanDisplay> booleanDisplays = new ArrayList<BooleanDisplay>();
    private List<RangeDisplay> rangeDisplays = new ArrayList<RangeDisplay>();

    public Property() {
        booleanDisplays = new ArrayList<BooleanDisplay>();
        rangeDisplays = new ArrayList<RangeDisplay>();
    }

    public Property(String id, String valueString) {
        this.propertyId = id;

        // parse property type and value from string value:
        parseObjectAndTypeFromString(valueString);
    }

    /**
     *   This is the setter for the property
     *
     * @param valueString
     */
    public void parseObjectAndTypeFromString(String valueString) {
        this.stringValue = valueString;

        type = null;
        if ("true".equalsIgnoreCase(valueString)) {
            type = PropertyType.BOOLEAN;
            value = new Boolean(true);
        }
        if ("false".equalsIgnoreCase(valueString)) {
            type = PropertyType.BOOLEAN;
            value = new Boolean(false);
        }
        if (!valueString.trim().equals("")) {
            boolean isInteger = true;
            for (char num : valueString.toCharArray()) {
                if (!Character.isDigit(num)) {
                    isInteger = false;
                }
            }
            if (isInteger) {
                type = PropertyType.INTEGER;
                value = new Integer(valueString);
            }
        }
        if (type==null) {
            type = PropertyType.STRING;
            value = valueString;
        }
    }

    public String getPropertyId() {
        return propertyId;
    }
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }
    public PropertyType getType() {
        return type;
    }
    public void setType(PropertyType type) {
        this.type = type;
    }
    public Object getValue() {
        if (value==null) {
            this.parseObjectAndTypeFromString(stringValue);
        }
        return value;
    }
    public String getStringValue() {
        return stringValue;
    }

    public List<Action> getChangeActions() {
        return changeActions;
    }

    public void setChangeActions(List<Action> changeActions) {
        this.changeActions = changeActions;
    }

    public boolean isBlank() {
        boolean blank = false;
        if (value==null || stringValue.trim().equals("")) {
            blank = true;
        }
        return blank;
    }

    public List<BooleanDisplay> getBooleanDisplays() {
        return booleanDisplays;
    }

    public void setBooleanDisplays(List<BooleanDisplay> booleanDisplays) {
        this.booleanDisplays = booleanDisplays;
    }

    public List<RangeDisplay> getRangeDisplays() {
        return rangeDisplays;
    }

    public void setRangeDisplays(List<RangeDisplay> rangeDisplays) {
        this.rangeDisplays = rangeDisplays;
    }

    @Override
    public String toString() {
        return "Property [type=" + type + ", propertyId=" + propertyId + ", value=" + value + ", stringValue=" + stringValue + ", changeActions="
                + changeActions + ", booleanDisplays=" + booleanDisplays + ", rangeDisplays=" + rangeDisplays + "]";
    }

}
