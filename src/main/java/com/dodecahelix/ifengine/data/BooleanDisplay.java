/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

/**
 *   When displaying a boolean property, print out some other text instead of true/false.
 *
 */
public class BooleanDisplay implements Display {

    private static final long serialVersionUID = 1205302673406345916L;

    private String trueDisplay;
    private String falseDisplay;

    public BooleanDisplay(String trueDisplay, String falseDisplay) {
        this.trueDisplay = trueDisplay;
        this.falseDisplay = falseDisplay;
    }

    public String getTrueDisplay() {
        return trueDisplay;
    }
    public void setTrueDisplay(String trueDisplay) {
        this.trueDisplay = trueDisplay;
    }
    public String getFalseDisplay() {
        return falseDisplay;
    }
    public void setFalseDisplay(String falseDisplay) {
        this.falseDisplay = falseDisplay;
    }

    public String getDisplay(boolean boolValue) {
        if (boolValue) {
            return trueDisplay;
        } else {
            return falseDisplay;
        }
    }

}
