/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;

public class Exit implements Serializable {

    private static final long serialVersionUID = 3841399395453849595L;

    private String exitName;
    private String toLocationId;

    /**
     *   Will determine if exit is available in the choice list (implemented in MoveCommandBuilder)
     */
    private ConditionSet validations = new ConditionSet();

    public Exit() {
    }

    public Exit(String exitName, String toLocationId) {
        this(exitName, toLocationId, null);
    }

    public Exit(String exitName, String toLocationId, String key) {
        this.exitName = exitName;
        this.toLocationId = toLocationId;
    }

    public String getExitName() {
        return exitName;
    }

    public void setExitName(String exitName) {
        this.exitName = exitName;
    }

    public String getToLocationId() {
        return toLocationId;
    }

    public void setToLocationId(String toLocationId) {
        this.toLocationId = toLocationId;
    }

    public ConditionSet getValidations() {
        return validations;
    }

    public void setValidations(ConditionSet validations) {
        this.validations = validations;
    }

    @Override
    public String toString() {
        return "Exit [exitName=" + exitName + ", toLocationId=" + toLocationId + ", validations=" + validations + "]";
    }

}
