/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.validation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.util.InvalidEnvironmentDataException;

/**
 *   Traverse environment and check objects for invalid entries
 *
 */
public class EnvironmentValidator {

    private final static Logger LOGGER = LoggerFactory.getLogger(EnvironmentValidator.class);

    public static void validate(Environment environment) throws InvalidEnvironmentDataException {

        LOGGER.info("Validating environment : {}", environment);

        EnvironmentValidationTraversal traversal = new EnvironmentValidationTraversal();
        traversal.traverseEnvironment(environment);

        List<String> validationMessages = traversal.getValidationMessages();
        if (!validationMessages.isEmpty()) {
            StringBuffer invalidEnvironmentMessage = new StringBuffer("Environment is invalid.  Reasons:");
            for (String message : validationMessages) {
                invalidEnvironmentMessage.append(" ");
                invalidEnvironmentMessage.append(message);
                invalidEnvironmentMessage.append(";");
            }
            throw new InvalidEnvironmentDataException(invalidEnvironmentMessage.toString());
        }

    }

}
