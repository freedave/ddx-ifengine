/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.validation;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.dao.traversal.DefaultEnvironmentTraversal;
import com.dodecahelix.ifengine.data.Environment;

public class EnvironmentValidationTraversal extends DefaultEnvironmentTraversal {

    private List<String> validationMessages = new ArrayList<String>();

    @Override
    public void processEnvironment(Environment environment) {
        if (environment.getStoryContent().getIntroduction()==null) {
            validationMessages.add("Environment is missing an introduction.");
        }

        if (environment.getCurrentNarrator()==null) {
            validationMessages.add("There is no CurrentNarrator defined for the environment.");
        }
    }

    public List<String> getValidationMessages() {
        return validationMessages;
    }

}
