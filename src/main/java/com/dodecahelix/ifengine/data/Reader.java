/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *  The reader is the current narrator of the story.
 *
 */
public class Reader extends Entity {

    private static final long serialVersionUID = 7331300780923906054L;

    private String currentLocation;

    private static final String DEFAULT_READER_ID = "me";
    private static final int DEFAULT_CARRY_SIZE = 5;

    private List<String> knownTopics = new ArrayList<String>();

    public Reader() {
        this.setId(DEFAULT_READER_ID);
        this.setCarrySize(DEFAULT_CARRY_SIZE);
    }

    public Reader(String id) {
        this.setId(id);
        this.setCarrySize(DEFAULT_CARRY_SIZE);
    }

    @Override
    public EntityType getEntityType() {
        return EntityType.READER;
    }

    private Property findPropertyById(String propId, Set<Property> props) {
        Property foundProp = null;
        if (propId!=null) {
            for (Property property : props) {
                if (propId.equalsIgnoreCase(property.getPropertyId())) {
                    foundProp = property;
                    break;
                }
            }
        }
        return foundProp;
    }

    public void setProperty(String propertyId, String propertyValue, Set<Property> props) {
        // if property already exists, just update the value
        Property prop = this.findPropertyById(propertyId, props);
        if (prop!=null) {
            // an existing prop, parse it
            prop.parseObjectAndTypeFromString(propertyValue);
        } else {
            props.add(new Property(propertyId, propertyValue));
        }
    }

    public List<String> getKnownTopics() {
        return knownTopics;
    }

    public void setKnownTopics(List<String> knownTopics) {
        this.knownTopics = knownTopics;
    }

    public void addTopic(String topic) {
        this.knownTopics.add(topic);
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    @Override
    public String toString() {
        return "Reader [currentLocation=" + currentLocation + ", knownTopics=" + knownTopics + ", entity=" + super.toString() + "]";
    }


}
