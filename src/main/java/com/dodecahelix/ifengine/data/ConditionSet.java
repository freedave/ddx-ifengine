/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class ConditionSet implements Serializable {

    private static final long serialVersionUID = 568803480964903465L;


    /**
     *   Whether the conditions should be linked by AND or OR
     *   <br/>defaults to AND
     *
     */
    private boolean andSet = true;


    /**
     *   Handle used for sharing this set
     */
    private String referenceId;

    private Set<ConditionSet> subsets = new HashSet<ConditionSet>();
    private Set<String> referenceConditions = new HashSet<String>();
    private Set<Condition> conditions = new HashSet<Condition>();

    /**
     *
     * @return TRUE for AND set, FALSE for OR set
     */
    public boolean isAndSet() {
        return andSet;
    }

    public void setAndSet(boolean andSet) {
        this.andSet = andSet;
    }

    public Set<ConditionSet> getSubsets() {
        return subsets;
    }

    public void setSubsets(Set<ConditionSet> subsets) {
        this.subsets = subsets;
    }

    public Set<String> getReferenceConditions() {
        return referenceConditions;
    }

    public void setReferenceConditions(Set<String> referenceConditions) {
        this.referenceConditions = referenceConditions;
    }

    public Set<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(Set<Condition> conditions) {
        this.conditions = conditions;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    /**
     *   Get all conditions including subset conditions (but not reference conditions)
     * @return
     */
    public Set<Condition> getNestedConditions() {
        if (subsets.isEmpty()) {
            return conditions;
        } else {
            Set<Condition> allConditions = new HashSet<Condition>();
            for (ConditionSet subset : subsets) {
                allConditions.addAll(subset.getNestedConditions());
            }
            allConditions.addAll(conditions);

            return allConditions;
        }
    }

    @Override
    public String toString() {
        return "ConditionSet [andSet=" + andSet + ", referenceId=" + referenceId + ", subsets=" + subsets + ", referenceConditions=" + referenceConditions
                + ", conditions=" + conditions + "]";
    }

}
