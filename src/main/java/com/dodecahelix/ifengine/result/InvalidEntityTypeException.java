/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result;

import com.dodecahelix.ifengine.data.EntityType;

public class InvalidEntityTypeException extends ExecutionException {

    private static final long serialVersionUID = -1995124412559608970L;

    public InvalidEntityTypeException(EntityType expected, EntityType actual) {
        super(getFormattedExceptionMessage(expected, actual, false));
    }

    public InvalidEntityTypeException(EntityType expected, EntityType actual, boolean isTargetNotSubject) {
        super(getFormattedExceptionMessage(expected, actual, isTargetNotSubject));
    }

    private static String getFormattedExceptionMessage(EntityType expected, EntityType actual, boolean isTargetNotSubject) {
        String formatter = "invalid %s entity type. expected: %s; actual: %s";
        String subject = "subject";
        if (isTargetNotSubject) {
            subject = "target";
        }
        String message = String.format(formatter, subject, expected.name(), actual.name());
        return message;
    }

}
