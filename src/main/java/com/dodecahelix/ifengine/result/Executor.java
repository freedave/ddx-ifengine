/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Execution;

public interface Executor {

    /**
     *   Execute a data model change, and return a response
     *   <br/>Typically does not return a message (i.e; returns null or empty)
     *
     * @param execution
     * @param subject - Entity subject (required)
     * @param target - Entity target (optional)
     * @return message to print to console, or null if none
     * @throws ExecutionException
     */
    public ExecutionResults execute(Execution execution, Entity subject, Entity target) throws ExecutionException;

    public void setEnvironmentDAO(EnvironmentDAO dao);

}
