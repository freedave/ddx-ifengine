/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result;

import com.dodecahelix.ifengine.result.execute.AddTopicExecutor;
import com.dodecahelix.ifengine.result.execute.ChangeItemOwnershipExecutor;
import com.dodecahelix.ifengine.result.execute.EnableDialogExecutor;
import com.dodecahelix.ifengine.result.execute.EnableScheduleExecutor;
import com.dodecahelix.ifengine.result.execute.FollowExecutor;
import com.dodecahelix.ifengine.result.execute.FreezeTimeExecutor;
import com.dodecahelix.ifengine.result.execute.MoveEntityExecutor;
import com.dodecahelix.ifengine.result.execute.OverrideScheduleExecutor;
import com.dodecahelix.ifengine.result.execute.SetDarknessExecutor;
import com.dodecahelix.ifengine.result.execute.SetLightSourceExecutor;
import com.dodecahelix.ifengine.result.execute.StoryCompleteExecutor;
import com.dodecahelix.ifengine.result.execute.SwitchNarratorExecutor;
import com.dodecahelix.ifengine.result.execute.TimeIncrementExecutor;
import com.dodecahelix.ifengine.result.execute.UpdatePropertyExecutor;
import com.dodecahelix.ifengine.result.execute.WearExecutor;


/**
 *   Returns the class that handles the execution type
 *
 */
public class ExecutionServiceLocator {

    private UpdatePropertyExecutor updatePropertyExecutor = new UpdatePropertyExecutor();
    private ChangeItemOwnershipExecutor takeItemExecutor = new ChangeItemOwnershipExecutor();
    private MoveEntityExecutor moveEntityExecutor = new MoveEntityExecutor();
    private TimeIncrementExecutor timeIncrementExecutor = new TimeIncrementExecutor();
    private AddTopicExecutor addTopicExecutor = new AddTopicExecutor();
    private WearExecutor wearExecutor = new WearExecutor();
    private FreezeTimeExecutor freezeTimeExecutor = new FreezeTimeExecutor();
    private SwitchNarratorExecutor switchNarratorExecutor = new SwitchNarratorExecutor();
    private StoryCompleteExecutor storyCompleteExecutor = new StoryCompleteExecutor();
    private EnableScheduleExecutor enableScheduleExecutor = new EnableScheduleExecutor();
    private OverrideScheduleExecutor overrideScheduleExecutor = new OverrideScheduleExecutor();
    private SetDarknessExecutor setDarknessExecutor = new SetDarknessExecutor();
    private SetLightSourceExecutor setLightSourceExecutor = new SetLightSourceExecutor();
    private FollowExecutor followExecutor = new FollowExecutor();
    private EnableDialogExecutor enableDialogExecutor = new EnableDialogExecutor();

    public Executor getExecutorForType(ExecutionType type) {
        Executor executor = null;
        switch (type) {
            case UPDATE_PROPERTY : executor = updatePropertyExecutor; break;
            case CHOWN_ITEM : executor = takeItemExecutor; break;
            case MOVE_ENTITY : executor = moveEntityExecutor; break;
            case INC_TIME_PERIOD : executor = timeIncrementExecutor; break;
            case INC_TIME_INTERVAL : executor = timeIncrementExecutor; break;
            case ADD_TOPIC : executor = addTopicExecutor; break;
            case WEAR_ITEM : executor = wearExecutor; break;
            case FREEZE_TIME : executor = freezeTimeExecutor; break;
            case SWITCH_NARRATOR : executor = switchNarratorExecutor; break;
            case STORY_COMPLETE : executor = storyCompleteExecutor; break;
            case OVERRIDE_SCHEDULE : executor = overrideScheduleExecutor; break;
            case ENABLE_SCHEDULE : executor = enableScheduleExecutor; break;
            case SET_DARKNESS : executor = setDarknessExecutor; break;
            case SET_LIGHT_SOURCE : executor = setLightSourceExecutor; break;
            case FOLLOW : executor = followExecutor; break;
            case ENABLE_DIALOG : executor = enableDialogExecutor; break;

            default : throw new UnsupportedOperationException("Execution type " + type + " not supported.");
        }

        return executor;
    }

}
