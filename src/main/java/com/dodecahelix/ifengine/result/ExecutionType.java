/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result;

public enum ExecutionType {

    // property executions
    UPDATE_PROPERTY ("update.prop", "Changes a property (of subject) to a specified value (target)"),
    //INCREMENT_STAT ("increment.stat", "Increment (or decrement) a stat property (subject) by some numeric value (target)"),

    // item executions
    CHOWN_ITEM ("change.item.owner", "Item (subject) will be moved to the contents of some entity (target)"),
    //USE_ITEM ("use", "The item (subject) is used (decrements the uses count) by some numeric value (target).  If less than zero, the item will be consumed."),
    ADD_TOPIC ("add.topic", "Adds some dialog topic (target static value) to the reader (subject) topic list"),
    WEAR_ITEM ("wear", "The item (subject) will be worn by the narrator or some person (target entity).  Select RESET to remove item."),
    //REVEAL_ITEM ("reveal", "This will reveal a hidden item (if any) on the searchable mass (subject)"),

    // Person executions
    OVERRIDE_SCHEDULE ("override.schedule", "Override some person's (subject) schedule (target static value is referenceId).  RESET will remove any override."),
    ENABLE_SCHEDULE ("enable.schedule", "The schedule (target static value is the referenceId) of the person (subject) will be enabled (or disabled if RESET)."),
    ENABLE_DIALOG ("enable.dialog", "The dialog (target static value is the referenceId) of the person (subject) will be enabled (or disabled if RESET)."),

    // movement executions
    MOVE_ENTITY ("move", "Moves the subject entity (reader or mass) to the target location."),

    // story executions
    SWITCH_NARRATOR ("switch.narrator", "Switches the reader point of view to the specified narrator (subject)"),
    STORY_COMPLETE ("story.complete", "Flags the story as complete.  No more turns will be processed.  Enter a conclusion ID for the target static value (or none will be printed)."),

    // time executions
    INC_TIME_PERIOD ("inc.time.period", "Bump up the time period (i.e; afternoon to evening).  Target static value is the number of periods."),
    INC_TIME_INTERVAL ("inc.time.interval", "Bump up the time in increments.  Target static value is the number of increments."),
    FREEZE_TIME ("freeze.time", "Freeze time, so that temporal commands cannot be executed.  Select Operator to 'SET' to freeze, and 'RESET' to unfreeze."),

    SET_DARKNESS ("set.darkness", "Set the subject location to be dark."),
    SET_LIGHT_SOURCE ("set.light.source", "Set the subject item to be a light source."),

    FOLLOW ("set.follow", "The subject person is set to follow the reader"),
    ;

    private String text;
    private String description;

    ExecutionType(String text, String description) {
        this.text = text;
        this.description = description;
    }

    public String getText() {
        return text;
    }

    public String getDescription() {
        return description;
    }

    public static ExecutionType forText(String text) {
        ExecutionType foundType = null;
        for (ExecutionType type : ExecutionType.values()) {
            if (type.getText().equals(text)) {
                foundType = type;
            }
        }
        return foundType;
    }

}
