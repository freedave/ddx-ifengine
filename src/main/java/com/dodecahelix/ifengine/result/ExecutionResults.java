/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.data.Action;

public class ExecutionResults {

    private List<String> messages = new ArrayList<String>();
    private List<Action> followupActions = new ArrayList<Action>();

    public List<String> getMessages() {
        return messages;
    }
    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
    public List<Action> getFollowupActions() {
        return followupActions;
    }
    public void setFollowupActions(List<Action> followupActions) {
        this.followupActions = followupActions;
    }

    /**
     * Append another result to this result
     * @param result
     */
    public void append(ExecutionResults result) {
        followupActions.addAll(result.getFollowupActions());
        messages.addAll(result.getMessages());
    }

}
