/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Time;
import com.dodecahelix.ifengine.result.ExecutionException;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.Executor;
import com.dodecahelix.ifengine.time.TimeHandler;

/**
 *   increments the current time by the number (target static value) of periods or intervals (depending on the ExecutionType)
 *   if the target static value is not specified it will increment by one
 *
 */
public class TimeIncrementExecutor implements Executor {

    private EnvironmentDAO dao;

    private TimeHandler timeHandler;

    @Override
    public ExecutionResults execute(Execution execution, Entity subject, Entity target) throws ExecutionException {
        int incrementNum = 1;

        if (execution.getTargetStaticValue()!=null) {
            incrementNum = Integer.parseInt(execution.getTargetStaticValue());
        }

        if (timeHandler==null) {
            timeHandler = new TimeHandler(dao);
        }
        Time currentTime = dao.getEnvironment().getCurrentTime();

        switch (execution.getExecutionType()) {
            case INC_TIME_PERIOD : timeHandler.incrementPeriod(currentTime, incrementNum); break;
            case INC_TIME_INTERVAL : timeHandler.increment(currentTime, incrementNum); break;
            default : // should never reach
        }

        return new ExecutionResults();
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
        this.timeHandler = new TimeHandler(dao);
    }

}
