/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.result.ExecutionException;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.Executor;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;

public class SetDarknessExecutor implements Executor {

    @SuppressWarnings("unused")
    private EnvironmentDAO dao;

    @Override
    public ExecutionResults execute(Execution execution, Entity subject, Entity target) throws ExecutionException {

        if (!(subject instanceof Location)) {
            throw new ExecutionException("subject for SetDarknessExecutor should be a Location not a " + subject.getEntityType());
        }

        boolean set = true;
        if (ExecutionOperator.RESET.equals(execution.getOperator())) {
            set = false;
        }
        ((Location)subject).setDark(set);

        return new ExecutionResults();
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
