/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.result.ExecutionException;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.Executor;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   updates the property value of the subject entity to the target value (which can be either a static value or another entity property)
 *
 */
public class UpdatePropertyExecutor implements Executor {

    @SuppressWarnings("unused")
    private EnvironmentDAO dao;

    @Override
    public ExecutionResults execute(Execution execution, Entity subject, Entity target) throws ExecutionException {
        ExecutionResults result = new ExecutionResults();

        String subjectPropId = execution.getEntityHolder().getSubjectProperty();
        Property subjectProperty = subject.getPropertyById(subjectPropId);
        if (subjectProperty==null) {
            throw new ExecutionException("subject property for UpdatePropertyExecutor does not exist : " + subjectPropId);
        }

        String targetValue = null;

        switch (execution.getTargetType()) {
            case STATIC : targetValue = execution.getTargetStaticValue(); break;
            case ENTITY : targetValue = resolveTargetEntityValue(execution, target); break;
            default : // do nothing
        }

        switch (execution.getOperator()) {
            case SET : executeSetOperation(subjectProperty, targetValue); break;
            case INCREMENT : executionIncrementOperation(subjectProperty, targetValue); break;
            case DECREMENT : executionDecrementOperation(subjectProperty, targetValue); break;
            default : throw new ExecutionException("unable to execute UpdateProperty using operator " + execution.getOperator());
        }

        // process change actions
        result.getFollowupActions().addAll(subjectProperty.getChangeActions());
        return result;
    }

    private void executionDecrementOperation(Property subjectValue, String targetValue) {
        int targetInt = 1;
        if (!StringUtils.isEmpty(targetValue)) {
            targetInt = Integer.parseInt(targetValue);
        }
        Integer subjectInt = (Integer)subjectValue.getValue();
        subjectInt -= targetInt;
        subjectValue.parseObjectAndTypeFromString(subjectInt.toString());
    }

    private void executionIncrementOperation(Property subjectValue, String targetValue) {
        int targetInt = 1;
        if (!StringUtils.isEmpty(targetValue)) {
            targetInt = Integer.parseInt(targetValue);
        }
        Integer subjectInt = (Integer)subjectValue.getValue();
        subjectInt += targetInt;
        subjectValue.parseObjectAndTypeFromString(subjectInt.toString());
    }

    private String resolveTargetEntityValue(Execution execution, Entity target) throws ExecutionException {
        if (target==null) {
            throw new ExecutionException("unable to find target entity for UpdatePropertyExecutor");
        }

        String targetPropId = execution.getEntityHolder().getTargetProperty();
        Property property = target.getPropertyById(targetPropId);
        return property.getStringValue();
    }

    private void executeSetOperation(Property originalValue, String targetValue) {
        originalValue.parseObjectAndTypeFromString(targetValue);
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
