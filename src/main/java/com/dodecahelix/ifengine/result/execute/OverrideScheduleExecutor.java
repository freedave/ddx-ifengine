/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.result.ExecutionException;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.Executor;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   TODO - is this necessary with enable / disable?
 *
 */
public class OverrideScheduleExecutor implements Executor {

    @SuppressWarnings("unused")
    private EnvironmentDAO dao;

    @Override
    public ExecutionResults execute(Execution execution, Entity subject, Entity target) throws ExecutionException {

        if (!EntityType.PERSON.equals(subject.getEntityType())) {
            throw new ExecutionException("subject type of OverrideScheduleExecutor should be type Person, not " + subject.getEntityType());
        }
        Person person = (Person)subject;

        boolean enable = ExecutionOperator.SET.equals(execution.getOperator());
        if (enable) {
            String referenceSchedule = execution.getTargetStaticValue();
            if (StringUtils.isEmpty(referenceSchedule)) {
                throw new ExecutionException("no target static value (reference schedule) defined for OverriedScheduleExecutor");
            }
            person.setOverrideSchedule(referenceSchedule);
        } else {
            // disable the override
            person.setOverrideSchedule(null);
        }

        return new ExecutionResults();
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
