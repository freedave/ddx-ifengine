/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.result.ExecutionException;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.Executor;

/**
 *   adds the subject item to the target entity contents (and removes from previous owner).  
 *   if target is not specified, the reader is assumed.
 *   
 *
 */
public class ChangeItemOwnershipExecutor implements Executor {

    private EnvironmentDAO dao;

    @Override
    public ExecutionResults execute(Execution execution, Entity subject, Entity target) throws ExecutionException {
        EntityHolder holder = execution.getEntityHolder();
        if (!EntityType.ITEM.equals(holder.getSubjectType())) {
            throw new ExecutionException("subject type of ChangeItemOwnershipExecutor should be type Item, not " + holder.getSubjectType());
        }

        // Find the old owner of this item -- assume unique
        Entity originalOwner = getItemOwner((Item)subject);

        // move item to a new entity (content)
        if (target==null) {
            target = dao.getReader();
        }
        target.addContentItem(subject.getId());

        // remove item from old owner
        if (originalOwner!=null) {
            originalOwner.getContents().remove(subject.getId());
        }

        return new ExecutionResults();
    }

    private Entity getItemOwner(Item item) {
        Entity owner = null;
        String itemId = item.getId();

        // Find the holder by parsing through all containers
        owner = dao.getEnvironment();
        if (doesEntityOwn(owner, itemId)) {
            return owner;
        }

        owner = dao.getReader();
        if (doesEntityOwn(owner, itemId)) {
            return owner;
        }

        for (Reader narrator : dao.getEnvironment().getNarrators()) {
            if (doesEntityOwn(narrator, itemId)) {
                return narrator;
            }
        }

        for (Person person : dao.getEnvironment().getPeople()) {
            if (doesEntityOwn(person, itemId)) {
                return person;
            }
        }

        for (Entity location : dao.getEnvironment().getLocations()) {
            if (doesEntityOwn(location, itemId)) {
                return location;
            }
        }

        for (Entity mass : dao.getEnvironment().getMasses()) {
            if (doesEntityOwn(mass, itemId)) {
                return mass;
            }
        }

        return null;
    }

    private boolean doesEntityOwn(Entity entity, String itemId) {
        for (String contentItem : entity.getContents()) {
            if (contentItem.equalsIgnoreCase(itemId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
