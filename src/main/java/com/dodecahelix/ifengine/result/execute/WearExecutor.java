/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.result.ExecutionException;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.Executor;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;

/**
 *   target entity (optional, narrator by default) to wear the subject Item
 *
 */
public class WearExecutor implements Executor {

    private EnvironmentDAO dao;

    @Override
    public ExecutionResults execute(Execution execution, Entity subject, Entity target) throws ExecutionException {

        if (!EntityType.ITEM.equals(subject.getEntityType())) {
            throw new ExecutionException("can only wear/remove Items, not " + subject.getEntityType());
        }

        boolean wear = true;
        if (ExecutionOperator.RESET == execution.getOperator()) {
            wear = false;
        }

        // by default, the reader is the target
        if (target==null) {
             target = dao.getReader();
        }

        if (target.getContents().contains(subject.getId())) {
            // TODO - check to see if another item is worn on this body part?  this should be a validation, but check just in case
            ((Item)subject).setWorn(wear);
        } else {
            throw new ExecutionException("cannot wear/remove something that is not carried by target " + target.getId());
        }

        return new ExecutionResults();
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
