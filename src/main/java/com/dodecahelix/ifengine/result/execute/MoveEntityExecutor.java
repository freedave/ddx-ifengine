/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.result.ExecutionException;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.Executor;

/**
 *   relocate subject (either a reader, or a mass) to target location (target entity)
 *
 */
public class MoveEntityExecutor implements Executor {

    private EnvironmentDAO dao;

    @Override
    public ExecutionResults execute(Execution execution, Entity subject, Entity target) throws ExecutionException {

        // Move Subject Entity to Target Entity (location)
        Location location = (Location)target;
        if (location==null) {
            throw new ExecutionException("no target location specified for MoveEntityExecutor");
        }

        switch (subject.getEntityType()) {
            case READER : moveReader(location); break;
            case NARRATOR : moveReader(location); break;
            case MASS : moveMass(subject, location); break;
            default : throw new ExecutionException("subject entity type not supported for MoveEntityExecutor : " + subject.getEntityType());
        }

        return new ExecutionResults();
    }

    private void moveMass(Entity mass, Location location) {
        for (Location searchLocation : dao.getEnvironment().getLocations()) {
            if (location.getId().equalsIgnoreCase(searchLocation.getId())) {
                searchLocation.addMass(mass.getId());
            } else {
                // remove from old location
                if (searchLocation.getMasses()!=null && searchLocation.getMasses().contains(mass.getId())) {
                    searchLocation.getMasses().remove(mass.getId());
                }
            }
        }
    }

    private void moveReader(Location location) {
        dao.getReader().setCurrentLocation(location.getId());
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
