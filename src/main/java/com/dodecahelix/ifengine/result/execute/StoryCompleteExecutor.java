/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.story.Conclusion;
import com.dodecahelix.ifengine.result.ExecutionException;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.Executor;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   ends the story
 *
 */
public class StoryCompleteExecutor implements Executor {

    private EnvironmentDAO dao;

    @Override
    public ExecutionResults execute(Execution execution, Entity subject, Entity target) throws ExecutionException {

        dao.getEnvironment().setStoryComplete(true);

        ExecutionResults result = new ExecutionResults();

        // if there is a conclusion defined for the target static value, print out the message
        String conclusionId = execution.getTargetStaticValue();
        for (Conclusion conclusion : dao.getEnvironment().getStoryContent().getConclusions()) {
            if (StringUtils.equalsIgnoreCase(conclusion.getConclusionId(), conclusionId)) {
                result.getMessages().add(conclusion.getConclusionText());
            }
        }

        return result;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }
}
