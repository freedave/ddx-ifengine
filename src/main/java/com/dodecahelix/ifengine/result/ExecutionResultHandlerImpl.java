/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.entity.ResolutionUtils;

public class ExecutionResultHandlerImpl implements ExecutionResultHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(ExecutionResultHandlerImpl.class);

    private ExecutionServiceLocator executionServiceLocator;
    private EnvironmentDAO dao;

    public ExecutionResultHandlerImpl(EnvironmentDAO dao, ExecutionServiceLocator executionServiceLocator) {
        this.dao = dao;
        this.executionServiceLocator = executionServiceLocator;
    }

    @Override
    public ExecutionResults execute(ExecutionSet executions, String subjectEntityId, String localTargetId) {

        ExecutionResults executionSetResult = new ExecutionResults();

        for (Execution execution : executions.getExecutions()) {
            ExecutionResults executionResult = new ExecutionResults();

            try {
                Executor executor = executionServiceLocator.getExecutorForType(execution.getExecutionType());
                executor.setEnvironmentDAO(dao);

                // resolve Subject (required)
                boolean skipExecution = false;
                EntityHolder holder = execution.getEntityHolder();
                String subjectId = ResolutionUtils.getSubjectEntityId(holder, subjectEntityId);
                Entity subject = dao.getEntityById(holder.getSubjectType(), subjectId);
                if (subject==null) {
                    // can't match a "THIS", so just resolve to false
                    if (EntityHolder.THIS_SUBJECT_ID.equalsIgnoreCase(holder.getSubjectEntityId())) {
                        skipExecution = true;
                    } else {
                        throw new ExecutionException("unable to find subject with id of " + subjectId);
                    }
                }

                // resolve Target (optional)
                Entity target = null;
                if (TargetType.ENTITY.equals(execution.getTargetType())) {
                    String targetId = ResolutionUtils.getTargetEntityId(holder, localTargetId);
                    target = dao.getEntityById(holder.getTargetType(), targetId);
                    if (target==null) {
                        // can't match a "LOCAL", just resolve to false
                        if (EntityHolder.LOCAL_TARGET_ID.equalsIgnoreCase(holder.getTargetEntityId())) {
                            skipExecution = true;
                        } else {
                            throw new ExecutionException("unable to find target with id of " + targetId);
                        }
                    }
                } else {
                    if (execution.getEntityHolder().getTargetType()!=null) {
                        throw new ExecutionException("malformed Execution.  TargetType is not Entity, but Target has a defined EntityType");
                    }
                }

                if (!skipExecution) {
                    if (LOGGER.isDebugEnabled()) {
                        String executionMessage = String.format("Execution for %s using parentEntityId %s and localTargetId %s", execution.getExecutionType(), subjectEntityId, localTargetId);
                        LOGGER.debug(executionMessage);
                    }

                    executionResult = executor.execute(execution, subject, target);
                }

                executionSetResult.append(executionResult);
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        for (String referenceSet : executions.getReferenceExecutions()) {
            ExecutionSet referenceExecutions = dao.findReferenceExecutionById(referenceSet);
            ExecutionResults referenceExecutionsResult = execute(referenceExecutions, subjectEntityId, localTargetId);
            executionSetResult.append(referenceExecutionsResult);
        }

        return executionSetResult;
    }

}
