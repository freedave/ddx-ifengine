/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition;

public enum ConditionOperator {

    EQUALS ("="),
    NOT_EQUALS ("!="),
    LESS_THAN_EQUALS ("<="),
    LESS_THAN ("<"),
    GREATER_THAN_EQUALS (">="),
    GREATER_THAN (">"),
    TRUE("true"),
    FALSE("false");

    private String abbreviation;

    ConditionOperator(String abbr) {
        this.abbreviation = abbr;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public static ConditionOperator getOperatorForAbbreviation(String abbr) {
        ConditionOperator found = null;
        for (ConditionOperator operator : ConditionOperator.values()) {
            if (operator.getAbbreviation().equalsIgnoreCase(abbr)) {
                found = operator;
            }
        }
        return found;
    }
}
