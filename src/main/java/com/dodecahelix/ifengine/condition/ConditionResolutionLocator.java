/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition;

import com.dodecahelix.ifengine.condition.resolve.FlagResolver;
import com.dodecahelix.ifengine.condition.resolve.FrozenTimeResolver;
import com.dodecahelix.ifengine.condition.resolve.HoldingLightSourceResolver;
import com.dodecahelix.ifengine.condition.resolve.InLocationResolver;
import com.dodecahelix.ifengine.condition.resolve.IsDarkResolver;
import com.dodecahelix.ifengine.condition.resolve.IsLightSourceResolver;
import com.dodecahelix.ifengine.condition.resolve.IsWearableResolver;
import com.dodecahelix.ifengine.condition.resolve.IsWearingResolver;
import com.dodecahelix.ifengine.condition.resolve.KnowsTopicResolver;
import com.dodecahelix.ifengine.condition.resolve.NumericComparisonResolver;
import com.dodecahelix.ifengine.condition.resolve.OwnershipResolver;
import com.dodecahelix.ifengine.condition.resolve.PercentageChancePropertyResolver;
import com.dodecahelix.ifengine.condition.resolve.PercentageChanceResolver;
import com.dodecahelix.ifengine.condition.resolve.StringComparisonResolver;
import com.dodecahelix.ifengine.condition.resolve.TimeResolver;
import com.dodecahelix.ifengine.condition.resolve.TooBigResolver;
import com.dodecahelix.ifengine.condition.resolve.VisibilityResolver;

public class ConditionResolutionLocator {

    private FlagResolver flagResolver = new FlagResolver();
    private StringComparisonResolver stringCompareResolver = new StringComparisonResolver();
    private NumericComparisonResolver numericCompareResolver = new NumericComparisonResolver();
    private VisibilityResolver visibilityResolver = new VisibilityResolver();
    private OwnershipResolver ownershipResolver = new OwnershipResolver();
    private TimeResolver timeResolver = new TimeResolver();
    private IsWearableResolver isWearableResolver = new IsWearableResolver();
    private IsWearingResolver isWearingResolver = new IsWearingResolver();
    private TooBigResolver tooBigResolver = new TooBigResolver();
    private PercentageChancePropertyResolver attributeRollResolver = new PercentageChancePropertyResolver();
    private PercentageChanceResolver randomRollResolver = new PercentageChanceResolver();
    private FrozenTimeResolver frozenTimeResolver = new FrozenTimeResolver();
    private InLocationResolver inLocationResolver = new InLocationResolver();
    private KnowsTopicResolver knowsTopicResolver = new KnowsTopicResolver();
    private IsLightSourceResolver isLightSourceResolver = new IsLightSourceResolver();
    private IsDarkResolver isDarkResolver = new IsDarkResolver();
    private HoldingLightSourceResolver holdingLightResolver = new HoldingLightSourceResolver();

    public ConditionResolver getResolverForType(ConditionType type) {
        ConditionResolver resolver = null;
        switch (type) {
            case FLAG : resolver = flagResolver; break;
            case STRING_PROPERTY_COMPARE : resolver = stringCompareResolver; break;
            case NUMERIC_PROPERTY_COMPARE : resolver = numericCompareResolver; break;
            case VISIBILE : resolver = visibilityResolver; break;
            case OWNED: resolver = ownershipResolver; break;
            case BOOLEAN_PROPERTY_COMPARE: resolver = flagResolver; break;
            case TIME : resolver = timeResolver; timeResolver.setTimeTypeToResolve(true, false, false); break;
            case TIME_WEEKDAY : resolver = timeResolver; timeResolver.setTimeTypeToResolve(false, true, false); break;
            case TIME_MONTH : resolver = timeResolver; timeResolver.setTimeTypeToResolve(false, false, true); break;
            case IS_WEARABLE : resolver = isWearableResolver; break;
            case IS_WEARING : resolver = isWearingResolver; break;
            case TOO_BIG_TO_CARRY : resolver = tooBigResolver; break;
            case RANDOM_PERCENT: resolver = randomRollResolver; break;
            case RANDOM_PERCENT_OF_PROPERTY: resolver = attributeRollResolver; break;
            case FROZEN_TIME : resolver = frozenTimeResolver; break;
            case IN_LOCATION : resolver = inLocationResolver; break;
            case KNOWS_TOPIC : resolver = knowsTopicResolver; break;
            case IS_DARK : resolver = isDarkResolver; break;
            case IS_LIGHT_SOURCE : resolver = isLightSourceResolver; break;
            case HOLDING_LIGHT_SOURCE : resolver = holdingLightResolver; break;
        }

        return resolver;
    }

}
