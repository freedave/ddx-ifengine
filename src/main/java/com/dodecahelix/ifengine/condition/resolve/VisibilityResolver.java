/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   Tests to see if the subject entity is somewhere in the current location
 *   
 * @author dpeters
 *
 */
public class VisibilityResolver implements ConditionResolver {

    private EnvironmentDAO dao;

    @Override
    public boolean resolve(Condition condition, Entity subject, Entity target) throws ResolutionException {

        boolean resolution = false;

        Location currentLocation = dao.getCurrentLocation();

        boolean isInLocation = false;
        String subjectId = subject.getId();
        switch (subject.getEntityType()) {
            case PERSON : isInLocation = isPersonVisible(currentLocation, subjectId); break;
            case MASS : isInLocation = isMassVisible(currentLocation, subjectId); break;
            case ITEM : isInLocation = isItemVisible(currentLocation, subjectId); break;

            default: throw new ResolutionException("subject entity type " + subject.getEntityType() + " not supported for VisiblityResolver");
        }

        ConditionOperator operator = condition.getOperator();
        if (operator==null || operator.equals(ConditionOperator.EQUALS)) {
            resolution = isInLocation;
        }
        if (operator.equals(ConditionOperator.NOT_EQUALS)) {
            resolution = !isInLocation;
        }

        return resolution;
    }

    private boolean isItemVisible(Location currentLocation, String itemEntityId) {

        boolean inLocation = false;
        for (String itemId : currentLocation.getContents()) {
            if (itemId.equalsIgnoreCase(itemEntityId)) {
                inLocation = true;
            }
        }

        // also, contained in masses
        for (String massId : currentLocation.getMasses()) {
            Mass mass = (Mass) dao.getEntityById(EntityType.MASS, massId);
            for (String itemId : mass.getContents()) {
                if (itemId.equalsIgnoreCase(itemEntityId)) {
                    inLocation = true;
                }
            }
        }

        return inLocation;
    }

    private boolean isMassVisible(Location currentLocation, String entityId) {
        boolean inLocation = false;
        for (String massId : currentLocation.getMasses()) {
            if (massId.equalsIgnoreCase(entityId)) {
                inLocation = true;
            }
        }
        return inLocation;
    }

    private boolean isPersonVisible(Location currentLocation, String entityId) {
        boolean inLocation = false;

        for (Person person : dao.getPeopleForCurrentLocation()) {
            if (StringUtils.equalsIgnoreCase(entityId, person.getId())) {
                inLocation = true;

                // found them, not need to look
                break;
            }
        }

        return inLocation;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
