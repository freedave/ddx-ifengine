/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Item;

/**
 * 
 *   Test to see if the subject item is too big for the target entity to hold/carry (depending on carry size field)
 *
 */
public class TooBigResolver implements ConditionResolver {

    private EnvironmentDAO dao;

    @Override
    public boolean resolve(Condition condition, Entity subject, Entity target) throws ResolutionException {
        boolean resolution = false;

        // default to reader as the target (if not specified)
        if (target==null) {
            target = dao.getReader();
        }

        if (EntityType.ITEM != subject.getEntityType()) {
            throw new ResolutionException("subject entity for TooBigResolver must be of type Item, not " + subject.getEntityType());
        }

        // how much does the target currently carry
        int targetCarryAmount = target.getCarrySize();
        for (String itemId : target.getContents()) {
            Item carriedItem = (Item) dao.getEntityById(EntityType.ITEM, itemId);

            if (!carriedItem.isWorn()) {
                targetCarryAmount -= carriedItem.getSize();
            } else {
                // if the item is worn, and is a container, then add CarrySize to targetCarryAmount
                targetCarryAmount += carriedItem.getCarrySize();
            }
        }

        int subjectSize = ((Item)subject).getSize();
        if (subjectSize>targetCarryAmount) {
            resolution = true;
        }

        return resolution;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
