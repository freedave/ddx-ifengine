/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.PropertyType;
import com.dodecahelix.ifengine.util.Randomizer;

/**
 *   Resolves the logic operation by comparing the subject numeric property with target static value (or target numeric property)
 *
 */
public class NumericComparisonResolver implements ConditionResolver {

    @SuppressWarnings("unused")
    private EnvironmentDAO dao;

    @Override
    public boolean resolve(Condition condition, Entity subject, Entity target) throws ResolutionException {
        boolean resolution = false;


        String propertyId = condition.getEntityHolder().getSubjectProperty();
        Property property = subject.getPropertyById(propertyId);
        if (property==null) {
            throw new ResolutionException("subject " + subject.getTitle() + " does not have a property " + propertyId);
        }

        Integer subjectValue = null;
        if (!PropertyType.INTEGER.equals(property.getType())) {
            if (!property.isBlank()) {
                throw new ResolutionException("Unable to perform numeric resolution when subject property is not an integer : " + property.getStringValue());
            }
        } else {
            subjectValue = (Integer) property.getValue();
        }

        Integer targetValue = null;
        switch(condition.getTargetType()) {
            case STATIC : targetValue = resolveTargetStaticValue(condition); break;
            case ENTITY : targetValue = resolveEntityTargetValue(condition, target); break;
            case RANDOM : targetValue = resolveRandomTargetValue(condition); break;
        }

        ConditionOperator operator = condition.getOperator();
        if (targetValue!=null && subjectValue!=null) {
            switch (operator) {
                case EQUALS : resolution = (targetValue.intValue()==subjectValue.intValue()); break;
                case NOT_EQUALS : resolution = (targetValue.intValue()!=subjectValue.intValue()); break;
                case LESS_THAN_EQUALS : resolution = (subjectValue.intValue()<=targetValue.intValue()); break;
                case LESS_THAN : resolution = (subjectValue.intValue()<targetValue.intValue()); break;
                case GREATER_THAN_EQUALS : resolution = (subjectValue.intValue()>=targetValue.intValue()); break;
                case GREATER_THAN : resolution = (subjectValue.intValue()>targetValue.intValue()); break;

                default : throw new ResolutionException("Operator " + operator + " not supported for numeric comparison.");
            }
        }

        return resolution;
    }

    private Integer resolveRandomTargetValue(Condition condition) {

        int maxRandom = Integer.parseInt(condition.getTargetStaticValue());
        return Randomizer.getRandomInteger(maxRandom);
    }

    private Integer resolveTargetStaticValue(Condition condition) {
        String targetValue = condition.getTargetStaticValue();
        return Integer.parseInt(targetValue);
    }

    private Integer resolveEntityTargetValue(Condition condition, Entity target) throws ResolutionException {

        if (target==null) {
            throw new ResolutionException("target entity required for NumericComparison");
        }

        EntityHolder holder = condition.getEntityHolder();
        String propertyId = holder.getTargetProperty();
        Property property = target.getPropertyById(propertyId);

        Integer targetValue = null;
        if (!PropertyType.INTEGER.equals(property.getType())) {
            if (!property.isBlank()) {
                throw new ResolutionException("unable to perform numeric resolution when target property is not an integer : " + property.getStringValue());
            }
        } else {
            targetValue = (Integer) property.getValue();
        }

        return targetValue;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
