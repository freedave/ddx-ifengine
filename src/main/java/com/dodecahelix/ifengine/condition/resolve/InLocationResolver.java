/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   Resolves whether the subject entity is found in the target location
 *
 */
public class InLocationResolver implements ConditionResolver {

    private EnvironmentDAO dao;

    @Override
    public boolean resolve(Condition condition, Entity subject, Entity target) throws ResolutionException {

        boolean resolved = false;

        // if a target is not specified, then assume this is the current location
        if (target==null) {
            target = dao.getCurrentLocation();
        }
        if (EntityType.LOCATION != target.getEntityType()) {
            throw new ResolutionException("invalid target type (should be Location) : " + target.getEntityType());
        }

        String subjectId = subject.getId();
        String targetId = target.getId();

        EntityType subjectType = subject.getEntityType();
        switch (subjectType) {
            case ITEM : resolved = isItemInLocation(subjectId, (Location) target); break;
            case PERSON : resolved = isPersonInLocation(subjectId, targetId); break;
            case MASS : resolved = isMassInLocation(subjectId, (Location) target); break;
            case READER : resolved = isReaderInLocation((Reader) subject, targetId); break;
            default : throw new ResolutionException("invalid subject type for InLocationResolver : " + subjectType);
        }

        return resolved;
    }

    private boolean isPersonInLocation(String personId, String locationId) {
        boolean inLocation = false;

        for (Person person : dao.getPeopleForLocation(locationId)) {
            if (StringUtils.equalsIgnoreCase(personId, person.getId())) {
                inLocation = true;
            }
        }

        return inLocation;
    }

    private boolean isItemInLocation(String itemId, Location location) {
        boolean inLocation = false;

        // is it in the reader inventory?
        if (dao.getReader().getContents().contains(itemId)) {
            inLocation = true;
        }

        // on the ground?
        if (location.getContents().contains(itemId)) {
            inLocation = true;
        }

        // in a local mass?

        return inLocation;
    }

    private boolean isMassInLocation(String massId, Location location) {
        boolean inLocation = false;
        if (location.getMasses().contains(massId)) {
            inLocation = true;
        }
        return inLocation;
    }

    private boolean isReaderInLocation(Reader subject, String locationId) {
        boolean inLocation = false;
        if (StringUtils.equalsIgnoreCase(subject.getCurrentLocation(), locationId)) {
            inLocation = true;
        }
        return inLocation;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
