/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Item;

/**
 *   
 *  Tests whether the subject item is worn by the target entity
 *
 */
public class IsWearingResolver implements ConditionResolver {

    private EnvironmentDAO dao;

    @Override
    public boolean resolve(Condition condition, Entity subject, Entity target) throws ResolutionException {

        boolean worn = false;

        if (EntityType.ITEM != subject.getEntityType()) {
            throw new ResolutionException("invalid condition.  Only an item may be a subject in IsWearingResolver, not " + subject.getEntityType());
        }

        if (target==null) {
            target = dao.getReader();
        }

        if (target.getContents().contains(subject.getId())
                && ((Item)subject).isWorn()) {
            worn = true;
        }

        return worn;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
