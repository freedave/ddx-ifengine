/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Time;

/**
 *   Check to see if the current time is equal to the target static value (in its numeric form, i.e; 1 = morning, etc..)
 *
 */
public class TimeResolver implements ConditionResolver {

    private EnvironmentDAO dao;

    @SuppressWarnings("unused")
    private boolean periodResolve;
    private boolean dayResolve;
    private boolean monthResolve;

    public void setTimeTypeToResolve(boolean period, boolean day, boolean month) {
        this.periodResolve = period;
        this.dayResolve = day;
        this.monthResolve = month;
    }

    @Override
    public boolean resolve(Condition condition, Entity subject, Entity target) throws ResolutionException {
        boolean onTime = false;
        int timeToCheck = Integer.parseInt(condition.getTargetStaticValue());
        Time currentTime = dao.getEnvironment().getCurrentTime();

        // default to period checking
        int currentTimeInt = currentTime.getPeriod();
        if (dayResolve) {
            currentTimeInt = currentTime.getWeekday();
        }
        if (monthResolve) {
            currentTimeInt = currentTime.getMonth();
        }

        switch (condition.getOperator()) {
            case EQUALS : onTime = (currentTimeInt == timeToCheck); break;
            case GREATER_THAN : onTime = (currentTimeInt > timeToCheck); break;
            case GREATER_THAN_EQUALS : onTime = (currentTimeInt >= timeToCheck); break;
            case LESS_THAN : onTime = (currentTimeInt < timeToCheck); break;
            case LESS_THAN_EQUALS : onTime = (currentTimeInt <= timeToCheck); break;
            case NOT_EQUALS : onTime = (currentTimeInt != timeToCheck); break;
            default : onTime = (currentTimeInt == timeToCheck);
        }

        return onTime;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
