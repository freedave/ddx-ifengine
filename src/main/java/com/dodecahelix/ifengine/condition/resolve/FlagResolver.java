/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.PropertyType;

/**
 * 
 *   Flags are simple Boolean properties of some entity.
 *   
 *   Tests whether the subject's property (a boolean) is true or false.
 *   
 *   If the operator is not provided, assume this is a test that the property value is true
 * 
 * @author dpeters
 *
 */
public class FlagResolver implements ConditionResolver {

    @SuppressWarnings("unused")
    private EnvironmentDAO dao;

    @Override
    public boolean resolve(Condition condition, Entity subject, Entity target) throws ResolutionException {
        boolean resolution = false;

        String propertyId = condition.getEntityHolder().getSubjectProperty();
        Property property = subject.getPropertyById(propertyId);
        if (property==null) {
            throw new ResolutionException("no property " + propertyId + " on subject entity " + subject.getId());
        }

        if (PropertyType.BOOLEAN.equals(property.getType())) {
            resolution = (Boolean)property.getValue();
        } else {
            throw new ResolutionException("Unable to resolve FLAG type.  Property " + propertyId + " is not a Boolean type.");
        }

        return resolution;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
