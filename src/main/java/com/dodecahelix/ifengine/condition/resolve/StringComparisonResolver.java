/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 * 
 *  Resolves whether two text values are the same, comparing the subject property value with target static value (or target property)
 *
 */
public class StringComparisonResolver implements ConditionResolver {

    @SuppressWarnings("unused")
    private EnvironmentDAO dao;

    @Override
    public boolean resolve(Condition condition, Entity subject, Entity target) throws ResolutionException {

        boolean resolution = false;

        String propertyId = condition.getEntityHolder().getSubjectProperty();
        Property property = subject.getPropertyById(propertyId);
        if (property==null) {
            throw new ResolutionException("unable to find subject property " + propertyId);
        }

        String subjectValue = property.getStringValue();
        String targetValue = null;

        switch(condition.getTargetType()) {
            case STATIC : targetValue = condition.getTargetStaticValue(); break;
            case ENTITY : targetValue = resolveEntityTargetValue(condition, target); break;
            default : targetValue = null;
        }

        ConditionOperator operator = condition.getOperator();
        switch (operator) {
            case EQUALS : resolution = StringUtils.equalsIgnoreCase(targetValue, subjectValue); break;
            case TRUE : resolution = StringUtils.equalsIgnoreCase(targetValue, subjectValue); break;
            case NOT_EQUALS : resolution = !StringUtils.equalsIgnoreCase(targetValue, subjectValue); break;
            case FALSE : resolution = StringUtils.equalsIgnoreCase(targetValue, subjectValue); break;
            default : throw new IllegalArgumentException("String comparison must be EQUALS/TRUE or NOT_EQUALS/FALSE.  Not " + operator);
        }

        return resolution;
    }

    private String resolveEntityTargetValue(Condition condition, Entity target) throws ResolutionException {

        if (target==null) {
            throw new ResolutionException("target must be supplied for StringComparisionResolver");
        }

        String propertyId = condition.getEntityHolder().getTargetProperty();
        Property property = target.getPropertyById(propertyId);
        return property.getStringValue();
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
