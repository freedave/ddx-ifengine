/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Item;

/**
 *  Resolves whether the subject item is an article of clothing, is not already worn, and the target entity (optional, defaults to the narrator) is wearing nothing else on that body part. 
 *  
 */
public class IsWearableResolver implements ConditionResolver {

    private EnvironmentDAO dao;

    @Override
    public boolean resolve(Condition condition, Entity subject, Entity target) throws ResolutionException {

        boolean resolution = false;

        if (!EntityType.ITEM.equals(subject.getEntityType())) {
            throw new ResolutionException("only an item can be wearable, not a " + subject.getEntityType());
        }

        Item clothing = (Item)subject;
        if (clothing.isWearable() && !clothing.isWorn()) {
            String toWearPart = clothing.getWearPart();

            if (target==null) {
                target = dao.getReader();
            }

            // assume wearable, unless something is already worn on that part
            resolution = true;
            for (String contentId : target.getContents()) {
                Item contentItem = (Item) dao.getEntityById(EntityType.ITEM, contentId);
                if (contentItem.isWorn()) {
                    String wearPart = contentItem.getWearPart();
                    if (wearPart.equalsIgnoreCase(toWearPart)) {
                        resolution = false;
                    }
                }
            }
        }

        return resolution;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
