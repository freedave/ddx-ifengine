/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.util.Randomizer;

/** 
 *   Use a 1-100 integer property (of the subject) as a percent chance of resolving to true.
 *     
 *   <p>This is a linear resolution.  (i.e; 90 = 90% chance, 40 = 40% chance)
 *
 */
public class PercentageChancePropertyResolver implements ConditionResolver {

    @SuppressWarnings("unused")
    private EnvironmentDAO dao;

    @Override
    public boolean resolve(Condition condition, Entity subject, Entity target) throws ResolutionException {
        boolean resolution = false;

        String propertyId = condition.getEntityHolder().getSubjectProperty();
        String attrValue = subject.getPropertyById(propertyId).getStringValue();
        int percent = Integer.parseInt(attrValue);

        int randomRoll = Randomizer.getRandomInteger(100);
        if (percent>=randomRoll) {
            resolution = true;
        }

        // flip the chances
        if (condition.getOperator().equals(ConditionOperator.NOT_EQUALS)) {
            resolution = !resolution;
        }

        return resolution;
    }

    @Override
    public void setEnvironmentDAO(EnvironmentDAO dao) {
        this.dao = dao;
    }

}
