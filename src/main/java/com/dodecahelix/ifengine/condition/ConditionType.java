/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition;

public enum ConditionType {

    // check to see if some boolean property is true
    FLAG ("flag", "A flag is simply a boolean (true or false) value.  Tests the subject property (a boolean) as being either either true or false."),

    // compare two string properties by an operator
    STRING_PROPERTY_COMPARE ("string.compare", "Compares the text of the subject property to the target (either a static value or another entity property)."),

    // compare two numeric properties by an operator
    NUMERIC_PROPERTY_COMPARE ("numeric.compare", "Compares the numeric value of the subject property to the target (either a static value or another entity property)."),

    // compare two boolean properties
    BOOLEAN_PROPERTY_COMPARE ("boolean.compare", "Compares the boolean value of the subject property to the target (either a static value or another entity property)."),

    // check to see if an item is carried/owned by the target
    OWNED ("is.owned", "Checks whether the subject is a content item of the target entity."),

    // check to see that the reader is in a certain location (current location)
    IN_LOCATION ("in.location", "Checks to see if the subject entity is located in the specified target location.  For current location, leave the target blank."),

    // check to see if an entity (person, item, mass) is visible to reader (i.e; inide an open container in the current location)
    VISIBILE ("is.visible", "Checks to see if the subject entity is 'visible' to the reader (i.e; in the current location, or inside of an open container)"),

    // match a certain time period (i.e; 0=morning, 1=afternoon)
    TIME ("is.time", "Checks to see if the current time period falls within the target time period (static value.  a number - i.e; 1=Night, 2=Morning, etc..)"),

    // match a certain day of the week (i.e; 0=Monday, 1=Tuesday)
    TIME_WEEKDAY ("is.weekday", "Checks to see if the current time falls within the target weekday (static value.  a number - i.e; 1=Monday, 2=Tuesday, etc..)"),

    // match a certain month (i.e; 0=January, 1=February)
    TIME_MONTH ("is.month", "Checks to see if the current time falls within the target month (static value.  a number - i.e; 1=January, 2=February, etc...)"),

    // whether or not time is stopped
    FROZEN_TIME ("is.time.frozen", "Checks to see if time is stopped/paused."),

    IS_WEARABLE ("is.bare", "Whether the subject item is an article of clothing, and the target entity (optional, if left out, this assumes the narrator) is wearing nothing else on that body part."),

    // is Character X (subject) wearing Item Y (target)
    IS_WEARING ("is.wearing", "Checks to see if the subject item is being worn by the target entity (optional, if left out, this assumes the narrator)"),

    // is item (subject) size too much for reader to carry
    TOO_BIG_TO_CARRY ("too.big", "Checks to see if the subject item is too large for the target entity to carry."),

    // does a stat roll
    RANDOM_PERCENT_OF_PROPERTY ("random.property.percent", "uses a 1-100 integer property (of the subject) as a percent chance of resolving to true."),

    // do a random roll (i.e; 1 in X chance)
    RANDOM_PERCENT ("random.percent", "test if a random number from 1 to 100 is greater than the target static value"),

    KNOWS_TOPIC ("knows.topcs", "Tests whether the reader knows a given topic (target static value)"),

    IS_LIGHT_SOURCE("is.light.source", "Test whether the subject item emits light (can enter dark locations)"),

    HOLDING_LIGHT_SOURCE("holding.light.source", "Test whether the subject entity (typically the Reader) has a content item that is a light source"),

    IS_DARK("is.dark", "Test whether the subject location is dark (cannot be entered without a light source)"),

    ;

    private String text;
    private String explanation;

    ConditionType(String text, String explanation) {
        this.text = text;
        this.explanation = explanation;
    }

    public String getText() {
        return text;
    }

    public static ConditionType forText(String text) {
        ConditionType foundType = null;
        for (ConditionType type : ConditionType.values()) {
            if (type.getText().equals(text)) {
                foundType = type;
            }
        }
        return foundType;
    }

    public String getExplanation() {
        return explanation;
    }


}
