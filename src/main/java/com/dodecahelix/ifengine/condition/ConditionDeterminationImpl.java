/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.entity.ResolutionUtils;
import com.dodecahelix.ifengine.util.StringUtils;

public class ConditionDeterminationImpl implements ConditionDetermination {

    private static Logger LOGGER = LoggerFactory.getLogger(ConditionDeterminationImpl.class);

    private ConditionResolutionLocator resolverLocator = new ConditionResolutionLocator();

    private EnvironmentDAO dao;

    public ConditionDeterminationImpl(EnvironmentDAO dao, ConditionResolutionLocator conditionResolutionLocator) {
        this.dao = dao;
        this.resolverLocator = conditionResolutionLocator;
    }

    @Override
    public boolean determine(Condition condition, String subjectEntityId, String localTargetId) {

        boolean resolution = false;

        try {
            // based on condition type, resolve the condition
            ConditionResolver resolver = resolverLocator.getResolverForType(condition.getType());
            resolver.setEnvironmentDAO(dao);

            // resolve Subject (required)
            boolean skipResolution = false;
            EntityHolder holder = condition.getEntityHolder();
            String subjectId = ResolutionUtils.getSubjectEntityId(holder, subjectEntityId);
            Entity subject = dao.getEntityById(holder.getSubjectType(), subjectId);
            if (subject==null) {
                // can't match a "THIS", so just resolve to false
                if (EntityHolder.THIS_SUBJECT_ID.equalsIgnoreCase(holder.getSubjectEntityId())) {
                    skipResolution = true;
                } else {
                    throw new ResolutionException("unable to find subject with id of " + subjectId);
                }
            }

            // resolve Target (optional)
            Entity target = null;
            if (TargetType.ENTITY.equals(condition.getTargetType())) {
                String targetId = ResolutionUtils.getTargetEntityId(holder, localTargetId);
                target = dao.getEntityById(holder.getTargetType(), targetId);
                if (target==null) {
                    // doesn't match a "LOCAL", just resolve to false
                    if (EntityHolder.LOCAL_TARGET_ID.equalsIgnoreCase(holder.getTargetEntityId())) {
                        skipResolution = true;
                    } else {
                        throw new ResolutionException("unable to find target with id of " + targetId);
                    }
                }
            }

            if (!skipResolution) {
                if (LOGGER.isTraceEnabled()) {
                    String resolveMessage = String.format("Resolution for %s using parentEntityId %s and localTargetId %s", condition.getType(), subjectEntityId, localTargetId);
                    LOGGER.trace(resolveMessage);
                }
                resolution = resolver.resolve(condition, subject, target);

                if (ConditionOperator.FALSE.equals(condition.getOperator())) {
                    resolution = !resolution;
                }
            }

        } catch (ResolutionException e) {
            e.printStackTrace();

            String errorMessage = String.format("Resolution exception for type %s", condition.getType());
            LOGGER.error(errorMessage, e);
            resolution = false;
        }

        return resolution;
    }

    /**
     *   Evaluates a set of condition objects using an AND or OR criteria.
     *
     *   For an AND condition, if you find any false conditions, this will evaluate to false
     *   For an OR condition, if you find any true conditions, this will evaluate to true
     *   Otherwise, the value will be null
     *
     * @param conditions
     * @param andSet
     * @param subjectEntityId
     * @return
     */
    private Boolean determine(Set<Condition> conditions, boolean andSet, String subjectEntityId, String localTargetId) {

        // return null if the condition is undetermined after parsing
        Boolean meets = null;

        for (Condition condition : conditions) {
            boolean meetsSingle = determine(condition, subjectEntityId, localTargetId);

            // in an AND set, if any condition is false, the condition is false;
            if (!meetsSingle && andSet) {
                meets = false;
                break;
            }

            // in an OR set, if any condition is true, then condition is true;
            if (meetsSingle && !andSet) {
                meets = true;
                break;
            }
        }

        return meets;
    }

    /**
     *      Evaluates a set of reference condition objects using AND or OR criteria.
     *
     *   For an AND condition, if you find any false conditions, this will evaluate to false
     *   For an OR condition, if you find any true conditions, this will evaluate to true
     *   Otherwise, the value will be null (unresolved)
     *
     * @param referenceConditions
     * @param andSet
     * @param subjectEntityId
     * @return
     */
    private Boolean determineReferences(Set<String> referenceConditions, boolean andSet, String subjectEntityId, String localTargetId) {
        Boolean meets = null;

        for (String conditionRef : referenceConditions) {
            ConditionSet conditionSet = findReferenceCondition(conditionRef);
            if (conditionSet==null) {
                throw new IllegalStateException("unable to find reference condition : " + conditionRef);
            }

            Boolean meetsReference = determine(conditionSet.getConditions(), conditionSet.isAndSet(), subjectEntityId, localTargetId);
            if (meetsReference==null) {
                // reference conditions themselves should not be unresolved.
                if (conditionSet.isAndSet()) {
                    // nothing has been found to be false, thus it is true
                    meetsReference = true;
                } else {
                    // nothing found to be true, thus is it false
                    meetsReference = false;
                }
            }

            // in an AND set, if any condition is false, the condition is false;
            if (!meetsReference && andSet) {
                meets = false;
                break;
            }

            // in an OR set, if any condition is true, then condition is true;
            if (meetsReference && !andSet) {
                meets = true;
                break;
            }
        }

        return meets;
    }

    /**
     *   Evaluates a set of ConditionSet objects using an AND or OR criteria.
     *
     *   For an AND condition, if you find any false conditions, this will evaluate to false
     *   For an OR condition, if you find any true conditions, this will evaluate to true
     *   Otherwise, the value will be null
     *
     * @param subsets
     * @param andSet
     * @param subjectEntityId
     * @return
     */
    private Boolean determineSubsets(Set<ConditionSet> subsets, boolean andSet, String subjectEntityId, String localTargetId) {
        Boolean meets = null;

        for (ConditionSet conditionSet : subsets) {
            boolean meetsSingle = determineSet(conditionSet, subjectEntityId, localTargetId);

            // in an AND set, if any condition is false, the condition is false;
            if (!meetsSingle && andSet) {
                meets = false;
                break;
            }

            // in an OR set, if any condition is true, then condition is true;
            if (meetsSingle && !andSet) {
                meets = true;
                break;
            }
        }

        return meets;
    }


    @Override
    public boolean determineSet(ConditionSet conditionSet, String subjectEntityId, String localTargetId) {

        // first, test the conditions
        Boolean meets = determine(conditionSet.getConditions(), conditionSet.isAndSet(), subjectEntityId, localTargetId);
        if (meets==null) {
            // next, test reference conditions
            meets = determineReferences(conditionSet.getReferenceConditions(), conditionSet.isAndSet(), subjectEntityId, localTargetId);

            if (meets==null) {
                meets = determineSubsets(conditionSet.getSubsets(), conditionSet.isAndSet(), subjectEntityId, localTargetId);
            }
        }

        // you've found a definite answer from the subconditions.  this is resolved
        if (meets!=null) {
            return meets;
        }

        if (conditionSet.isAndSet()) {
            // AND set: havent found any false conditions in the entire set, so this must be true
            return true;
        } else {
            // OR set: havent found any true conditions in the entire set, so this must be false
            return false;
        }

    }


    private ConditionSet findReferenceCondition(String conditionRef) {

        for (ConditionSet sharedCondition : dao.getEnvironment().getSharedConditions()) {
            if (StringUtils.equalsIgnoreCase(conditionRef, sharedCondition.getReferenceId())) {
                return sharedCondition;
            }
        }

        return null;
    }

}
