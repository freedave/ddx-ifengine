/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.dialog;

import com.dodecahelix.ifengine.Constants;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.response.DynamicTextFilter;
import com.dodecahelix.ifengine.response.ResponseType;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;
import com.dodecahelix.ifengine.choice.ChoiceResult;

public class DialogHandlerImpl implements DialogHandler {

    private ExecutionResultHandler resultHandler;
    private DynamicTextFilter responseFilter;

    public DialogHandlerImpl(EnvironmentDAO dao,
            ExecutionResultHandler resultHandler,
            DynamicTextFilter textFilter) {
        //resultHandler = new ResultHandlerImpl(dao);
        //responseFilter = new DynamicTextFilterImpl(dao);

        this.resultHandler = resultHandler;
        this.responseFilter = textFilter;
    }

    @Override
    public ChoiceResult handle(Dialog dialog, String personId) {

        ChoiceResult result = new ChoiceResult();
        filterResponse(result, dialog.getResponse(), personId);

        ExecutionResults executionResult = resultHandler.execute(dialog.getResults(), personId, personId);
        for (String executionMessage : executionResult.getMessages()) {
            filterResponse(result, executionMessage, personId);
        }

        if (dialog.isOnce()) {
            dialog.setDisabled(true);
        }

        return result;
    }

    private void filterResponse(ChoiceResult result, String responseText, String personId) {
        try {
            // filter dynamic property tokens
            String filteredResponse = responseFilter.filter(responseText, personId, personId);

            String[] lines = filteredResponse.split(Constants.NEWLINE_TOKEN);
            for (String line : lines) {
                StyledResponseLine responseLine = new StyledResponseLine(line);
                responseLine.setType(ResponseType.DIALOG);
                result.addResponseLine(responseLine);
            }
        } catch (Exception e) {
            // if filtering chokes, spit out a stack trace and print out the line unfiltered
            e.printStackTrace();

            StyledResponseLine responseLine = new StyledResponseLine(responseText);
            responseLine.setType(ResponseType.DIALOG);
            result.addResponseLine(responseLine);
        }
    }

}
