/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.choice;

import java.util.List;
import java.util.Set;

import com.dodecahelix.ifengine.command.CommandHandler;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.dialog.DialogHandler;
import com.dodecahelix.ifengine.options.OptionTreeBuilder;
import com.dodecahelix.ifengine.options.DialogTreeBuilder;
import com.dodecahelix.ifengine.options.item.GroupListItem;
import com.dodecahelix.ifengine.response.ResponseType;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.ifengine.time.TimeHandler;
import com.dodecahelix.ifengine.action.ActionHandler;
import com.dodecahelix.ifengine.util.StringUtils;

/**
 *   Primary interface with the client (i.e; android) app, handling "choices".
 *   
 *   <p>A "choice" is the option decided by the reader, either a 'command' or a 'dialog',
 *   from the provided hierarchy of options.
 *
 *   <p></p>After processing, a "ChoiceResult" will be returned, consisting of the feedback for the choice,
 *   and a new hierarchy of options to choose from.
 *   
 */
public class ChoiceProcessorImpl implements ChoiceProcessor {

    private EnvironmentDAO environmentDao;

    private ChoiceRecorder choiceRecorder;
    private CommandHandler commandHandler;
    private ActionHandler actionHandler;
    private DialogHandler dialogHandler;
    private TimeHandler timeHandler;
    private StoryCompleteHandler storyCompleteHandler;
    private CharacterMovementHandler characterMovementHandler;

    private OptionTreeBuilder optionTreeBuilder;
    private DialogTreeBuilder dialogTreeBuilder;

    public ChoiceProcessorImpl(EnvironmentDAO environmentDao,
                               OptionTreeBuilder optionTreeBuilder,
                               DialogTreeBuilder dialogTreeBuilder,
                               CommandHandler commandHandler,
                               DialogHandler dialogHandler,
                               ActionHandler actionHandler,
                               TimeHandler timeHandler,
                               ChoiceRecorder choiceRecorder) {

        this.environmentDao = environmentDao;
        this.actionHandler = actionHandler;
        this.timeHandler = timeHandler;
        this.commandHandler = commandHandler;
        this.dialogHandler = dialogHandler;
        this.optionTreeBuilder = optionTreeBuilder;
        this.dialogTreeBuilder = dialogTreeBuilder;
        this.choiceRecorder = choiceRecorder;

        this.storyCompleteHandler = new StoryCompleteHandler(environmentDao);
        this.characterMovementHandler = new CharacterMovementHandler();
    }

    @Override
    public ChoiceResult processDialog(Dialog dialog, String personId) {

        ChoiceResult result = null;
        try {
            result = dialogHandler.handle(dialog, personId);

            List<Dialog> subDialogs = dialog.getSubDialogs();
            GroupListItem optionTree = null;
            if (subDialogs!=null && !subDialogs.isEmpty()) {
                GroupListItem dialogs = dialogTreeBuilder.buildDialogTree(subDialogs, personId);
                if (!dialogs.getChildListItems().isEmpty()) {
                    // only use the dialog tree if there are valid subdialogs
                    optionTree = dialogs;
                }
            }

            if (optionTree==null) {
                // no subdialogs.  Followup with a regular command tree
                optionTree = optionTreeBuilder.buildOptionTree();
            }

            result.setOptionTree(optionTree);
            choiceRecorder.recordDialog(dialog, result, personId);
        } catch (Exception e) {
            result = sendExceptionResult(e, dialog.getOptionDisplay());
        }

        return result;
    }

    @Override
    public ChoiceResult processCommand(Command command, String subjectEntityId, String localTargetId) {

        ChoiceResult result = null;
        try {
            if (command.isTemporal() && environmentDao.getEnvironment().isFrozenInTime()) {
                // trying to execute a temporal command when time is frozen
                // should not get to this point if the command tree filtering is correct
                result = new ChoiceResult();
                StyledResponseLine responseLine = new StyledResponseLine(environmentDao.getEnvironment()
                    .getConfiguration()
                    .getMessageConfiguration()
                    .getNotEnoughTimeMessage());

                result.addResponseLine(responseLine);
            } else {
                String beforeLocation = environmentDao.getCurrentLocation().getId();
                Set<Person> beforePeople = environmentDao.getPeopleForCurrentLocation();

                result = commandHandler.handle(command, subjectEntityId, localTargetId);

                // process the passage of time
                int timePassage = command.getTimeToExecute();
                if (!result.isFailure() && timePassage>0) {
                    timeHandler.increment(environmentDao.getEnvironment().getCurrentTime(), timePassage);
                }

                if (StringUtils.equalsIgnoreCase(environmentDao.getCurrentLocation().getId(), beforeLocation)) {
                    // process the movement of people
                    Set<Person> afterPeople = environmentDao.getPeopleForCurrentLocation();
                    result = characterMovementHandler.handleCharacterMovement(result, beforePeople, afterPeople);
                }

                // process global actions
                // NOTE: global actions are independent of the command
                result = actionHandler.processGlobalActions(result);
            }

            if (!environmentDao.getEnvironment().isStoryComplete()) {
                // build the command tree
                GroupListItem commandTree = optionTreeBuilder.buildOptionTree();
                result.setOptionTree(commandTree);
            } else {
                storyCompleteHandler.storyComplete(result);
            }

            choiceRecorder.recordCommand(command, result, subjectEntityId, localTargetId);
        } catch (Exception e) {
            result = sendExceptionResult(e, command.getCommandDisplay());
        }

        return result;
    }

    @Override
    public ChoiceResult replayChoices(List<ChoiceRecord> record) {
        ChoiceResult lastChoiceResult = null;
        try {
            for (ChoiceRecord choice : record) {
                if (choice.isDialog()) {
                    Dialog dialog = environmentDao.findDialogByReferenceId(choice.getReferenceId());
                    lastChoiceResult = processDialog(dialog, choice.getPersonId());
                } else {
                    Command command = environmentDao.findCommandByReferenceId(choice.getReferenceId());
                    lastChoiceResult = processCommand(command, choice.getSubjectId(), choice.getTargetId());
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException("failed replaying choice record.  please review log for result.");
        }

        return lastChoiceResult;
    }

    /**
     *  Handles any exception that is thrown during turn or dialog processing.
     *
     * @param e
     * @param optionDisplay
     * @return
     */
    private ChoiceResult sendExceptionResult(Exception e, String optionDisplay) {
        e.printStackTrace();

        ChoiceResult result = new ChoiceResult();
        result.setException(true);

        String exceptionMessage = environmentDao.getEnvironment()
            .getConfiguration()
            .getMessageConfiguration()
            .getGenericExceptionMessage();

        StyledResponseLine responseLine = new StyledResponseLine(ResponseType.SYSTEM, exceptionMessage);

        result.addResponseLine(responseLine);
        result.setCommandDisplay(optionDisplay);

        // by nullifying this field, the command tree should stay the same as before
        result.setOptionTree(null);
        return result;
    }

}
