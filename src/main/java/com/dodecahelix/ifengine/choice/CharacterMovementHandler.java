/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.choice;

import java.util.Set;

import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.ifengine.util.StringUtils;

public class CharacterMovementHandler {

    public ChoiceResult handleCharacterMovement(ChoiceResult result, Set<Person> beforePeople, Set<Person> afterPeople) {

        for (Person beforePerson : beforePeople) {
            if (!isCharacterInGroup(beforePerson, afterPeople)) {
                // person left!
                result.addResponseLine(new StyledResponseLine(beforePerson.getTitle() + " left the area."));
            }
        }

        for (Person afterPerson : afterPeople) {
            if (!isCharacterInGroup(afterPerson, beforePeople)) {
                // person arrived!
                result.addResponseLine(new StyledResponseLine(afterPerson.getTitle() + " arrived."));
            }
        }

        return result;
    }

    private boolean isCharacterInGroup(Person person, Set<Person> group) {
        boolean inGroup = false;
        String personId = person.getId();
        for (Person groupPerson : group) {
            if (StringUtils.equalsIgnoreCase(personId, groupPerson.getId())) {
                inGroup = true;
            }
        }
        return inGroup;
    }

}
