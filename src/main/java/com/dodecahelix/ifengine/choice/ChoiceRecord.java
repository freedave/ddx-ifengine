/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.choice;

public class ChoiceRecord {

    private boolean dialog = false;

    private String referenceId;

    private String subjectId;
    private String targetId;
    private String personId;

    private String responseFragment;

    public ChoiceRecord(String commandId, String subjectId, String targetId) {
        super();
        dialog = false;

        this.referenceId = commandId;
        this.subjectId = subjectId;
        this.targetId = targetId;
    }

    public ChoiceRecord(String dialogId, String personId) {
        super();
        dialog = true;

        this.referenceId = dialogId;
        this.personId = personId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public boolean isDialog() {
        return dialog;
    }

    public String getResponseFragment() {
        return responseFragment;
    }

    public void setResponseFragment(String responseFragment) {
        this.responseFragment = responseFragment;
    }

}
