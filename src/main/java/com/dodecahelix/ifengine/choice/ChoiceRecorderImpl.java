/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.choice;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.ifengine.util.StringUtils;

public class ChoiceRecorderImpl implements ChoiceRecorder {

    private List<ChoiceRecord> record = new ArrayList<ChoiceRecord>();

    public ChoiceRecorderImpl() {
    }

    @Override
    public void recordCommand(Command command, ChoiceResult choiceResult, String subjectId, String targetId) {
        ChoiceRecord turn = new ChoiceRecord(command.getReferenceId(), subjectId, targetId);

        StyledResponseLine firstLine = choiceResult.getResponseLines().get(0);
        String fragment = StringUtils.truncate(firstLine.getText(), 20, false);
        turn.setResponseFragment(fragment);
        record.add(turn);
    }

    @Override
    public void recordDialog(Dialog dialog, ChoiceResult choiceResult, String personId) {
        ChoiceRecord turn = new ChoiceRecord(dialog.getReferenceId(), personId);

        StyledResponseLine firstLine = choiceResult.getResponseLines().get(0);
        String fragment = StringUtils.truncate(firstLine.getText(), 20, false);
        turn.setResponseFragment(fragment);
        record.add(turn);
    }

    @Override
    public void resetRecord() {
        record.clear();
    }

    @Override
    public void printRecord() {
        System.out.println("---RECORD BEGIN---");

        String turnRecordFormat = "%s,%s,%s,%s,%s";
        String message;
        for (ChoiceRecord turn : record) {
            if (!turn.isDialog()) {
                message = String.format(turnRecordFormat, "COMMAND", turn.getReferenceId(), turn.getSubjectId(), turn.getTargetId(), turn.getResponseFragment());
            } else {
                message = String.format(turnRecordFormat, "DIALOG", turn.getReferenceId(), turn.getPersonId(), "x", turn.getResponseFragment());
            }
            System.out.println(message);
        }

        System.out.println("---RECORD END---");
    }

    @Override
    public List<ChoiceRecord> getRecord() {
        return record;
    }

}
