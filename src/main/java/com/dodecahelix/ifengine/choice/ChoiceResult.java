/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.choice;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.options.item.GroupListItem;
import com.dodecahelix.ifengine.response.StyledResponseLine;

/**
 *  Result after processing a turn.
 *
 */
public class ChoiceResult {

    /**
     *   Flag to indicate that there were actions to be processed, but none were valid
     */
    private boolean failure = false;

    /**
     *   Indicates an exception was caught when processing the turn (response lines will contain the error message)
     */
    private boolean exception = false;

    /**
     *  display text of the reader's command that was just executed
     */
    private String commandDisplay;

    /**
     *  hierarchy of choices available to the reader for the next turn.
     */
    private GroupListItem optionTree;

    /**
     *  display text to be presented by the client
     *
     */
    private List<StyledResponseLine> responseLines;

    public ChoiceResult() {
        responseLines = new ArrayList<StyledResponseLine>();
    }

    public void addResponseLine(StyledResponseLine responseLine) {
        responseLines.add(responseLine);
    }

    public void clearResponses() {
        responseLines.clear();
    }

    public List<StyledResponseLine> getResponseLines() {
        return responseLines;
    }

    public GroupListItem getOptionTree() {
        return optionTree;
    }

    public void setOptionTree(GroupListItem optionTree) {
        this.optionTree = optionTree;
    }

    public void setResponseLines(List<StyledResponseLine> responseLines) {
        this.responseLines = responseLines;
    }

    public boolean isFailure() {
        return failure;
    }

    public void setFailure(boolean failure) {
        this.failure = failure;
    }

    public String getCommandDisplay() {
        return commandDisplay;
    }

    public void setCommandDisplay(String commandDisplay) {
        this.commandDisplay = commandDisplay;
    }

    public boolean isException() {
        return exception;
    }

    public void setException(boolean exception) {
        this.exception = exception;
    }

}
