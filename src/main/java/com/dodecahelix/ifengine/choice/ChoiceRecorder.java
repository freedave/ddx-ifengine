/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.choice;

import java.util.List;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Dialog;

/**
 *  Records the choices (commands and dialogs) that are made for playback.
 */
public interface ChoiceRecorder {

    public void recordCommand(Command command, ChoiceResult result, String subjectId, String targetId);

    public void recordDialog(Dialog dialog, ChoiceResult result, String personId);

    /**
     *  clear all TurnRecords
     */
    public void resetRecord();

    /**
     *   prints the current record (formatted) to the console
     */
    public void printRecord();

    /**
     *  retrieves the record of turns
     *
     * @return
     */
    public List<ChoiceRecord> getRecord();

}
