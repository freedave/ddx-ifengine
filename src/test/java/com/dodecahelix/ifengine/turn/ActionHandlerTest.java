/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.turn;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import com.dodecahelix.ifengine.action.ActionHandler;
import com.dodecahelix.ifengine.action.ActionHandlerImpl;
import com.dodecahelix.ifengine.action.ActionResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.response.StyledResponseFilter;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;

@RunWith(MockitoJUnitRunner.class)
public class ActionHandlerTest {

    private ActionHandler actionHandler;

    private ConditionDetermination conditionDetermination;
    private ExecutionResultHandler resultHandler;
    private StyledResponseFilter responseFilter;
    private Environment environment;

    @Before
    public void setup() {
        EnvironmentDAO dao = mock(EnvironmentDAO.class);
        environment = new Environment();
        when(dao.getEnvironment()).thenReturn(environment);

        conditionDetermination = mock(ConditionDetermination.class);
        resultHandler = mock(ExecutionResultHandler.class);
        responseFilter = mock(StyledResponseFilter.class);

        actionHandler = new ActionHandlerImpl(dao, conditionDetermination, resultHandler, responseFilter);
    }

    @Test
    public void testHandleAction() {

        Action action = new Action();
        action.setExecutionMessage("Foo");
        ExecutionResults result = new ExecutionResults();
        result.setMessages(Arrays.asList("Bar"));

        List<StyledResponseLine> actionResponse = Arrays.asList(new StyledResponseLine("Boom!"));

        ActionResult followupResult = new ActionResult();
        followupResult.addResponseLine(new StyledResponseLine("YAA"));

        when( resultHandler.execute(any(ExecutionSet.class), anyString(), anyString()) ).thenReturn(result);
        when( responseFilter.filterResponse(anyString(), anyString(), anyString()) ).thenReturn(actionResponse);
        when( conditionDetermination.determineSet(any(ConditionSet.class), anyString(), anyString())).thenReturn(true);

        ActionResult actionResult = actionHandler.handleAction(action, null, null);

        // shoudl be two responses:  one for the action, and one for the result execution message (uncommon)
        assertEquals(2, actionResult.getResponseLines().size());
        assertEquals("Boom!", actionResult.getResponseLines().get(0).getText());
        assertEquals("Boom!", actionResult.getResponseLines().get(1).getText());

    }


}
