/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.turn;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import com.dodecahelix.ifengine.action.ActionHandler;
import com.dodecahelix.ifengine.choice.ChoiceProcessor;
import com.dodecahelix.ifengine.choice.ChoiceProcessorImpl;
import com.dodecahelix.ifengine.choice.ChoiceRecorder;
import com.dodecahelix.ifengine.choice.ChoiceResult;
import com.dodecahelix.ifengine.data.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.dodecahelix.ifengine.command.CommandHandler;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dialog.DialogHandler;
import com.dodecahelix.ifengine.options.OptionTreeBuilder;
import com.dodecahelix.ifengine.options.DialogTreeBuilder;
import com.dodecahelix.ifengine.time.TimeHandler;

@RunWith(MockitoJUnitRunner.class)
public class ChoiceProcessorTest {

    private ChoiceProcessor choiceProcessor;
    private DialogHandler dialogHandler;
    private OptionTreeBuilder optionTreeBuilder;
    private DialogTreeBuilder dialogTreeBuilder;
    private CommandHandler commandHandler;
    private ActionHandler actionHandler;
    private TimeHandler timeHandler;
    private ChoiceRecorder choiceRecorder;

    private Environment environment;

    @Before
    public void setup() {
        EnvironmentDAO dao = mock(EnvironmentDAO.class);
        environment = new Environment();

        when(dao.getEnvironment()).thenReturn(environment);
        when(dao.getCurrentLocation()).thenReturn(new Location("room","blah"));

        optionTreeBuilder = mock(OptionTreeBuilder.class);
        dialogTreeBuilder = mock(DialogTreeBuilder.class);
        commandHandler = mock(CommandHandler.class);
        dialogHandler = mock(DialogHandler.class);
        actionHandler = mock(ActionHandler.class);
        timeHandler = mock(TimeHandler.class);
        choiceRecorder = mock(ChoiceRecorder.class);

        choiceProcessor = new ChoiceProcessorImpl(dao, optionTreeBuilder, dialogTreeBuilder, commandHandler, dialogHandler, actionHandler, timeHandler, choiceRecorder);
    }

    @Test
    public void testProcessChoice() {
        Command command = new Command();
        command.setTimeToExecute(5);

        ChoiceResult expectedResult = new ChoiceResult();
        when( commandHandler.handle(any(Command.class), anyString(), anyString())).thenReturn(expectedResult);
        when( actionHandler.processGlobalActions(any(ChoiceResult.class))).thenReturn(expectedResult);

        ChoiceResult result = choiceProcessor.processCommand(command, null, null);
        assertFalse(result.isException());

        // there is a time to execute.  make sure this is incremented
        verify(timeHandler).increment(any(Time.class), anyInt());

        // story should not be complete, so we should have built a command tree
        verify(optionTreeBuilder).buildOptionTree();
    }

    @Test
    public void testProcessDialog() {
        Dialog dialog = mock(Dialog.class);
        String personId = "";

        ChoiceResult expectedResult = new ChoiceResult();
        expectedResult.setException(false);
        when( dialogHandler.handle(any(Dialog.class), anyString())).thenReturn(expectedResult);

        ChoiceResult actualResult = choiceProcessor.processDialog(dialog, personId);
        assertFalse(actualResult.isException());
    }

    @Test
    public void testProcessTemporalTurn() {

        // two conditions - must be frozen in time, and the command must be temporal
        environment.setFrozenInTime(true);
        Command command = new Command();
        command.setTimeToExecute(5);
        environment.getConfiguration().getMessageConfiguration().setNotEnoughTimeMessage("NOT");

        ChoiceResult result = choiceProcessor.processCommand(command, null, null);
        assertFalse(result.isException());
        assertEquals("NOT", result.getResponseLines().get(0).getText());
    }

    @Test
    public void testProcessStoryComplete() {

        environment.setStoryComplete(true);
        Command command = new Command();

        ChoiceResult expectedResult = new ChoiceResult();
        when( commandHandler.handle(any(Command.class), anyString(), anyString())).thenReturn(expectedResult);
        when( actionHandler.processGlobalActions(any(ChoiceResult.class))).thenReturn(expectedResult);

        ChoiceResult result = choiceProcessor.processCommand(command, null, null);
        assertFalse(result.isException());

        assertEquals("Story Complete", result.getOptionTree().getLabel());
    }

}
