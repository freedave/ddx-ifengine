/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.turn;

import java.io.IOException;
import java.util.List;

import com.dodecahelix.ifengine.action.ActionHandler;
import com.dodecahelix.ifengine.action.ActionHandlerImpl;
import com.dodecahelix.ifengine.choice.*;
import com.dodecahelix.ifengine.data.serializer.JsonStoryBundle;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.dodecahelix.ifengine.command.CommandHandler;
import com.dodecahelix.ifengine.command.CommandHandlerImpl;
import com.dodecahelix.ifengine.command.defaults.BuiltinCommandBuilder;
import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.condition.ConditionDeterminationImpl;
import com.dodecahelix.ifengine.condition.ConditionResolutionLocator;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Dialog;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.data.serializer.json.JsonStoryBundleLoader;
import com.dodecahelix.ifengine.dialog.DialogHandler;
import com.dodecahelix.ifengine.dialog.DialogHandlerImpl;
import com.dodecahelix.ifengine.options.OptionTreeBuilder;
import com.dodecahelix.ifengine.options.OptionTreeBuilderFactory;
import com.dodecahelix.ifengine.options.DialogTreeBuilder;
import com.dodecahelix.ifengine.response.DynamicTextFilter;
import com.dodecahelix.ifengine.response.DynamicTextFilterImpl;
import com.dodecahelix.ifengine.response.StyledResponseFilter;
import com.dodecahelix.ifengine.response.StyledResponseFilterImpl;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;
import com.dodecahelix.ifengine.result.ExecutionResultHandlerImpl;
import com.dodecahelix.ifengine.result.ExecutionServiceLocator;
import com.dodecahelix.ifengine.time.TimeHandler;
import com.dodecahelix.ifengine.util.IoUtils;

/**
 *   This runs a recorded script of commands on a feature-rich story 
 *   to identify any issues with the codebase.
 *   
 *   <p>(Temporarily @Ignored)
 *
 */
public class TwistedWoodScriptRunnerTest {

    private static String TEST_ENV_FILE = "twistedwood-test.json";
    private static String TEST_RECORD = "twisted-script.txt";

    private ChoiceProcessor choiceProcessor;
    private EnvironmentDAO environmentDao;

    @Ignore
    @Test
    public void test() throws Exception {

        Environment environment = loadTestEnvironment();
        buildTurnProcessor(environment);

        List<String> record = IoUtils.readFileAsStringList(TEST_RECORD);
        for (int lineNum = 0; lineNum<record.size(); lineNum++) {
            String line = record.get(lineNum);
            System.out.println("processing turn from record line " + lineNum + " : " + line);
            processTurn(line);
        }
    }

    private void processTurn(String turn) {
        String[] turnParts = turn.split(",");
        String turnType = turnParts[0];

        if ("DIALOG".equalsIgnoreCase(turnType)) {
            processDialog(turnParts);
        }
        if ("COMMAND".equalsIgnoreCase(turnType)) {
            processCommand(turnParts);
        }
    }

    private void processCommand(String[] turnParts) {
        String refId = turnParts[1];
        String parentEntityId = turnParts[2];
        if ("null".equalsIgnoreCase(parentEntityId)) {
            parentEntityId = null;
        }

        String localTargetId = turnParts[3];
        if ("null".equalsIgnoreCase(localTargetId)) {
            localTargetId = null;
        }

        String responseFragment = turnParts[4];
        if ("null".equalsIgnoreCase(responseFragment)) {
            responseFragment = null;
        }

        Command command = environmentDao.findCommandByReferenceId(refId);
        Assert.assertNotNull("no command found for reference id of " + refId, command);
        ChoiceResult result = choiceProcessor.processCommand(command, parentEntityId, localTargetId);

        assertContainsFragment(result, responseFragment);
    }

    private void processDialog(String[] turnParts) {
        String refId = turnParts[1];
        String personId = turnParts[2];
        if ("null".equalsIgnoreCase(personId)) {
            personId = null;
        }

        String responseFragment = turnParts[4];
        if ("null".equalsIgnoreCase(responseFragment)) {
            responseFragment = null;
        }

        Dialog dialog = environmentDao.findDialogByReferenceId(refId);
        Assert.assertNotNull("no dialog found for reference id of " + refId, dialog);
        ChoiceResult result = choiceProcessor.processDialog(dialog, personId);

        assertContainsFragment(result, responseFragment);
    }

    private void assertContainsFragment(ChoiceResult result, String responseFragment) {
        boolean hasFragment = false;
        String fullResponse = "";
        for (StyledResponseLine line : result.getResponseLines()) {
            fullResponse += line.getText();
            if (line.getText().contains(responseFragment)) {
                hasFragment = true;
            }
        }

        if (!hasFragment) {
            String message = "turn failure : expected fragment '%s' : actual '%s'";
            Assert.fail(String.format(message, responseFragment, fullResponse));
        }

    }

    private ChoiceProcessor buildTurnProcessor(Environment environment) {
        environmentDao = new SimpleEnvironmentDAO(environment);

        ConditionDetermination conditionDetermination = new ConditionDeterminationImpl(environmentDao, new ConditionResolutionLocator());
        OptionTreeBuilderFactory optionTreeBuilderFactory = new OptionTreeBuilderFactory(environmentDao, conditionDetermination);
        OptionTreeBuilder optionTreeBuilder = new OptionTreeBuilder(optionTreeBuilderFactory);
        DialogTreeBuilder dialogTreeBuilder = new DialogTreeBuilder(environmentDao, conditionDetermination);
        DynamicTextFilter textFilter = new DynamicTextFilterImpl(environmentDao);
        ExecutionResultHandler resultHandler = new ExecutionResultHandlerImpl(environmentDao, new ExecutionServiceLocator());
        StyledResponseFilter responseFilter = new StyledResponseFilterImpl(environmentDao);
        ActionHandler actionHandler = new ActionHandlerImpl(environmentDao, conditionDetermination, resultHandler, responseFilter);
        CommandHandler commandHandler = new CommandHandlerImpl(actionHandler, resultHandler, responseFilter);
        DialogHandler dialogHandler = new DialogHandlerImpl(environmentDao, resultHandler, textFilter);
        TimeHandler timeHandler = new TimeHandler(environmentDao);
        ChoiceRecorder choiceRecorder = new ChoiceRecorderImpl();

        choiceProcessor = new ChoiceProcessorImpl(environmentDao, optionTreeBuilder, dialogTreeBuilder,
                commandHandler, dialogHandler, actionHandler, timeHandler, choiceRecorder);

        return choiceProcessor;
    }

    private Environment loadTestEnvironment() throws IOException {
        String jsonFile = IoUtils.readFileAsString(TEST_ENV_FILE);
        Assert.assertTrue(jsonFile.length()>0);

        StoryBundle story = new JsonStoryBundle(jsonFile);

        JsonStoryBundleLoader builder = new JsonStoryBundleLoader();
        builder.setBundle(story);

        Environment environment = builder.build();

        BuiltinCommandBuilder builtinCommandBuilder = new BuiltinCommandBuilder();
        builtinCommandBuilder.buildCommands(environment);

        return environment;
    }
}
