/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.turn;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Set;

import com.dodecahelix.ifengine.choice.ChoiceResult;
import org.junit.Test;

import com.dodecahelix.ifengine.command.defaults.MoveCommandBuilder;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Person;

public class FollowsTurnEndToEndTest extends ChoiceProcessorEndToEndTest {

    @Test
    public void testFollows() {
        ChoiceResult choiceResult = null;

        // ask the dude to follow (it doesnt matter that he is in another room
        Person dude = (Person) environmentDao.getEntityById(EntityType.PERSON, "dude");
        Command followCommand = environmentDao.findCommandByReferenceId(dude, "dude-follow-me");
        assertNotNull(followCommand);

        choiceResult = choiceProcessor.processCommand(followCommand, null, null);
        assertNotNull(choiceResult);

        // move up the stairs
        Location startingLocation = environmentDao.getCurrentLocation();
        String moveCommandRef = String.format(MoveCommandBuilder.EXIT_REF_ID, "livingroom", "upstairs");
        Command goUpstairsCommand = environmentDao.findCommandByReferenceId(startingLocation, moveCommandRef);
        assertNotNull(goUpstairsCommand);

        choiceResult = choiceProcessor.processCommand(goUpstairsCommand, null, null);
        assertNotNull(choiceResult);

        assertEquals("upstairs", environmentDao.getCurrentLocation().getId());

        Set<Person> peopleInCurrentLocation = environmentDao.getPeopleForCurrentLocation();
        assertEquals(1, peopleInCurrentLocation.size());

    }
}
