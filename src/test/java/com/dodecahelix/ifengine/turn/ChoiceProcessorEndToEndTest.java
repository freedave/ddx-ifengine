/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.turn;

import com.dodecahelix.ifengine.action.ActionHandler;
import com.dodecahelix.ifengine.action.ActionHandlerImpl;
import com.dodecahelix.ifengine.choice.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.dodecahelix.ifengine.command.CommandHandler;
import com.dodecahelix.ifengine.command.CommandHandlerImpl;
import com.dodecahelix.ifengine.command.defaults.LookCommandBuilder;
import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.condition.ConditionDeterminationImpl;
import com.dodecahelix.ifengine.condition.ConditionResolutionLocator;
import com.dodecahelix.ifengine.dao.EnvironmentBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.TestEnvironmentBuilder;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.dialog.DialogHandler;
import com.dodecahelix.ifengine.dialog.DialogHandlerImpl;
import com.dodecahelix.ifengine.options.OptionTreeBuilder;
import com.dodecahelix.ifengine.options.OptionTreeBuilderFactory;
import com.dodecahelix.ifengine.options.DialogTreeBuilder;
import com.dodecahelix.ifengine.response.DynamicTextFilter;
import com.dodecahelix.ifengine.response.DynamicTextFilterImpl;
import com.dodecahelix.ifengine.response.StyledResponseFilter;
import com.dodecahelix.ifengine.response.StyledResponseFilterImpl;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;
import com.dodecahelix.ifengine.result.ExecutionResultHandlerImpl;
import com.dodecahelix.ifengine.result.ExecutionServiceLocator;
import com.dodecahelix.ifengine.time.TimeHandler;

/**
 *   End to end test of the IF engine turn processor
 *
 */
public class ChoiceProcessorEndToEndTest {

    protected ChoiceProcessor choiceProcessor;
    protected EnvironmentDAO environmentDao;

    @Before
    public void setup() {
        EnvironmentBuilder builder = new TestEnvironmentBuilder();
        Environment environment = builder.build();
        environmentDao = new SimpleEnvironmentDAO(environment);

        ConditionDetermination conditionDetermination = new ConditionDeterminationImpl(environmentDao, new ConditionResolutionLocator());
        OptionTreeBuilderFactory optionTreeBuilderFactory = new OptionTreeBuilderFactory(environmentDao, conditionDetermination);

        OptionTreeBuilder optionTreeBuilder = new OptionTreeBuilder(optionTreeBuilderFactory);
        DialogTreeBuilder dialogTreeBuilder = new DialogTreeBuilder(environmentDao, conditionDetermination);

        DynamicTextFilter textFilter = new DynamicTextFilterImpl(environmentDao);
        ExecutionResultHandler resultHandler = new ExecutionResultHandlerImpl(environmentDao, new ExecutionServiceLocator());
        StyledResponseFilter responseFilter = new StyledResponseFilterImpl(environmentDao);
        ActionHandler actionHandler = new ActionHandlerImpl(environmentDao, conditionDetermination, resultHandler, responseFilter);
        CommandHandler commandHandler = new CommandHandlerImpl(actionHandler, resultHandler, responseFilter);
        DialogHandler dialogHandler = new DialogHandlerImpl(environmentDao, resultHandler, textFilter);
        TimeHandler timeHandler = new TimeHandler(environmentDao);
        ChoiceRecorder choiceRecorder = new ChoiceRecorderImpl();

        choiceProcessor = new ChoiceProcessorImpl(environmentDao, optionTreeBuilder, dialogTreeBuilder, commandHandler, dialogHandler, actionHandler, timeHandler, choiceRecorder);
    }

    @Test
    public void testTurnProcessor() {

        ChoiceResult choiceResult = null;

        Environment environment = environmentDao.getEnvironment();

        // find the look at myself command
        String lookCommandRef = String.format(LookCommandBuilder.LOOK_REF_ID, EntityType.READER.name());
        CommandConfiguration commandConfig = environment.getConfiguration().getCommandConfiguration();
        for (Command command : commandConfig.getNarratorCommands()) {
            if (command.getReferenceId()!=null && command.getReferenceId().equalsIgnoreCase(lookCommandRef)) {
                choiceResult = choiceProcessor.processCommand(command, null, null);
            }
        }

        Assert.assertNotNull(choiceResult);
        Assert.assertNotNull(choiceResult.getOptionTree());
        Assert.assertTrue(choiceResult.getResponseLines().size() > 0);
        Assert.assertTrue(choiceResult.getOptionTree().getChildListItems().size() > 0);
    }

}
