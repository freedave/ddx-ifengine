/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.serializer.json;

import java.io.IOException;

import com.dodecahelix.ifengine.data.serializer.JsonStoryBundle;
import junit.framework.TestCase;

import org.junit.Assert;

import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.util.IoUtils;

public class JsonEnvironmentBuilderTest extends TestCase {

    private JsonStoryBundleLoader builder;

    public void test() throws IOException {
        String jsonFile = IoUtils.readFileAsString("test.json");
        Assert.assertTrue(jsonFile.length()>0);

        StoryBundle story = new JsonStoryBundle(jsonFile);

        builder = new JsonStoryBundleLoader();
        builder.setBundle(story);

        Environment environment = builder.build();

        // todo - test everything
        Assert.assertEquals("me", environment.getCurrentNarrator());
    }

}
