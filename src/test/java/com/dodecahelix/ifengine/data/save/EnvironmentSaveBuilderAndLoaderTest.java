/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.save;

import com.dodecahelix.ifengine.data.save.json.JsonEnvironmentSave;
import junit.framework.TestCase;

import org.junit.Test;

import com.dodecahelix.ifengine.dao.EnvironmentBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.TestEnvironmentBuilder;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.save.json.JsonEnvironmentSaveBuilder;
import com.dodecahelix.ifengine.data.save.json.JsonEnvironmentSaveLoader;
import com.dodecahelix.ifengine.data.story.Genre;

public class EnvironmentSaveBuilderAndLoaderTest extends TestCase {

    @Test
    public void testJsonSaveGameBuilderAndLoader() {

        EnvironmentBuilder environmentBuilder = new TestEnvironmentBuilder();
        Environment environment = environmentBuilder.build();

        // make a modification that we can test
        environment.getLibraryCard().setGenre(Genre.HISTORICAL);

        EnvironmentDAO simpleDao = new SimpleEnvironmentDAO(environment);

        EnvironmentSaveBuilder builder = new JsonEnvironmentSaveBuilder();
        EnvironmentSave environmentSave = builder.buildFromEnvironment(simpleDao.getEnvironment(), "some-save-name");
        assertTrue(environmentSave instanceof JsonEnvironmentSave);
        String jsonifiedEnvironment = ((JsonEnvironmentSave) environmentSave).getContent();

        assertTrue(jsonifiedEnvironment.contains("kitchen"));
        assertTrue(jsonifiedEnvironment.contains("Dude"));
        assertTrue(jsonifiedEnvironment.contains("Blah"));
        assertTrue(jsonifiedEnvironment.contains(Genre.HISTORICAL.toString()));

        EnvironmentSaveLoader loader = new JsonEnvironmentSaveLoader();

        environment.getLibraryCard().setGenre(Genre.CRIME);
        assertEquals(Genre.CRIME, environment.getLibraryCard().getGenre());

        Environment newEnvironment = loader.buildEnvironmentFromSave(environmentSave);
        assertEquals(Genre.HISTORICAL, newEnvironment.getLibraryCard().getGenre());
    }

}
