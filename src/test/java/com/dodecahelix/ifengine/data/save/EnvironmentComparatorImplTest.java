/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.data.save;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.dodecahelix.ifengine.dao.EnvironmentBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.TestEnvironmentBuilder;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.save.csv.CsvEnvironmentComparator;
import com.dodecahelix.ifengine.data.save.csv.DiffParser;
import com.dodecahelix.ifengine.data.save.csv.DiffType;
import com.dodecahelix.ifengine.data.serializer.csv.ParsedDiff;

public class EnvironmentComparatorImplTest {

    @Test
    public void test() {
        EnvironmentBuilder builder = new TestEnvironmentBuilder();
        Environment environment = builder.build();
        EnvironmentDAO dao = new SimpleEnvironmentDAO(environment);

        EnvironmentComparator ec = new CsvEnvironmentComparator();

        // make some changes
        environment.setCurrentNarrator("me");

        // clone the environment
        Environment dirtyEnvironment = EnvironmentCloneTool.clone(environment);

        // now reset the environment to get the default
        dao.loadEnvironment(builder.build(), true);

        List<String> diff = ec.compareEnvironment(environment, dirtyEnvironment);
        Assert.assertFalse(diff.isEmpty());

        boolean foundCurrLocationDiff = false;
        for (String diffString : diff) {
            ParsedDiff parsedDiffLine = DiffParser.parse(diffString);
            if (DiffType.SET_CURRENT_NARRATOR.equals(parsedDiffLine.getDiffType())) {
                foundCurrLocationDiff = true;
                Assert.assertEquals("me", parsedDiffLine.getStaticValue());
            }
        }
        Assert.assertTrue("The diff did not contain the current narrator", foundCurrLocationDiff);

    }

}
