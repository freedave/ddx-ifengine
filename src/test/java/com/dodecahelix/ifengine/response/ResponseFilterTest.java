/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.response;

import junit.framework.TestCase;

import org.junit.Test;

import com.dodecahelix.ifengine.dao.EnvironmentBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.TestEnvironmentBuilder;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.entity.EntityParser;
import com.dodecahelix.ifengine.entity.ParseException;
import com.dodecahelix.ifengine.response.handlers.DescriptionPropertyHandler;
import com.dodecahelix.ifengine.response.handlers.StoryPassageHandler;
import com.dodecahelix.ifengine.util.IdUtil;

public class ResponseFilterTest extends TestCase {

    private static String TEST_STATIC_TEXT = "Blah";

    private EnvironmentDAO dao;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        EnvironmentBuilder builder = new TestEnvironmentBuilder();
        Environment environment = builder.build();
        dao = new SimpleEnvironmentDAO(environment);
    }

    @Test
    public void testDescriptionResponse() {

        DynamicTextFilter filter = new DynamicTextFilterImpl(dao);

        StringBuffer unfiltered = new StringBuffer(TEST_STATIC_TEXT);
        unfiltered.append(ResponseTokenBuilder.FILTER_START_TOKEN);

        unfiltered.append(EntityType.ITEM);
        unfiltered.append(EntityParser.ENTITY_TOKEN);
        unfiltered.append("knife");
        unfiltered.append(EntityParser.ENTITY_TOKEN);
        unfiltered.append(DescriptionPropertyHandler.DESCRIPTION);

        unfiltered.append(ResponseTokenBuilder.FILTER_CLOSE_TOKEN);
        unfiltered.append(TEST_STATIC_TEXT);

        try {
            String response = filter.filter(unfiltered.toString(), null, null);
            String expected = TEST_STATIC_TEXT + "A Ginsu steak knife." + TEST_STATIC_TEXT;

            assertEquals(expected, response);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Filter threw an exception.");
        }
    }

    @Test
    public void testShowIntroductionResponse() throws InvalidResponseException, ParseException {

        DynamicTextFilter filter = new DynamicTextFilterImpl(dao);

        StringBuffer unfiltered = new StringBuffer(ResponseTokenBuilder.FILTER_START_TOKEN);
        unfiltered.append(IdUtil.fromTitle(EntityType.ENVIRONMENT.getAbbreviation()));
        unfiltered.append(EntityParser.ENTITY_TOKEN);

        unfiltered.append(StoryPassageHandler.PASSAGE);
        unfiltered.append(StoryContent.INTRO_PASSAGE_ID);

        unfiltered.append(ResponseTokenBuilder.FILTER_CLOSE_TOKEN);

        String response = filter.filter(unfiltered.toString(), null, null);

        assertEquals("Prologue: Blah", response);
    }

    @Test
    public void testFilterThisSubjectToken() throws InvalidResponseException, ParseException {

        DynamicTextFilter filter = new DynamicTextFilterImpl(dao);

        StringBuffer unfiltered = new StringBuffer(TEST_STATIC_TEXT);
        unfiltered.append(ResponseTokenBuilder.FILTER_START_TOKEN);

        unfiltered.append(EntityType.ITEM);
        unfiltered.append(EntityParser.ENTITY_TOKEN);
        unfiltered.append(EntityHolder.THIS_SUBJECT_ID);
        unfiltered.append(EntityParser.ENTITY_TOKEN);
        unfiltered.append(DescriptionPropertyHandler.DESCRIPTION);

        unfiltered.append(ResponseTokenBuilder.FILTER_CLOSE_TOKEN);
        unfiltered.append(TEST_STATIC_TEXT);

        try {
            String response = filter.filter(unfiltered.toString(), "knife", null);
            String expected = TEST_STATIC_TEXT + "A Ginsu steak knife." + TEST_STATIC_TEXT;

            assertEquals(expected, response);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Filter threw an exception.");
        }
    }

}
