/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.options.build;

import static org.mockito.Mockito.*;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.configuration.CommandConfiguration;
import com.dodecahelix.ifengine.options.item.CommandListItem;
import com.dodecahelix.ifengine.options.item.ListItem;
import com.dodecahelix.ifengine.util.StringUtils;

public class TestOverrideCommandOption {

    private Environment environment;
    private ConditionDetermination mockConditionDetermination;
    private EnvironmentDAO mockDao;

    @Test
    public void testOverrideCommand() {

        String itemId = "thingamajig";
        // command id of the item-type command to be overridden
        String commandId = "cid-01";
        String commandDisplay = "do stuff";

        Item item = new Item(itemId, itemId, "its a thingamajig");
        Command itemCommand = new Command();
        itemCommand.setReferenceId("cid01-a");
        itemCommand.setOverrideId(commandId);
        itemCommand.setCommandDisplay(commandDisplay);
        itemCommand.setDefaultExecutionMessage("the thingamajig works");
        item.addCommand(itemCommand);

        mockDao = mock(EnvironmentDAO.class);
        mockConditionDetermination = mock(ConditionDetermination.class);
        when(mockConditionDetermination.determineSet(any(ConditionSet.class), any(String.class), any(String.class))).thenReturn(true);

        environment = new Environment();
        when(mockDao.getEnvironment()).thenReturn(environment);
        when(mockDao.getEntityById(EntityType.ITEM, itemId)).thenReturn(item);

        environment.setFrozenInTime(false);
        CommandConfiguration cc = environment.getConfiguration().getCommandConfiguration();
        Command itemTypeCommand = new Command();
        itemTypeCommand.setReferenceId(commandId);
        itemTypeCommand.setCommandDisplay("do stuff");
        itemTypeCommand.setDefaultExecutionMessage("this thing does not work");
        cc.getItemCommands().add(itemTypeCommand);

        ItemOptionListBuilder iclBuilder = new ItemOptionListBuilder(mockDao, mockConditionDetermination);
        List<ListItem> options = iclBuilder.getItemsForGroup(itemId);
        boolean found = false;
        for (ListItem option : options) {
            if (option instanceof CommandListItem) {
                Command command = ((CommandListItem)option).getCommand();
                if (StringUtils.equalsIgnoreCase(command.getReferenceId(), "cid01-a")) {
                    found = true;
                }
                if (StringUtils.equalsIgnoreCase(command.getReferenceId(), commandId)) {
                    Assert.fail("overridden item-type command was not filtered from options");
                }
            }
        }

        if (!found) {
            Assert.fail("item command did not appear in options");
        }

    }

}
