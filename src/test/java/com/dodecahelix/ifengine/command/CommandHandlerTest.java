/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.response.StyledResponseFilter;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;
import com.dodecahelix.ifengine.action.ActionHandler;
import com.dodecahelix.ifengine.action.ActionResult;
import com.dodecahelix.ifengine.choice.ChoiceResult;

@RunWith(MockitoJUnitRunner.class)
public class CommandHandlerTest {

    private CommandHandler commandHandler;

    private ActionHandler actionHandler;
    private ExecutionResultHandler defaultResultHandler;
    private StyledResponseFilter responseFilter;

    @Before
    public void setup() {
        actionHandler = mock(ActionHandler.class);
        defaultResultHandler = mock(ExecutionResultHandler.class);
        responseFilter = mock(StyledResponseFilter.class);

        commandHandler = new CommandHandlerImpl(actionHandler, defaultResultHandler, responseFilter);
    }

    @Test
    public void testHandleCommandWithOutcomes() {

        Command command = new Command();
        Action outcome = new Action();
        command.addOutcome(outcome);

        ActionResult expectedResult = new ActionResult();
        expectedResult.setSuccess(true);
        expectedResult.addResponseLine(new StyledResponseLine("HOO"));
        when( actionHandler.handleAction(any(Action.class), anyString(), anyString()) ).thenReturn(expectedResult);

        ChoiceResult result = commandHandler.handle(command, null, null);
        assertFalse(result.isFailure());

        // this had an outcome, so the default result handler should not have been used
        verify(defaultResultHandler, never()).execute(any(ExecutionSet.class), anyString(), anyString());

        assertEquals("HOO", result.getResponseLines().get(0).getText());
    }

    @Test
    public void testHandleCommandWithoutSuccessfulOutcome() {

        Command command = new Command();
        ExecutionResults defaultResult = new ExecutionResults();
        Action followupAction = new Action();
        defaultResult.getFollowupActions().add(followupAction);

        List<StyledResponseLine> defaultResponse = Arrays.asList(new StyledResponseLine("BADABA"));

        ActionResult followupResult = new ActionResult();
        followupResult.addResponseLine(new StyledResponseLine("YAA"));
        when( actionHandler.handleAction(any(Action.class), anyString(), anyString()) ).thenReturn(followupResult);
        when( defaultResultHandler.execute(any(ExecutionSet.class), anyString(), anyString()) ).thenReturn(defaultResult);
        when( responseFilter.filterResponse(anyString(), anyString(), anyString()) ).thenReturn(defaultResponse);

        ChoiceResult result = commandHandler.handle(command, null, null);
        assertFalse(result.isFailure());

        assertEquals("BADABA", result.getResponseLines().get(0).getText());
        assertEquals("YAA", result.getResponseLines().get(1).getText());
    }


}
