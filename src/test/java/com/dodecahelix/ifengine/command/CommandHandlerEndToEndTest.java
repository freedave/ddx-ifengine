/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.command;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.condition.ConditionDeterminationImpl;
import com.dodecahelix.ifengine.condition.ConditionResolutionLocator;
import com.dodecahelix.ifengine.dao.EnvironmentBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.TestEnvironmentBuilder;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.response.StyledResponseFilter;
import com.dodecahelix.ifengine.response.StyledResponseFilterImpl;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;
import com.dodecahelix.ifengine.result.ExecutionResultHandlerImpl;
import com.dodecahelix.ifengine.result.ExecutionServiceLocator;
import com.dodecahelix.ifengine.action.ActionHandler;
import com.dodecahelix.ifengine.action.ActionHandlerImpl;
import com.dodecahelix.ifengine.choice.ChoiceResult;

public class CommandHandlerEndToEndTest extends TestCase {

    private EnvironmentDAO dao;

    public void testCommandHandler() {

        EnvironmentBuilder builder = new TestEnvironmentBuilder();
        Environment environment = builder.build();
        dao = new SimpleEnvironmentDAO(environment);

        ConditionDetermination conditionDetermination = new ConditionDeterminationImpl(dao, new ConditionResolutionLocator());

        ExecutionResultHandler resultHandler = new ExecutionResultHandlerImpl(dao, new ExecutionServiceLocator());
        StyledResponseFilter responseFilter = new StyledResponseFilterImpl(dao);
        ActionHandler actionHandler = new ActionHandlerImpl(dao, conditionDetermination, resultHandler, responseFilter);
        CommandHandler handler = new CommandHandlerImpl(actionHandler, resultHandler, responseFilter);

        ChoiceResult message = null;
        for (Command command : commandSequence()) {
            message = handler.handle(command, null, null);
        }

        assertTrue(!message.getResponseLines().isEmpty());
        assertEquals("kitchen", dao.getCurrentLocation().getId());
    }

    private List<Command> commandSequence() {
        List<Command> commands = new ArrayList<Command>();

        // TODO - add more commands
        commands.add(getCommandByEntity(dao.getCurrentLocation(), "north"));

        return commands;
    }

    private Command getCommandByEntity(Entity entity, String commandName) {
        Command foundCommand = null;

        for (Command command : entity.getCommands()) {
            if (commandName.equalsIgnoreCase(command.getCommandDisplay())) {
                foundCommand = command;
            }
        }

        if (foundCommand==null) {
            throw new IllegalStateException("Command not found: " + commandName);
        }
        return foundCommand;
    }

}
