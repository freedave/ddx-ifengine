/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine;

import com.dodecahelix.ifengine.data.save.EnvironmentSaveBuilderAndLoaderTest;
import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestSuite;

import com.dodecahelix.ifengine.command.CommandHandlerEndToEndTest;
import com.dodecahelix.ifengine.command.CommandHandlerTest;
import com.dodecahelix.ifengine.condition.ConditionDeterminationTest;
import com.dodecahelix.ifengine.condition.resolve.CarriedResolverTest;
import com.dodecahelix.ifengine.condition.resolve.FlagResolverTest;
import com.dodecahelix.ifengine.condition.resolve.HoldingLightSourceResolverTest;
import com.dodecahelix.ifengine.condition.resolve.IsDarkResolverTest;
import com.dodecahelix.ifengine.condition.resolve.IsLightSourceResolverTest;
import com.dodecahelix.ifengine.condition.resolve.LocalTargetConditionTest;
import com.dodecahelix.ifengine.condition.resolve.NumericComparisonResolverTest;
import com.dodecahelix.ifengine.condition.resolve.StringComparisonResolverTest;
import com.dodecahelix.ifengine.condition.resolve.ThisConditionTest;
import com.dodecahelix.ifengine.condition.resolve.TimeResolverTest;
import com.dodecahelix.ifengine.condition.resolve.VisiblityResolverTest;
import com.dodecahelix.ifengine.dao.EnvironmentDAOTest;
import com.dodecahelix.ifengine.data.PropertyTest;
import com.dodecahelix.ifengine.data.serializer.json.JsonEnvironmentBuilderTest;
import com.dodecahelix.ifengine.response.ResponseFilterTest;
import com.dodecahelix.ifengine.result.ExecutionResultHandlerTest;
import com.dodecahelix.ifengine.result.actions.PropertyChangeHandlerTest;
import com.dodecahelix.ifengine.result.execute.AddTopicExecutorTest;
import com.dodecahelix.ifengine.result.execute.IncrementTimeExecutorTest;
import com.dodecahelix.ifengine.result.execute.MoveEntityExecutorTest;
import com.dodecahelix.ifengine.result.execute.SetDarknessExecutorTest;
import com.dodecahelix.ifengine.result.execute.SetLightSourceExecutorTest;
import com.dodecahelix.ifengine.result.execute.TakeItemExecutorTest;
import com.dodecahelix.ifengine.result.execute.UpdatePropertyExecutorTest;
import com.dodecahelix.ifengine.turn.ActionHandlerTest;
import com.dodecahelix.ifengine.turn.ChoiceProcessorTest;

public class AllTests {

    public static Test suite() {
        TestSuite suite = new TestSuite(AllTests.class.getName());

        //$JUnit-BEGIN$
        suite.addTest(new JUnit4TestAdapter(ChoiceProcessorTest.class));
        suite.addTest(new JUnit4TestAdapter(CommandHandlerTest.class));
        suite.addTest(new JUnit4TestAdapter(ActionHandlerTest.class));
        suite.addTest(new JUnit4TestAdapter(ExecutionResultHandlerTest.class));
        suite.addTest(new JUnit4TestAdapter(ConditionDeterminationTest.class));

        suite.addTest(new JUnit4TestAdapter(EnvironmentDAOTest.class));
        suite.addTestSuite(EnvironmentSaveBuilderAndLoaderTest.class);

        suite.addTestSuite(PropertyTest.class);

        suite.addTestSuite(FlagResolverTest.class);
        suite.addTestSuite(NumericComparisonResolverTest.class);
        suite.addTestSuite(StringComparisonResolverTest.class);
        suite.addTestSuite(VisiblityResolverTest.class);
        suite.addTestSuite(CarriedResolverTest.class);
        suite.addTestSuite(TimeResolverTest.class);
        suite.addTestSuite(ThisConditionTest.class);
        suite.addTestSuite(LocalTargetConditionTest.class);
        suite.addTestSuite(IsDarkResolverTest.class);
        suite.addTestSuite(IsLightSourceResolverTest.class);
        suite.addTestSuite(HoldingLightSourceResolverTest.class);

        suite.addTestSuite(MoveEntityExecutorTest.class);
        suite.addTestSuite(TakeItemExecutorTest.class);
        suite.addTestSuite(UpdatePropertyExecutorTest.class);
        suite.addTestSuite(AddTopicExecutorTest.class);
        suite.addTestSuite(IncrementTimeExecutorTest.class);
        suite.addTestSuite(SetDarknessExecutorTest.class);
        suite.addTestSuite(SetLightSourceExecutorTest.class);

        suite.addTestSuite(ResponseFilterTest.class);
        suite.addTestSuite(CommandHandlerEndToEndTest.class);
        suite.addTestSuite(PropertyChangeHandlerTest.class);
        suite.addTestSuite(JsonEnvironmentBuilderTest.class);

        //$JUnit-END$
        return suite;
    }

}
