/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Schedule;

public class EnvironmentDAOTest {

    private EnvironmentDAO dao;

    @Before
    public void setUp() throws Exception {
        EnvironmentBuilder builder = new TestEnvironmentBuilder();
        Environment environment = builder.build();
        dao = new SimpleEnvironmentDAO(environment);

        Assert.assertNotNull(environment);
    }

    @Test
    public void testReaderLocation() {
        Assert.assertEquals("livingroom", dao.getCurrentLocation().getId());
    }

    @Test
    public void testGetActiveSchedule() {

        Person person = (Person) dao.getEntityById(EntityType.PERSON, "dude");

        Schedule activeSchedule = dao.getActiveScheduleForPerson(person);
        Assert.assertEquals("kitchen", activeSchedule.getLocationId());
    }

}
