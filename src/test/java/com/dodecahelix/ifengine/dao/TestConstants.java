/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.dao;

public class TestConstants {

    public static final String DESCRIPTION_APPENDER = " Description.";

    public static final String LOCATION_ONE_TITLE = "Location One";
    public static final String LOCATION_TWO_TITLE = "Location Two";

    public static final String LOCATION_ONE_EXIT = "Exit One";
    public static final String LOCATION_TWO_EXIT = "Exit Two";

    public static final String ITEM_ONE_TITLE = "Item One";
    public static final String ITEM_TWO_TITLE = "Item Two";
    public static final String ITEM_THREE_TITLE = "Item Three";

    public static final String MASS_ONE_TITLE = "Mass One";

    public static final String PERSON_ONE_TITLE = "Person One";

    public static final String STAT_CATEGORY_SKILL = "skill";

    public static final String STAT_PROPERTY_TYPING = "skill-typing";


}
