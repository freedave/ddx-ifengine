/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.dao;

import java.util.HashSet;
import java.util.Set;

import com.dodecahelix.ifengine.data.Command;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Exit;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Mass;
import com.dodecahelix.ifengine.data.Person;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.data.Schedule;
import com.dodecahelix.ifengine.data.Time;
import com.dodecahelix.ifengine.data.configuration.TimeConfiguration;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.data.story.StoryContent;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;

/**
 *  Build the environment from java objects located in code
 *  
 * @author dpeters
 *
 */
public class TestEnvironmentBuilder implements EnvironmentBuilder {

    public Environment build() {

        Environment environment = new Environment();

        StoryContent content = new StoryContent();
        content.setIntroduction("Prologue: Blah");
        environment.setStoryContent(content);

        LibraryCard libraryCard = new LibraryCard();
        libraryCard.setTitle("Guess Whos Coming To Dinner");
        environment.setLibraryCard(libraryCard);

        Reader reader = buildReader();
        environment.getNarrators().add(reader);
        environment.setCurrentNarrator(reader.getId());

        environment.setLocations(buildLocations());
        environment.setPeople(buildPeople());
        environment.setMasses(buildMasses());
        environment.setItems(buildItems());

        environment.setCurrentTime(new Time(new TimeConfiguration()));

        return environment;
    }

    private static Set<Item> buildItems() {
        Set<Item> items = new HashSet<Item>();

        // item being carried by reader
        Item itemOne = new Item("towel", "towel", "A dirty green bath towel.");
        items.add(itemOne);

        // item lying on the floor of the kitchen
        Item itemTwo = new Item("knife", "knife", "A Ginsu steak knife.");
        items.add(itemTwo);

        // item on a bookshelf
        Item itemThree = new Item("book", "book", "A book on gardening.");
        items.add(itemThree);

        return items;
    }

    private static Set<Mass> buildMasses() {
        Set<Mass> masses = new HashSet<Mass>();
        Mass massOne = new Mass("bookshelf", "bookshelf", "A bookshelf fills up one wall of the room.");
        massOne.addContentItem("book");
        masses.add(massOne);

        return masses;
    }

    private static Set<Person> buildPeople() {
        Set<Person> people = new HashSet<Person>();

        Person personOne = new Person("dude", "dude", "The Dude is pretty cool.");

        Schedule personSchedule = new Schedule();
        personSchedule.setLocationId("kitchen");
        personSchedule.setActionMessage("The Dude is cooking an omelet");
        personOne.addSchedule(personSchedule);

        Command followMeCommand = new Command("ask dude to follow you", EntityType.PERSON);
        followMeCommand.setDefaultExecutionMessage("dude is following you");
        followMeCommand.setReferenceId("dude-follow-me");

        Execution followExecution = new Execution(ExecutionType.FOLLOW);
        followExecution.getEntityHolder().setSubjectType(EntityType.PERSON);
        followExecution.getEntityHolder().setSubjectEntityId("dude");
        followExecution.setOperator(ExecutionOperator.SET);
        followMeCommand.getDefaultResults().getExecutions().add(followExecution);
        personOne.addCommand(followMeCommand);

        people.add(personOne);

        return people;
    }

    private static Reader buildReader() {
        Reader reader = new Reader();
        reader.setTitle("Me");
        reader.setCurrentLocation("livingroom");
        reader.setDescription("I look good.");
        reader.addContentItem("towel");
        return reader;
    }

    private static Set<Location> buildLocations() {
        Set<Location> locations = new HashSet<Location>();

        Location roomOne = new Location("livingroom", "Living Room","You are standing in the living room of your house.");
        roomOne.addExit(new Exit("North", "kitchen"));
        roomOne.addExit(new Exit("Up", "upstairs"));
        roomOne.addContentItem("knife");

        Location roomTwo = new Location("kitchen", "Kitchen", "You are standing in the kitchen.");
        roomTwo.addExit(new Exit("South","living-room"));
        roomTwo.addMass("bookshelf");

        Location roomThree = new Location("upstairs", "Upstairs", "You are standing in the attic.");
        roomThree.addExit(new Exit("Down","living-room"));

        locations.add(roomOne);
        locations.add(roomTwo);
        locations.add(roomThree);

        return locations;
    }

}
