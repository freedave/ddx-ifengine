/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.actions;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Assert;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.condition.ConditionDeterminationImpl;
import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionResolutionLocator;
import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.dao.EnvironmentBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.TestEnvironmentBuilder;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.Action;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.response.StyledResponseFilter;
import com.dodecahelix.ifengine.response.StyledResponseFilterImpl;
import com.dodecahelix.ifengine.result.ExecutionException;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;
import com.dodecahelix.ifengine.result.ExecutionResultHandlerImpl;
import com.dodecahelix.ifengine.result.ExecutionServiceLocator;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;
import com.dodecahelix.ifengine.action.ActionHandler;
import com.dodecahelix.ifengine.action.ActionHandlerImpl;

public class PropertyChangeHandlerTest extends TestCase {

    private static final String INT_TO_ADD = "5";
    private static final String UPDATE_INT_PROPERTY = "numberProp";
    private static final String INT_INITIAL_VALUE = "10";

    private ActionHandler actionHandler;
    private EnvironmentDAO dao;

    public void setUp() {
        EnvironmentBuilder builder = new TestEnvironmentBuilder();
        Environment environment = builder.build();
        dao = new SimpleEnvironmentDAO(environment);

        ConditionDetermination conditionDetermination = new ConditionDeterminationImpl(dao, new ConditionResolutionLocator());
        ExecutionResultHandler resultHandler = new ExecutionResultHandlerImpl(dao, new ExecutionServiceLocator());
        StyledResponseFilter styledResponseFilter = new StyledResponseFilterImpl(dao);
        actionHandler = new ActionHandlerImpl(dao, conditionDetermination, resultHandler, styledResponseFilter);

        setupEnvironment(dao);
    }

    public void setupEnvironment(EnvironmentDAO dao) {

        Location testLocation = (Location) dao.getEntityById(EntityType.LOCATION, "kitchen");
        testLocation.setProperty(UPDATE_INT_PROPERTY, INT_INITIAL_VALUE);

        Action changeAction = new Action();
        Condition checkNewValue = new Condition(ConditionType.NUMERIC_PROPERTY_COMPARE);
        checkNewValue.getEntityHolder().setSubjectType(EntityType.LOCATION);
        checkNewValue.getEntityHolder().setSubjectEntityId("kitchen");
        checkNewValue.getEntityHolder().setSubjectProperty(UPDATE_INT_PROPERTY);
        checkNewValue.setOperator(ConditionOperator.GREATER_THAN);
        checkNewValue.setTargetType(TargetType.STATIC);
        checkNewValue.setTargetStaticValue("11");

        changeAction.getValidations().getConditions().add(checkNewValue);

        Execution changeExecution = new Execution(ExecutionType.ADD_TOPIC);
        changeExecution.getEntityHolder().setSubjectType(EntityType.READER);
        changeExecution.setOperator(ExecutionOperator.SET);
        changeExecution.setTargetStaticValue("GOTCHA");

        changeAction.getResults().getExecutions().add(changeExecution);

        testLocation.getPropertyById(UPDATE_INT_PROPERTY).getChangeActions().add(changeAction);
    }

    private List<Execution> buildExecutions() {
        List<Execution> executions = new ArrayList<Execution>();

        Execution incrementValueExecution = new Execution(ExecutionType.UPDATE_PROPERTY);
        incrementValueExecution.setOperator(ExecutionOperator.INCREMENT);
        incrementValueExecution.setTargetType(TargetType.STATIC);
        incrementValueExecution.setTargetStaticValue(INT_TO_ADD);

        EntityHolder entityHolderB = new EntityHolder();
        entityHolderB.setSubjectType(EntityType.LOCATION);
        entityHolderB.setSubjectEntityId("kitchen");
        entityHolderB.setSubjectProperty(UPDATE_INT_PROPERTY);
        incrementValueExecution.setEntityHolder(entityHolderB);

        executions.add(incrementValueExecution);

        return executions;
    }

    public void testHandleAction() throws ExecutionException {

        Action action = new Action();
        action.getResults().getExecutions().addAll(buildExecutions());

        actionHandler.handleAction(action, null, null);

        Reader reader = dao.getReader();
        Assert.assertTrue(reader.getKnownTopics().contains("GOTCHA"));
    }


}
