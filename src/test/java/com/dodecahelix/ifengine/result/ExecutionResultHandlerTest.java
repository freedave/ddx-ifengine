/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.data.Item;

@RunWith(MockitoJUnitRunner.class)
public class ExecutionResultHandlerTest {

    private ExecutionResultHandler executionResultHandler;

    private ExecutionServiceLocator mockExecutionServiceLocator;
    private Executor mockExecutor;

    @Before
    public void setup() {
        EnvironmentDAO dao = mock(EnvironmentDAO.class);
        Environment environment = new Environment();
        when(dao.getEnvironment()).thenReturn(environment);

        Entity stubEntity = new Item();
        when(dao.getEntityById(any(EntityType.class), anyString())).thenReturn(stubEntity);

        mockExecutionServiceLocator = mock(ExecutionServiceLocator.class);
        mockExecutor = mock(Executor.class);

        when( mockExecutionServiceLocator.getExecutorForType(any(ExecutionType.class)) ).thenReturn(mockExecutor);

        executionResultHandler = new ExecutionResultHandlerImpl(dao, mockExecutionServiceLocator);
    }

    @Test
    public void testExecute() throws ExecutionException {

        ExecutionSet executionSet = new ExecutionSet();
        executionSet.getExecutions().add(new Execution(ExecutionType.ADD_TOPIC));

        ExecutionResults executionResults = new ExecutionResults();
        executionResults.getMessages().add("WOO");

        when( mockExecutor.execute(any(Execution.class), any(Entity.class), any(Entity.class)) ).thenReturn(executionResults);

        ExecutionResults executionResult = executionResultHandler.execute(executionSet, null, null);

        assertEquals("WOO", executionResult.getMessages().get(0));

    }

}
