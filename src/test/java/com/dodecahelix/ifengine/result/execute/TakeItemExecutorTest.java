/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.Executor;

public class TakeItemExecutorTest extends AbstractExecutorTest {

    @Override
    public void setupEnvironment(EnvironmentDAO dao) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Execution> buildExecutions() {

        Execution readerTakeExecution = new Execution(ExecutionType.CHOWN_ITEM);
        EntityHolder itemToReaderEntityHolder = new EntityHolder();
        readerTakeExecution.setEntityHolder(itemToReaderEntityHolder);
        itemToReaderEntityHolder.setSubjectType(EntityType.ITEM);
        itemToReaderEntityHolder.setSubjectEntityId("knife");

        Execution personTakeExecution = new Execution(ExecutionType.CHOWN_ITEM);
        EntityHolder itemToPersonEntityHolder = new EntityHolder();
        personTakeExecution.setEntityHolder(itemToPersonEntityHolder);
        personTakeExecution.setTargetType(TargetType.ENTITY);
        itemToPersonEntityHolder.setSubjectType(EntityType.ITEM);
        itemToPersonEntityHolder.setSubjectEntityId("book");
        itemToPersonEntityHolder.setTargetType(EntityType.PERSON);
        itemToPersonEntityHolder.setTargetEntityId("dude");

        List<Execution> executions = new ArrayList<Execution>();
        executions.add(readerTakeExecution);
        executions.add(personTakeExecution);
        return executions;
    }

    @Override
    public Executor initExecutor() {
        return new ChangeItemOwnershipExecutor();
    }

    @Override
    public void testPreExecutionConditions(EnvironmentDAO dao) {
        assertFalse(dao.getReader().getContents().contains("knife"));

        Entity personEntity = dao.getEntityById(EntityType.PERSON, "dude");
        assertFalse(personEntity.getContents().contains("book"));
    }

    @Override
    public void testResult(EnvironmentDAO dao, List<ExecutionResults> results) {
        assertTrue(dao.getReader().getContents().contains("knife"));
        assertFalse(dao.getReader().getContents().contains("book"));

        Entity personEntity = dao.getEntityById(EntityType.PERSON, "dude");
        assertTrue(personEntity.getContents().contains("book"));
    }

}
