/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import java.util.ArrayList;
import java.util.List;

import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.data.Property;
import com.dodecahelix.ifengine.data.PropertyType;
import com.dodecahelix.ifengine.data.Reader;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.Executor;
import com.dodecahelix.ifengine.result.parse.ExecutionOperator;

public class UpdatePropertyExecutorTest extends AbstractExecutorTest {

    private static final String UPDATE_PROPERTY = "testProp";
    private static final String COPY_PROPERTY = "copyProp";
    private static final String COPY_VALUE = "testValue";
    private static final String INT_TO_ADD = "5";
    private static final String UPDATE_INT_PROPERTY = "numberProp";
    private static final String INT_INITIAL_VALUE = "10";

    public void testExecute() {
    }

    @Override
    public void setupEnvironment(EnvironmentDAO dao) {
        Item item = (Item) dao.getEntityById(EntityType.ITEM, "book");
        item.setProperty(COPY_PROPERTY, COPY_VALUE);

        Location testLocation = (Location) dao.getEntityById(EntityType.LOCATION, "kitchen");
        testLocation.setProperty(UPDATE_INT_PROPERTY, INT_INITIAL_VALUE);

        dao.getReader().setCurrentLocation("kitchen");
        dao.getReader().setProperty(UPDATE_PROPERTY, "Bogus");
    }

    @Override
    public List<Execution> buildExecutions() {
        List<Execution> executions = new ArrayList<Execution>();

        Execution updateFromEntityExecution = new Execution(ExecutionType.UPDATE_PROPERTY);
        updateFromEntityExecution.setOperator(ExecutionOperator.SET);
        updateFromEntityExecution.setTargetType(TargetType.ENTITY);

        EntityHolder entityHolderA = new EntityHolder();
        entityHolderA.setSubjectType(EntityType.READER);
        entityHolderA.setSubjectProperty(UPDATE_PROPERTY);
        entityHolderA.setTargetType(EntityType.ITEM);
        entityHolderA.setTargetEntityId("book");
        entityHolderA.setTargetProperty(COPY_PROPERTY);
        updateFromEntityExecution.setEntityHolder(entityHolderA);

        Execution incrementValueExecution = new Execution(ExecutionType.UPDATE_PROPERTY);
        incrementValueExecution.setOperator(ExecutionOperator.INCREMENT);
        incrementValueExecution.setTargetType(TargetType.STATIC);
        incrementValueExecution.setTargetStaticValue(INT_TO_ADD);

        EntityHolder entityHolderB = new EntityHolder();
        entityHolderB.setSubjectType(EntityType.CURRENT_LOCATION);
        entityHolderB.setSubjectProperty(UPDATE_INT_PROPERTY);
        incrementValueExecution.setEntityHolder(entityHolderB);

        executions.add(updateFromEntityExecution);
        executions.add(incrementValueExecution);

        return executions;
    }

    @Override
    public Executor initExecutor() {
        return new UpdatePropertyExecutor();
    }


    @Override
    public void testPreExecutionConditions(EnvironmentDAO dao) {
        Location location = dao.getCurrentLocation();
        Property intProp = location.getPropertyById(UPDATE_INT_PROPERTY);
        assertEquals(PropertyType.INTEGER, intProp.getType());
        assertEquals(10, intProp.getValue());
    }

    @Override
    public void testResult(EnvironmentDAO dao, List<ExecutionResults> results) {
        Reader reader = dao.getReader();
        Property stringProp = reader.getPropertyById(UPDATE_PROPERTY);
        assertEquals(stringProp.getStringValue(), COPY_VALUE);

        Location location = dao.getCurrentLocation();
        Property intProp = location.getPropertyById(UPDATE_INT_PROPERTY);
        assertEquals(PropertyType.INTEGER, intProp.getType());
        assertEquals(15, intProp.getValue());
    }

}
