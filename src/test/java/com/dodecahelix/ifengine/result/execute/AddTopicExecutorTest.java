/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.Executor;

public class AddTopicExecutorTest extends AbstractExecutorTest {

    @Override
    public void setupEnvironment(EnvironmentDAO dao) {
    }

    @Override
    public List<Execution> buildExecutions() {
        Execution addTopicExecution = new Execution(ExecutionType.ADD_TOPIC);
        addTopicExecution.getEntityHolder().setSubjectType(EntityType.ENVIRONMENT);
        addTopicExecution.setTargetStaticValue("USA");

        List<Execution> executions = new ArrayList<Execution>();
        executions.add(addTopicExecution);

        return executions;
    }

    @Override
    public Executor initExecutor() {
        return new AddTopicExecutor();
    }

    @Override
    public void testPreExecutionConditions(EnvironmentDAO dao) {
        List<String> knownTopics = dao.getReader().getKnownTopics();
        Assert.assertFalse(knownTopics.contains("USA"));
    }

    @Override
    public void testResult(EnvironmentDAO dao, List<ExecutionResults> results) {
        List<String> knownTopics = dao.getReader().getKnownTopics();
        Assert.assertTrue(knownTopics.contains("USA"));
    }

}
