package com.dodecahelix.ifengine.result.execute;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.story.Conclusion;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.Executor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 8/1/2016.
 */
public class StoryCompleteExecutorTest  extends AbstractExecutorTest {

    @Override
    public Executor initExecutor() {
        return new StoryCompleteExecutor();
    }

    @Override
    public void setupEnvironment(EnvironmentDAO dao) {
        dao.getEnvironment().getStoryContent().getConclusions().add(new Conclusion("conclusion-a", "This is ending A."));
        dao.getEnvironment().getStoryContent().getConclusions().add(new Conclusion("conclusion-b", "This is ending B!"));
    }

    @Override
    public List<Execution> buildExecutions() {
        List<Execution> executions = new ArrayList<Execution>();

        Execution storyCompleteConclusionA = new Execution(ExecutionType.STORY_COMPLETE);
        storyCompleteConclusionA.setTargetStaticValue("conclusion-a");
        storyCompleteConclusionA.getEntityHolder().setSubjectType(EntityType.ENVIRONMENT);
        executions.add(storyCompleteConclusionA);

        return executions;
    }

    @Override
    public void testPreExecutionConditions(EnvironmentDAO dao) {
    }


    @Override
    public void testResult(EnvironmentDAO dao, List<ExecutionResults> results) {
        assertEquals("This is ending A.", results.get(0).getMessages().get(0));
    }

}
