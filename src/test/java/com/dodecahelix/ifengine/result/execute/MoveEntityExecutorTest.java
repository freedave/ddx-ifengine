/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import java.util.Arrays;
import java.util.List;

import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Location;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.Executor;

public class MoveEntityExecutorTest extends AbstractExecutorTest {

    @Override
    public Executor initExecutor() {
        return new MoveEntityExecutor();
    }

    @Override
    public void setupEnvironment(EnvironmentDAO dao) {
        // not necessary
    }

    @Override
    public List<Execution> buildExecutions() {

        Execution moveReaderExecution = new Execution(ExecutionType.MOVE_ENTITY);
        EntityHolder readerToLocationEntityHolder = new EntityHolder();
        moveReaderExecution.setEntityHolder(readerToLocationEntityHolder);
        moveReaderExecution.setTargetType(TargetType.ENTITY);
        readerToLocationEntityHolder.setSubjectType(EntityType.READER);
        readerToLocationEntityHolder.setTargetType(EntityType.LOCATION);
        readerToLocationEntityHolder.setTargetEntityId("kitchen");

        Execution moveMassExecution = new Execution(ExecutionType.MOVE_ENTITY);
        EntityHolder massToLocationEntityHolder = new EntityHolder();
        moveMassExecution.setEntityHolder(massToLocationEntityHolder);
        moveMassExecution.setTargetType(TargetType.ENTITY);
        massToLocationEntityHolder.setSubjectType(EntityType.MASS);
        massToLocationEntityHolder.setSubjectEntityId("bookshelf");
        massToLocationEntityHolder.setTargetType(EntityType.LOCATION);
        massToLocationEntityHolder.setTargetEntityId("livingroom");

        List<Execution> executions = Arrays.asList(moveReaderExecution, moveMassExecution);

        return executions;
    }

    @Override
    public void testPreExecutionConditions(EnvironmentDAO dao) {
        assertNotSame("kitchen", dao.getCurrentLocation().getId());
    }

    @Override
    public void testResult(EnvironmentDAO dao, List<ExecutionResults> results) {
        assertEquals("kitchen", dao.getCurrentLocation().getId());

        Location locationOne = (Location) dao.getEntityById(EntityType.LOCATION, "livingroom");

        boolean found = false;
        for (String massId : locationOne.getMasses()) {
            if ("bookshelf".equalsIgnoreCase(massId)) {
                found = true;
            }
        }
        assertTrue(found);
    }

}
