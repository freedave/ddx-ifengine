/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import com.dodecahelix.ifengine.dao.EnvironmentBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.TestEnvironmentBuilder;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.ExecutionSet;
import com.dodecahelix.ifengine.result.ExecutionException;
import com.dodecahelix.ifengine.result.ExecutionResultHandler;
import com.dodecahelix.ifengine.result.ExecutionResultHandlerImpl;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.ExecutionServiceLocator;
import com.dodecahelix.ifengine.result.Executor;

public abstract class AbstractExecutorTest extends TestCase {

    private ExecutionResultHandler executionHandler;
    private EnvironmentDAO dao;

    public abstract void setupEnvironment(EnvironmentDAO dao);

    public abstract List<Execution> buildExecutions();

    public abstract Executor initExecutor();

    public abstract void testPreExecutionConditions(EnvironmentDAO dao);

    public abstract void testResult(EnvironmentDAO dao, List<ExecutionResults> results);

    public void setUp() {

        EnvironmentBuilder builder = new TestEnvironmentBuilder();
        Environment environment = builder.build();
        dao = new SimpleEnvironmentDAO(environment);

        ExecutionServiceLocator serviceLocator = new ExecutionServiceLocator();
        executionHandler = new ExecutionResultHandlerImpl(dao, serviceLocator);

        initExecutor();

        setupEnvironment(dao);
    }

    public void testExecution() throws ExecutionException {

        testPreExecutionConditions(dao);

        ExecutionSet execSet = new ExecutionSet();
        execSet.setExecutions(buildExecutions());

        ExecutionResults results = executionHandler.execute(execSet, null, null);
        List<ExecutionResults> resultList = Arrays.asList(results);

        testResult(dao, resultList);

    }

}
