/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.result.execute;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Execution;
import com.dodecahelix.ifengine.data.Time;
import com.dodecahelix.ifengine.result.ExecutionResults;
import com.dodecahelix.ifengine.result.ExecutionType;
import com.dodecahelix.ifengine.result.Executor;

public class IncrementTimeExecutorTest extends AbstractExecutorTest {

    @Override
    public Executor initExecutor() {
        return new TimeIncrementExecutor();
    }

    @Override
    public void setupEnvironment(EnvironmentDAO dao) {
        Time currentTime = dao.getEnvironment().getCurrentTime();

        currentTime.setIncrement(6);
        currentTime.setPeriod(4);
        currentTime.setWeekday(7);
        currentTime.setWeek(1);
        currentTime.setMonth(11);
        currentTime.setYear(2000);
    }

    @Override
    public List<Execution> buildExecutions() {

        Execution incrementPeriodExecution = new Execution(ExecutionType.INC_TIME_PERIOD);
        EntityHolder environmentEntityHolder = new EntityHolder();
        incrementPeriodExecution.setEntityHolder(environmentEntityHolder);
        environmentEntityHolder.setSubjectType(EntityType.ENVIRONMENT);

        Execution incrementIntervalExecution = new Execution(ExecutionType.INC_TIME_INTERVAL);
        incrementIntervalExecution.setEntityHolder(environmentEntityHolder);

        List<Execution> executions = new ArrayList<Execution>();
        executions.add(incrementIntervalExecution);
        executions.add(incrementPeriodExecution);

        return executions;
    }

    @Override
    public void testPreExecutionConditions(EnvironmentDAO dao) {
    }

    @Override
    public void testResult(EnvironmentDAO dao, List<ExecutionResults> results) {
        Time resultTime = dao.getEnvironment().getCurrentTime();

        Assert.assertEquals(1, resultTime.getIncrement());
        Assert.assertEquals(2, resultTime.getPeriod());
        Assert.assertEquals(1, resultTime.getWeekday());
        Assert.assertEquals(2, resultTime.getWeek());
        Assert.assertEquals(11, resultTime.getMonth());
        Assert.assertEquals(2000, resultTime.getYear());
    }
}
