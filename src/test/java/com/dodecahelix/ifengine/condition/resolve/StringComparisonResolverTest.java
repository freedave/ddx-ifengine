/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Item;

public class StringComparisonResolverTest extends AbstractResolverTest {

    @Override
    public ConditionResolver initResolver() {
        return new StringComparisonResolver();
    }

    @Override
    public void updateEnvironment(EnvironmentDAO dao) {
        Item testItem = new Item("Test Item", "testDescription");
        testItem.setProperty("testProp", "testValue");
        dao.getEnvironment().addItem(testItem);
    }

    @Override
    public Condition buildCondition() {
        Condition condition = new Condition(ConditionType.STRING_PROPERTY_COMPARE);
        condition.getEntityHolder().setSubjectType(EntityType.ITEM);
        condition.getEntityHolder().setSubjectEntityId("test-item");
        condition.getEntityHolder().setSubjectProperty("testProp");

        condition.setOperator(ConditionOperator.EQUALS);

        condition.setTargetType(TargetType.STATIC);
        condition.setTargetStaticValue("testValue");

        return condition;
    }

}
