/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import org.junit.Assert;

import junit.framework.TestCase;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.condition.ConditionDeterminationImpl;
import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionResolutionLocator;
import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.condition.ResolutionException;
import com.dodecahelix.ifengine.condition.TargetType;
import com.dodecahelix.ifengine.dao.EnvironmentBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.TestEnvironmentBuilder;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Item;
import com.dodecahelix.ifengine.data.Location;

public class ThisConditionTest extends TestCase {

    private ConditionDetermination conditionDetermination;

    public void setUp() {

        EnvironmentBuilder builder = new TestEnvironmentBuilder();
        Environment environment = builder.build();
        EnvironmentDAO dao = new SimpleEnvironmentDAO(environment);

        ConditionResolutionLocator locator = new ConditionResolutionLocator();
        conditionDetermination = new ConditionDeterminationImpl(dao, locator);

        Item testItem = new Item("Test Item", "testDescription");
        testItem.setProperty("testProp", "18");
        dao.getEnvironment().addItem(testItem);

        Location testLocation = new Location("Test Location", "testDescription");
        testLocation.setProperty("testProp", "15");
        dao.getEnvironment().addLocation(testLocation);
    }

    public void testThisCondition() throws ResolutionException {

        Condition condition = new Condition(ConditionType.NUMERIC_PROPERTY_COMPARE);
        condition.getEntityHolder().setSubjectType(EntityType.ITEM);
        condition.getEntityHolder().setSubjectEntityId(EntityHolder.THIS_SUBJECT_ID);
        condition.getEntityHolder().setSubjectProperty("testProp");

        condition.setOperator(ConditionOperator.GREATER_THAN_EQUALS);

        condition.setTargetType(TargetType.ENTITY);

        condition.getEntityHolder().setTargetEntityId("test-location");
        condition.getEntityHolder().setTargetType(EntityType.LOCATION);
        condition.getEntityHolder().setTargetProperty("testProp");

        boolean resolution = conditionDetermination.determine(condition, "test-item", null);
        Assert.assertTrue(resolution);
    }

}
