/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import junit.framework.TestCase;

import com.dodecahelix.ifengine.condition.ConditionDetermination;
import com.dodecahelix.ifengine.condition.ConditionDeterminationImpl;
import com.dodecahelix.ifengine.condition.ConditionResolutionLocator;
import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.dao.EnvironmentBuilder;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.dao.TestEnvironmentBuilder;
import com.dodecahelix.ifengine.dao.simple.SimpleEnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.Environment;

public abstract class AbstractResolverTest extends TestCase {

    private ConditionDetermination conditionDetermination;

    public abstract ConditionResolver initResolver();

    public abstract void updateEnvironment(EnvironmentDAO dao);

    public abstract Condition buildCondition();

    public void setUp() {
        initResolver();

        EnvironmentBuilder builder = new TestEnvironmentBuilder();
        Environment environment = builder.build();
        EnvironmentDAO dao = new SimpleEnvironmentDAO(environment);

        updateEnvironment(dao);

        ConditionResolutionLocator locator = new ConditionResolutionLocator();
        conditionDetermination = new ConditionDeterminationImpl(dao, locator);
    }

    public void testResolve() {

        boolean resolution = conditionDetermination.determine(buildCondition(), null, null);
        assertTrue(resolution);

    }

}
