/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition.resolve;

import com.dodecahelix.ifengine.condition.ConditionOperator;
import com.dodecahelix.ifengine.condition.ConditionResolver;
import com.dodecahelix.ifengine.condition.ConditionType;
import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Item;

public class IsLightSourceResolverTest extends AbstractResolverTest {

    private Item flashlight = new Item("flashlight", "this is a flashlight.");

    @Override
    public ConditionResolver initResolver() {
        return new IsLightSourceResolver();
    }

    @Override
    public void updateEnvironment(EnvironmentDAO dao) {

        flashlight.setLightSource(true);

        dao.getEnvironment().addItem(flashlight);
        dao.getReader().addContentItem("flashlight");
    }

    @Override
    public Condition buildCondition() {
        Condition condition = new Condition(ConditionType.IS_LIGHT_SOURCE);
        condition.setOperator(ConditionOperator.TRUE);
        condition.getEntityHolder().setSubjectType(EntityType.ITEM);
        condition.getEntityHolder().setSubjectEntityId(flashlight.getId());
        return condition;
    }


}
