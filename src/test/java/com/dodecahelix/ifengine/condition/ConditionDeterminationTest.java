/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     David Peters - initial API and implementation
 *******************************************************************************/
package com.dodecahelix.ifengine.condition;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.dodecahelix.ifengine.dao.EnvironmentDAO;
import com.dodecahelix.ifengine.data.Condition;
import com.dodecahelix.ifengine.data.ConditionSet;
import com.dodecahelix.ifengine.data.Entity;
import com.dodecahelix.ifengine.data.EntityHolder;
import com.dodecahelix.ifengine.data.EntityType;
import com.dodecahelix.ifengine.data.Environment;
import com.dodecahelix.ifengine.data.Item;

@RunWith(MockitoJUnitRunner.class)
public class ConditionDeterminationTest {

    private ConditionDetermination conditionDetermination;

    private ConditionResolutionLocator mockConditionResolutionLocator;

    private ConditionResolver mockTrueConditionResolver;
    private ConditionResolver mockFalseConditionResolver;

    private EnvironmentDAO mockDao;

    private Environment environment;

    @Before
    public void setup() throws ResolutionException {
        mockDao = mock(EnvironmentDAO.class);
        environment = new Environment();
        when(mockDao.getEnvironment()).thenReturn(environment);

        Entity mockEntity = new Item();
        when(mockDao.getEntityById(any(EntityType.class), anyString())).thenReturn(mockEntity);

        mockConditionResolutionLocator = mock(ConditionResolutionLocator.class);
        mockTrueConditionResolver = mock(ConditionResolver.class);
        mockFalseConditionResolver = mock(ConditionResolver.class);

        // make it easy to create conditions that are true and false, for testing

        // for a FLAG condition, return true
        when( mockConditionResolutionLocator.getResolverForType(ConditionType.FLAG)).thenReturn(mockTrueConditionResolver);
        when( mockTrueConditionResolver.resolve(any(Condition.class), any(Entity.class), any(Entity.class))).thenReturn(true);

        // for an OWNED condition, return false
        when( mockConditionResolutionLocator.getResolverForType(ConditionType.OWNED)).thenReturn(mockFalseConditionResolver);
        when( mockFalseConditionResolver.resolve(any(Condition.class), any(Entity.class), any(Entity.class))).thenReturn(false);

        conditionDetermination = new ConditionDeterminationImpl(mockDao, mockConditionResolutionLocator);
    }

    @Test
    public void testResolve() throws ResolutionException {
        Condition condition = new Condition(ConditionType.FLAG);

        boolean valid = conditionDetermination.determine(condition, null, null);
        assertTrue(valid);

        valid = conditionDetermination.determine(condition, EntityHolder.THIS_SUBJECT_ID, null);
        assertTrue(valid);

        valid = conditionDetermination.determine(condition, null, EntityHolder.LOCAL_TARGET_ID);
        assertTrue(valid);
    }


    @Test
    public void testDetermineSetForAndCondition() throws ResolutionException {

        // sample AND condition.  two true, one false.  should resolve to FALSE
        ConditionSet conditionSet = new ConditionSet();
        conditionSet.setAndSet(true);
        conditionSet.getConditions().add(new Condition(ConditionType.FLAG));
        conditionSet.getConditions().add(new Condition(ConditionType.FLAG));
        conditionSet.getConditions().add(new Condition(ConditionType.OWNED));

        boolean valid = conditionDetermination.determineSet(conditionSet, null, null);
        assertFalse(valid);

        // but this should work for an OR set
        conditionSet.setAndSet(false);
        valid = conditionDetermination.determineSet(conditionSet, null, null);
        assertTrue(valid);
    }

    @Test
    public void testDetermineSetWithReferenceConditions() throws ResolutionException {

        ConditionSet sharedConditionA = new ConditionSet();
        sharedConditionA.setReferenceId("shared-isdark");
        sharedConditionA.getConditions().add(new Condition(ConditionType.FLAG));
        environment.getSharedConditions().add(sharedConditionA);

        ConditionSet sharedConditionB = new ConditionSet();
        sharedConditionB.setReferenceId("shared-isugly");
        sharedConditionB.getConditions().add(new Condition(ConditionType.OWNED));
        environment.getSharedConditions().add(sharedConditionB);

        // make the test set require these two conditions
        ConditionSet conditionSet = new ConditionSet();
        conditionSet.getReferenceConditions().add("shared-isdark");
        conditionSet.getReferenceConditions().add("shared-isugly");

        boolean valid = conditionDetermination.determineSet(conditionSet, null, null);

        // these should be false, since its an AND and one is true but the other is false
        assertFalse(valid);

        // now, make it an OR and it should pass
        conditionSet.setAndSet(false);
        valid = conditionDetermination.determineSet(conditionSet, null, null);
        assertTrue(valid);
    }

    @Test
    public void testReferenceConditionsMixedWithNormalConditions() throws ResolutionException {

        ConditionSet sharedConditionA = new ConditionSet();
        sharedConditionA.setReferenceId("shared-isugly");
        sharedConditionA.getConditions().add(new Condition(ConditionType.OWNED));
        environment.getSharedConditions().add(sharedConditionA);

        // make the test set require these two conditions
        ConditionSet conditionSet = new ConditionSet();
        conditionSet.getReferenceConditions().add("shared-isugly");
        conditionSet.getConditions().add(new Condition(ConditionType.FLAG));

        boolean valid = conditionDetermination.determineSet(conditionSet, null, null);

        // this should be false, since the shared condition is false
        assertFalse(valid);
    }

}
