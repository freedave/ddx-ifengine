# IF-Engine #

### What is this repository for? ###

* IF-Engine is a library which provides a java API for creating interactive fiction (IF) stories.  It contains the story model and structure of the elements, as well as the processing engine for handling commands and responses.
* Version 1.0.0

### How do I get set up? ###

* This is a java library based on JRE 1.7+
* Maven 3+ is required to build
* Run `mvn clean install` on the root directory to generate the library and install to your local repository.
* Dependencies:
    * [Google GSON](https://github.com/google/gson) version 2.2.4 - for model data serialization to JSON format
    * [SLF4J](http://www.slf4j.org/) version 1.7.13 - logging facade
    * [J-Unit](http://junit.org/) version 4.11 - unit testing
    * [Mockito](http://mockito.org/) version 1.9.5 - mocks for unit testing
    * [Hamcrest](http://hamcrest.org/) version 1.3 - matchers for unit testing

### Contribution guidelines ###

* If you are interested in contributing to this codebase, please contact freedavep@gmail.com

### Who can I contact concerning this code? ###

* Contact : freedavep@gmail.com

### Introduction to the IF-Engine Library
IF-Engine is a java library for processing an interactive fiction story.  It is the "engine" component (or back-end) intended to be used by some front-end, or "reader" application (such as the ClickFic mobile application) which will provide the display of the story text and handle the reader input/choices.  The IF-Engine library is presentation agnostic, and the reader application can be built for any platform/OS, as long as it can wrap this java library and interact with its interface.

### Building the IF-Engine library
The library binary is packaged as a jar file, using Apache Maven to for its compile and build tool.  To create the jar, install Maven and run "mvn clean package" from a command line (or "mvn clean install" if you are interested in the ClickFic platform).

### Initializing the Engine and interacting with the API
The reader client should interact with the library through the *com.dodecahelix.ifengine.IfEngine* interface, creating an instance of the default implementation:

    IfEngine ifEngine = new DefaultIfEngine();

IfEngine provides two sub-interfaces, the **ChoiceProcessor** and the **EnvironmentProcessor**, accessed through getter methods.  The ChoiceProcessor is for handling decisions made by the reader -- by executing the choice (for example, getting a milk carton out of the refrigerator), updating the story data model (otherwise know as the *environment*), sending a response message, and providing a new set of options for the next choice.  The EnvironmentProcessor is for system commands, which handles story restarts, saving and reloading of story progress, and some administrative features (such as the recording of choices for playback).

To load a story into the engine for processing:

    ifEngine.getEnvironmentProcessor().loadStory(StoryBundle bundle);

Where a *StoryBundle* is a java object which contains the story model in some format for persistent storage.  The default media type is JSON-formatted text string, but a future update will support file encryption and compression of that text.  StoryBundle is only an interface, implemented by JsonStoryBundle which takes the JSON-formatted text as a constructor.  More information on the data model for the JSON format is described in the *Environment Model* section below.

To start a story, processing an initial command that is configured in the data model:

    ChoiceResult initialResponse = ifEngine.getEnvironmentProcessor().startStory();

The default command for starting a story (and loading saved progress) is to look around the location where the narrator begins.  The description of that location should be returned in the response text of the ChoiceResult.  The actual response text is modeled as a List<StyledResponseLine>.  It is split into lines so that each line may have its own unique style.  For example, the ClickFic application displays the style for a system line using the color red.  Different line styles are supported in a single response.

Also included in the response is the CommandTree.  This is a hierarchy of options available to the reader for their next choice.  These options are categorized into groups, called GroupListItem's, strategically to force the reader into some critical thinking for their next decision.  There are two "types" of options -- commands and dialogs.  Commands are physical actions, whereas dialogs are conversations with third-person characters.

To process the reader's command decision:

    ChoiceResult result = ifEngine.getChoiceProcessor().processCommand(Command command, String thisSubjectId, String localTargetId);

To process a dialog choice:

    ChoiceResult result = ifEngine.getChoiceProcessor().processDialog(Dialog command, String characterId);

Those are the most commonly used interface points that the reader client will need to interact with.  For the full source of the IfEngine interface, [follow this link.](https://bitbucket.org/freedave/ddx-ifengine/src/bce733549ade7092554dace61d9e108a64b938d8/src/main/java/com/dodecahelix/ifengine/IfEngine.java?at=master&fileviewer=file-view-default)

## The Environment Model
The **Environment** is a data model which describes the current state of the story.  It contains all of the entities of the story - the characters, the narrator, the locations and items which can be used, etc.., as well as their current location, and any properties they may have (for example, whether a door is open or closed).  It also contains the choice commands which can be used with these entities, and any conditional rules for evaluating these commands.

NOTE: It will be easier to visualize this model by bringing up the [ifengine-authoring-tool](https://bitbucket.org/freedave/ifengine-authoring-tool), which has a GUI for creating and modifying these data model objects.  The ClickFic website has a [walkthough](http://www.clickfic.com) in the documentation section that gives a basic understanding of the Environment model and IF story creation.

There are five basic *EntityType*s:

* Narrator(s) or Reader:  Voice and decision maker of the story.  The IfEngine supports switching between multiple narrators.
* Location:  A room or place in the story.  The narrator can move between locations using *Exits*
* Person:  A third-person character in the story.  Has schedules to set their location, and dialogs for conversation with the reader.
* Item:  A physical item that can be carried around by someone or put into/onto something.  Items have owners that can be any entity.
* Mass:  A stationary prop that is tied to a location (for example, a refrigerator that belongs to the kitchen).

These entities share several common features -- they each have a unique id, title and description text, can maintain key-value properties (such as boolean, string, integer), can have ownership of items, can include a unique set of commands, and a set of actions they can perform independent of choices (called timed actions).   Each of these EntityTypes have their own unique features as well.  A Location, for example, has a set of *Exit*s that determine where the narrator can move to, and a set of *Mass* objects contained within.  One of the best ways to become familiar with these item types is through the [ClickFic](http://www.clickfic.com) walkthrough and quickstart documentation.

### Commands, Executions and Conditions

[Commands](https://bitbucket.org/freedave/ddx-ifengine/src/bce733549ade7092554dace61d9e108a64b938d8/src/main/java/com/dodecahelix/ifengine/data/Command.java?at=master&fileviewer=file-view-default) are the choices that are presented in the option tree, along with Dialogs, and represent such actions as movement, using items, searching, and other physical activities.  Commands are associated with entities (i.e; an 'Eat' command would be associated with a hamburger item), and so all of the EntityType objects have a **List<Command> commands** field (this is a list, not a set, so that its presentation order will be consistent).  Commands belong to the specific object (i.e; the Axe will have its own "Chop Tree" action ).  There are also EntityType commands that are applicable to all objects of a certain EntityType -- for example, all items have a "Pick Up" command, so that they can be picked up from the ground if they are left lying around.  EntityType commands are part of the Configuration section of the environment model.

When a command-type choice is passed to the IfEngine ChoiceProcessor, in addition to producing a response, it will likely result in a change to the environment model.  For example, using the key on the door lock will open the door for the reader to pass through.  Changes to the evironment are handled by **Executions**.  There are dozens of [ExecutionType's](https://bitbucket.org/freedave/ddx-ifengine/src/bce733549ade7092554dace61d9e108a64b938d8/src/main/java/com/dodecahelix/ifengine/result/ExecutionType.java?at=master&fileviewer=file-view-default) -- updating a property, changing ownership of an item, moving the reader to a different location, etc..  [Browse through the source code](https://bitbucket.org/freedave/ddx-ifengine/src/bce733549ade7092554dace61d9e108a64b938d8/src/main/java/com/dodecahelix/ifengine/result/execute/?at=master) for the different types of executions available.  Suggestions for new execution types are welcome.

Commands can have different responses and executions based on environment conditions, called **outcomes**.  Every command has a default outcome, since there must be a response to every command, and a *List<Action> outcomes* for all of the alternatives that can happen.  An example is that the "Eat Hamburger" command might default to being eaten, but have an alternative outcome where the reader is too full to eat.  To determine if an alternative outcome should be executed instead of the default, it must meet a set of **conditions** based on the context and environment state.  Conditions are fine-grained rules used for determining if actions should be executed.  Just like there are various ExecutionTypes for Executions, there are also various [ConditionTypes](https://bitbucket.org/freedave/ddx-ifengine/src/bce733549ade7092554dace61d9e108a64b938d8/src/main/java/com/dodecahelix/ifengine/condition/ConditionType.java?at=master&fileviewer=file-view-default) for Conditions.  For example, there are condition types for checking if a flag is set, if the reader is in a certain location, etc..  Multiple conditions can be grouped into a ConditionSet, using either AND or OR logic to evaluate the composite.  ConditionSets can be nested inside of other ConditionSets to perform complex decision making.

Executions and Conditions are the fundamental components of the ChoiceProcessor model, and should be understood before anything else in the codebase.
